﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class CareerController : Controller
    {
        // GET: Career
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        public ActionResult Index()
        {
            var career = Dbcontext.Tbl_Job_Details.Where(a => a.is_active == true && a.status == "OPEN").ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.careers = career;
            return View();
        }
        public ActionResult jobdetails(int id)
        {
            var career = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).FirstOrDefault();
            ViewBag.careerdet = career;
            return View();
        }
        public ActionResult Applynow(int id)
        {
            Session["jid"] = id;
            var career = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).FirstOrDefault();
            ViewBag.careerdet = career;
            return View();
        }
        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
        private void SendHtmlFormattedEmailForHr(string recepientEmail, string subject, string body,string attachment)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                Attachment att = new Attachment(Server.MapPath(VirtualPathUtility.ToAbsolute("~/Images/resume/" + attachment)));
                mailMessage.Attachments.Add(att);
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
        private string hrbody(string applicant, string position, string email,string phone, string subject,string image,string attach)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Templates/hrtemplate.html")))
            {
               
                body = reader.ReadToEnd().Replace("{{applicant}}", applicant).Replace("{{position}}", position).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{profileimage}}",image);
                SendHtmlFormattedEmailForHr("hr@odmegroup.org", subject, body, attach);
                //SendHtmlFormattedEmailForHr("parismita@thedigichamps.com", subject, body,attach);
            }

            return body;
        }
        private string candidatebody(string applicant, string position, string subject,string email)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Templates/Candidatetemplate.html")))
            {
                body = reader.ReadToEnd().Replace("{{applicant}}", applicant).Replace("{{position}}", position);
                SendHtmlFormattedEmail(email.Trim().ToLower(), subject, body);
            }

            return body;
        }
        [HttpPost]
        public ActionResult savecandidatedetails(string fullname, string email, string phone, HttpPostedFileBase RegImage1, HttpPostedFileBase RegImage11)
        {
            Tbl_Candidate_For_Job ob = new Tbl_Candidate_For_Job();
            ob.email = email;
            ob.phone = phone;
            ob.jobid = Convert.ToInt32(Session["jid"].ToString());
            if (RegImage1 != null)
            {
                string guid = Guid.NewGuid().ToString();
                var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                var path = Path.Combine(Server.MapPath("~/Images/resume/"), fileName);
                RegImage1.SaveAs(path);
                ob.resume = fileName.ToString();
            }
            if (RegImage11 != null)
            {
                string guid = Guid.NewGuid().ToString();
                var fileName = Path.GetFileName(RegImage11.FileName.Replace(RegImage11.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                var path = Path.Combine(Server.MapPath("~/Images/candidates/"), fileName);
                RegImage11.SaveAs(path);
                ob.image = fileName.ToString();
            }
            ob.status = "APPLIED";
            ob.name = fullname;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.modified_on = DateTime.Now;
            Dbcontext.Tbl_Candidate_For_Job.Add(ob);
            Dbcontext.SaveChanges();
            int candidateid = ob.id;
            var career = Dbcontext.Tbl_Job_Details.Where(a => a.id == ob.jobid).FirstOrDefault();
            string profile = ob.image;
            //Send Mail to HR
            //using (MailMessage mailMessage = new MailMessage())
            //{
            //    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
            //    mailMessage.Subject = career.position;
            //    mailMessage.Body = "You've a new applicant," + career.position;
            //    mailMessage.IsBodyHtml = true;
            //    Attachment att = new Attachment(Server.MapPath(VirtualPathUtility.ToAbsolute("~/Images/resume/" + ob.resume)));
            //    mailMessage.Attachments.Add(att);
            //    mailMessage.To.Add(new MailAddress("parismita@thedigichamps.com"));
            //    SmtpClient smtp = new SmtpClient();
            //    smtp.Host = ConfigurationManager.AppSettings["Host"];
            //    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            //    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            //    NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
            //    NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
            //    smtp.UseDefaultCredentials = true;
            //    smtp.Credentials = NetworkCred;
            //    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            //    smtp.Send(mailMessage);
            //}
            this.hrbody(fullname, career.position, email, phone, "Resume For " + career.position, "https://www.odmps.org/Images/candidates/" + profile,ob.resume);
            //Send mail to candidate
            //using (MailMessage mailMessage = new MailMessage())
            //{
            //    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
            //    mailMessage.Subject = career.position;
            //    mailMessage.Body = "Hello,\n Your resume successfully sent to HR...";
            //    mailMessage.IsBodyHtml = true;
            //    //Attachment att = new Attachment(Server.MapPath(VirtualPathUtility.ToAbsolute("~/Images/resume/" + ob.resume)));
            //    //mailMessage.Attachments.Add(att);
            //    mailMessage.To.Add(new MailAddress(email));
            //    SmtpClient smtp = new SmtpClient();
            //    smtp.Host = ConfigurationManager.AppSettings["Host"];
            //    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            //    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            //    NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
            //    NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
            //    smtp.UseDefaultCredentials = true;
            //    smtp.Credentials = NetworkCred;
            //    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            //    smtp.Send(mailMessage);
            //}
            this.candidatebody(fullname, career.position, "Confirmation From HR", email);
            return RedirectToAction("thankyou", "Career",new{id=candidateid});
        }
        public ActionResult thankyou(int id)
        {
            Session["careerid"] = id;
            return View();
        }
        public JsonResult SaveFeedback(string knowaboutus)
        {
            try
            {
                int id = Convert.ToInt32(Session["careerid"].ToString());
                Tbl_Candidate_For_Job ob = Dbcontext.Tbl_Candidate_For_Job.Where(a => a.id == id).FirstOrDefault();
                ob.feedback = knowaboutus;
                Dbcontext.SaveChanges();
                Session["careerid"] = null;
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
    }
}