﻿using odm_site.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class AdminController : Controller
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string Username, string Password)
        {
            if (Username == "Admin")
            {
                if (Password == "admin@1234")
                {
                    Session["role_id"] = 1;
                    Session["user_id"] = 121;
                    return RedirectToAction("Dashboard", "Admin");
                }
                else
                {
                    ViewBag.errormessage = "Incorrect Password... Please try again";
                }
            }
            else
            {
                ViewBag.errormessage = "Invalid Username and Password... Please try again";
            }
            if (Username == "BlogAdmin")
            {
                if (Password == "blog@1234")
                {
                    Session["role_id"] = 2;
                    Session["user_id"] = 121;
                    return RedirectToAction("Dashboard", "Admin");
                }
                else
                {
                    ViewBag.errormessage = "Incorrect Password... Please try again";
                }
            }
            else
            {
                ViewBag.errormessage = "Invalid Username and Password... Please try again";
            }
            return View();
        }
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Admin");
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public static Bitmap ResizeImage(Stream stream, int hght, int wdth)
        {
            int height = hght;
            int width = wdth;
            Bitmap scaledImage = new Bitmap(width, height);
            try
            {
                System.Drawing.Image originalImage = Bitmap.FromStream(stream);
                using (Graphics g = Graphics.FromImage(scaledImage))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(originalImage, 0, 0, width, height);
                    return scaledImage;
                }
            }
            catch (Exception Exc)
            {
                Exc.ToString();
                return scaledImage;
            }
        }
        #region landing page
        public ActionResult landingpage()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.landingpagelist = Dbcontext.tbl_main_slider.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removelandingpage(int id)
        {
            var ob = Dbcontext.tbl_main_slider.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_main_slider obj = Dbcontext.tbl_main_slider.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("landingpage", "Admin");
        }
        public ActionResult createoreditlandingpage(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["lid"] = null;
                ViewBag.description = "";
                ViewBag.title1 = "";
                ViewBag.link = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["lid"] = id;
                tbl_main_slider ob = Dbcontext.tbl_main_slider.Where(a => a.id == id).FirstOrDefault();
                ViewBag.description = ob.description;
                ViewBag.title1 = ob.title;
                ViewBag.link = ob.link;
                ViewBag.image = "../../Images/landingpage/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatelandingpage(string title, string description, string link, HttpPostedFileBase RegImage1)
        {
            if (Session["lid"] == null)
            {
                tbl_main_slider ob = new tbl_main_slider();
                ob.title = title;
                ob.description = description;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.link = link;
                ob.modified_on = DateTime.Now;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/landingpage/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.tbl_main_slider.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["lid"]));
                tbl_main_slider ob = Dbcontext.tbl_main_slider.Where(a => a.id == id).FirstOrDefault();
                ob.description = description;
                ob.title = title;
                ob.is_active = true;
                ob.link = link;
                ob.modified_on = DateTime.Now;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/landingpage/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("landingpage", "Admin");
        }
        #endregion

        #region contact us
        public class contact_Us_class
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string purpose { get; set; }
            public string message { get; set; }
            public string feedback { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
        }
        public ActionResult contactus()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            var contact = Dbcontext.tbl_school_contact_us.Where(a => a.is_active == true).ToList();
            var catg = Dbcontext.Tbl_School.ToList();
            var classes = Dbcontext.Tbl_Class.ToList();
            var res = (from a in contact.ToList()
                       select new contact_Us_class
                       {
                           purpose = a.purpose,
                           email = a.email,
                           feedback = a.feedback,
                           id = a.id,
                           inserted_on = a.inserted_on,
                           message = a.message,
                           name = a.name,
                           phone = a.phone
                       }).ToList().OrderByDescending(a => a.inserted_on);

            ViewBag.schoolcontactus = res;
            return View();
        }
        public ActionResult removecontactus(int id)
        {
            var ob = Dbcontext.tbl_school_contact_us.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_school_contact_us obj = Dbcontext.tbl_school_contact_us.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("contactus", "Admin");
        }
        #endregion

        #region Campus
        public ActionResult campus()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.campuslist = Dbcontext.Tbl_campus.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removecampus(int id)
        {
            var ob = Dbcontext.Tbl_campus.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_campus obj = Dbcontext.Tbl_campus.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("campus", "Admin");
        }

        public ActionResult createoreditcampus(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["cid"] = null;
                ViewBag.description = "";
                ViewBag.location = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["cid"] = id;
                Tbl_campus ob = Dbcontext.Tbl_campus.Where(a => a.id == id).FirstOrDefault();
                ViewBag.description = ob.description;
                ViewBag.location = ob.location;
                ViewBag.image = "../../Images/campus/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatecampus(string location, string description, HttpPostedFileBase RegImage1)
        {
            if (Session["cid"] == null)
            {
                Tbl_campus ob = new Tbl_campus();
                ob.location = location;
                ob.description = description;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/campus/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.Tbl_campus.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["cid"]));
                Tbl_campus ob = Dbcontext.Tbl_campus.Where(a => a.id == id).FirstOrDefault();
                ob.description = description;
                ob.location = location;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/campus/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("campus", "Admin");
        }
        #endregion

        #region Blog
        public class blog_class
        {
            public int id { get; set; }
            public string description { get; set; }
            public string image { get; set; }
            public string title { get; set; }
            // public string school_Name { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
        }
        public ActionResult blog()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            var blog = Dbcontext.tbl_school_blog.Where(a => a.is_active == true).ToList();
            var school = Dbcontext.Tbl_School.ToList();
            var res = (from a in blog
                       select new blog_class
                       {
                           description = a.description,
                           id = a.id,
                           image = a.image,
                           title = a.title,
                           inserted_on = a.inserted_on,
                           //school_Name = a.campusId != 0 ? school.Where(b => b.school_Id == a.campusId).FirstOrDefault().school_Name : ""
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.campuslist = Dbcontext.Tbl_School.Where(a => a.Is_Active == true).ToList();
            ViewBag.schoolbloglist = res;
            return View();
        }
        public ActionResult removeblog(int id)
        {
            var ob = Dbcontext.tbl_school_blog.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_school_blog obj = Dbcontext.tbl_school_blog.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("blog", "Admin");
        }
        public ActionResult createoreditblog(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["bid"] = null;
                ViewBag.title1 = "";
                ViewBag.description = "";
                // ViewBag.campusid = 0;
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["bid"] = id;
                tbl_school_blog ob = Dbcontext.tbl_school_blog.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                ViewBag.description = ob.description;
                //  ViewBag.campusid = ob.campusId;
                ViewBag.image = "../../Images/blog/" + ob.image;
            }
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveorUpdateblog(string title, string description, HttpPostedFileBase RegImage1)
        {
            if (Session["bid"] == null)
            {
                tbl_school_blog ob = new tbl_school_blog();
                ob.title = title;
                ob.description = description;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                // ob.campusId = campusid;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/blog/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 150, 150);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/blog_thumb/"), fileName);
                    bitmapImage.Save(path1);
                    ob.image_thumb = fileName.ToString();
                    // Image2.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(stream.ToArray(), 0, stream.ToArray().Length);
                }
                Dbcontext.tbl_school_blog.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["bid"]));
                tbl_school_blog ob = Dbcontext.tbl_school_blog.Where(a => a.id == id).FirstOrDefault();
                ob.description = description;
                ob.title = title;
                // ob.campusId = campusid;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/blog/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 150, 150);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/blog_thumb/"), fileName);
                    bitmapImage.Save(path1);
                    ob.image_thumb = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("blog", "Admin");
        }
        #endregion

        #region Blog comment
        public ActionResult Comments_on_blog(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Account");
            }
            ViewBag.blog_id = id;
            ViewBag.commentslist = Dbcontext.Tbl_Comment_On_Blog.Where(a => a.is_active == true && a.blog_id == id).ToList();
            return View();
        }
        public ActionResult CreateComments(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Account");
            }

            Session["blog_id"] = id;
            return View();
        }
        public ActionResult CreateSubComments(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Account");
            }
            Session["comment_id"] = id;
            return View();
        }
        public ActionResult SaveComments(string email, string message, string name, string website)
        {
            int blogid = Convert.ToInt32(Session["blog_id"].ToString());
            Tbl_Comment_On_Blog ob = new Tbl_Comment_On_Blog();
            ob.blog_id = blogid;
            ob.email = email;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.message = message;
            ob.name = name;
            Dbcontext.Tbl_Comment_On_Blog.Add(ob);
            Dbcontext.SaveChanges();
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("Comments_on_blog", "Admin", new { id = blogid });
        }
        public ActionResult SaveSubComments(string email, string message, string name, string website)
        {
            int commentid = Convert.ToInt32(Session["comment_id"].ToString());
            Tbl_Sub_Comments_On_Blog ob = new Tbl_Sub_Comments_On_Blog();
            ob.comment_id = commentid;
            ob.email = email;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.message = message;
            ob.name = name;
            Dbcontext.Tbl_Sub_Comments_On_Blog.Add(ob);
            Dbcontext.SaveChanges();
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("Sub_Comments_on_blog", "Admin", new { id = commentid });
        }
        public ActionResult Remove_Comments_on_blog(int id)
        {
            var ob = Dbcontext.Tbl_Comment_On_Blog.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Comment_On_Blog obj = Dbcontext.Tbl_Comment_On_Blog.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Comments_on_blog", "Admin");
        }
        public ActionResult Sub_Comments_on_blog(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Account");
            }
            ViewBag.commentid = id;
            ViewBag.subcommentslist = Dbcontext.Tbl_Sub_Comments_On_Blog.Where(a => a.is_active == true && a.comment_id == id).ToList();
            return View();
        }
        public ActionResult Remove_Sub_Comments_on_blog(int id)
        {
            var ob = Dbcontext.Tbl_Sub_Comments_On_Blog.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Sub_Comments_On_Blog obj = Dbcontext.Tbl_Sub_Comments_On_Blog.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Sub_Comments_on_blog", "Admin", new { id = ob.ToList()[0].comment_id });
        }
        #endregion

        #region Video Testimonial
        public ActionResult videotestimonial()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.videotestimoniallist = Dbcontext.Tbl_parents_testimonial.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult convertallvideotestimonialimagetothumb()
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("/Images/videotestimonial/"));
            if (directory != null)
            {
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    Image img = Image.FromFile(file.FullName);
                    Bitmap bitmapImage = ResizeImageToThumb(img, 174, 301);

                    string filename = Path.GetFileName(file.FullName);
                    var path1 = Server.MapPath("~/Images/videotestimonialthumb/") + filename;
                    if (!System.IO.File.Exists(path1))
                    {
                        bitmapImage.Save(path1);
                        bitmapImage.Dispose();
                    }
                }
            }
            TempData["SuccessMessage"] = "Converted Successfully";
            return RedirectToAction("videotestimonial", "Admin");
        }
        public ActionResult removevideotestimonial(int id)
        {
            var ob = Dbcontext.Tbl_parents_testimonial.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_parents_testimonial obj = Dbcontext.Tbl_parents_testimonial.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("videotestimonial", "Admin");
        }
        public ActionResult createoreditvideotestimonial(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["tid"] = null;
                ViewBag.message = "";
                ViewBag.name = "";
                ViewBag.videolink = "";
                ViewBag.designation = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["tid"] = id;
                Tbl_parents_testimonial ob = Dbcontext.Tbl_parents_testimonial.Where(a => a.id == id).FirstOrDefault();
                ViewBag.message = ob.message;
                ViewBag.name = ob.name;
                ViewBag.videolink = ob.videolink;
                ViewBag.designation = ob.designation;
                ViewBag.image = "../../Images/videotestimonial/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatevideotestimonial(string name, string message, string videolink, string designation, HttpPostedFileBase RegImage1)
        {
            if (Session["tid"] == null)
            {
                Tbl_parents_testimonial ob = new Tbl_parents_testimonial();
                ob.designation = designation;
                ob.message = message;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.name = name;
                ob.videolink = videolink;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/videotestimonial/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 174, 301);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/videotestimonialthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.Tbl_parents_testimonial.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["tid"]));
                Tbl_parents_testimonial ob = Dbcontext.Tbl_parents_testimonial.Where(a => a.id == id).FirstOrDefault();
                ob.designation = designation;
                ob.name = name;
                ob.is_active = true;
                ob.videolink = videolink;
                ob.message = message;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/videotestimonial/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 174, 301);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/videotestimonialthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("videotestimonial", "Admin");
        }
        #endregion

        #region Photo Gallery
        public ActionResult photogallery()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.photogallerylist = Dbcontext.tbl_odm_gallery.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removephotogallery(int id)
        {
            var ob = Dbcontext.tbl_odm_gallery.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_odm_gallery obj = Dbcontext.tbl_odm_gallery.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("photogallery", "Admin");
        }
        public ActionResult createoreditphotogallery(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["gid"] = null;
                ViewBag.title1 = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["gid"] = id;
                tbl_odm_gallery ob = Dbcontext.tbl_odm_gallery.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                ViewBag.image = "../../Images/gallery/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatephotogallery(string title, HttpPostedFileBase RegImage1)
        {
            if (Session["gid"] == null)
            {
                tbl_odm_gallery ob = new tbl_odm_gallery();
                ob.title = title;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/gallery/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.tbl_odm_gallery.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["gid"]));
                tbl_odm_gallery ob = Dbcontext.tbl_odm_gallery.Where(a => a.id == id).FirstOrDefault();
                ob.title = title;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/gallery/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("photogallery", "Admin");
        }
        #endregion

        #region Media Gallery
        public ActionResult mediagallery()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.mediagallerylist = Dbcontext.tbl_media_gallery.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removemediagallery(int id)
        {
            var ob = Dbcontext.tbl_media_gallery.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_media_gallery obj = Dbcontext.tbl_media_gallery.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("mediagallery", "Admin");
        }
        public ActionResult createoreditmediagallery(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["mid"] = null;
                ViewBag.title1 = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["mid"] = id;
                tbl_media_gallery ob = Dbcontext.tbl_media_gallery.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                ViewBag.image = "../../Images/media/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatemediagallery(string title, HttpPostedFileBase RegImage1)
        {
            if (Session["mid"] == null)
            {
                tbl_media_gallery ob = new tbl_media_gallery();
                ob.title = title;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/media/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.tbl_media_gallery.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["mid"]));
                tbl_media_gallery ob = Dbcontext.tbl_media_gallery.Where(a => a.id == id).FirstOrDefault();
                ob.title = title;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/media/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("mediagallery", "Admin");
        }
        #endregion

        #region Events
        public string converttime(string sp)
        {
            string s = DateTime.ParseExact(sp.Substring(0, 5), "HH:mm", System.Globalization.CultureInfo.CurrentCulture).ToString("hh:mm tt");
            return s;
        }
        public ActionResult events()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            var events = Dbcontext.tbl_school_event.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.eventslist = events;
            return View();
        }
        public ActionResult removeevent(int id)
        {
            var ob = Dbcontext.tbl_school_event.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                tbl_school_event obj = Dbcontext.tbl_school_event.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("events", "Admin");
        }
        public ActionResult createoreditevents(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["eid"] = null;
                ViewBag.description = "";
                ViewBag.end_date = DateTime.Now;
                ViewBag.end_time = DateTime.Now.TimeOfDay;
                ViewBag.location = "";
                ViewBag.title1 = "";
                ViewBag.start_date = DateTime.Now;
                ViewBag.start_time = DateTime.Now.TimeOfDay;
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["eid"] = id;
                tbl_school_event ob = Dbcontext.tbl_school_event.Where(a => a.id == id).FirstOrDefault();
                ViewBag.description = ob.description;
                ViewBag.end_date = ob.end_date;
                ViewBag.end_time = ob.end_time;
                ViewBag.location = ob.location;
                ViewBag.title1 = ob.name;
                ViewBag.start_date = ob.start_date;
                ViewBag.start_time = ob.start_time;
                ViewBag.image = "../../Images/events/" + ob.image;
            }
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveorUpdateevents(string name, string description, string location, DateTime startdate, DateTime enddate, TimeSpan starttime, TimeSpan endtime, HttpPostedFileBase RegImage1)
        {
            if (Session["eid"] == null)
            {
                tbl_school_event ob = new tbl_school_event();
                ob.name = name;
                ob.description = description;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.location = location;
                ob.start_date = startdate;
                ob.start_time = starttime;
                ob.end_time = endtime;
                ob.end_date = enddate;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/events/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.tbl_school_event.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["eid"]));
                tbl_school_event ob = Dbcontext.tbl_school_event.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.description = description;
                ob.is_active = true;
                ob.location = location;
                ob.start_date = startdate;
                ob.start_time = starttime;
                ob.end_time = endtime;
                ob.end_date = enddate;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/blog/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("events", "Admin");
        }
        #endregion

        #region Awards
        public ActionResult awards()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.awardslist = Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeawards(int id)
        {
            var ob = Dbcontext.Tbl_School_Awards.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_School_Awards obj = Dbcontext.Tbl_School_Awards.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("awards", "Admin");
        }
        public ActionResult createoreditawards(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["aid"] = null;
                ViewBag.description = "";
                ViewBag.name = "";
                ViewBag.year = 0;
                //ViewBag.isviewinmainpage = false;
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["aid"] = id;
                Tbl_School_Awards ob = Dbcontext.Tbl_School_Awards.Where(a => a.id == id).FirstOrDefault();
                ViewBag.description = ob.description;
                ViewBag.name = ob.name;
                ViewBag.year = ob.year;
                //  ViewBag.isviewinmainpage = ob.is_viewinmainpage;
                ViewBag.image = "../../Images/awards/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateawards(string name, string description, int year, HttpPostedFileBase RegImage1)
        {
            if (Session["aid"] == null)
            {
                Tbl_School_Awards ob = new Tbl_School_Awards();
                ob.description = description;
                ob.year = year;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.name = name;
                ob.is_viewinmainpage = false;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/awards/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.Tbl_School_Awards.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["aid"]));
                Tbl_School_Awards ob = Dbcontext.Tbl_School_Awards.Where(a => a.id == id).FirstOrDefault();
                ob.description = description;
                ob.year = year;
                ob.is_active = true;
                ob.name = name;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/awards/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("awards", "Admin");
        }
        public JsonResult addmainpageawards(string elem)
        {
            var awards=Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true).ToList();
            foreach(var a in awards)
            {
                Tbl_School_Awards ob = Dbcontext.Tbl_School_Awards.Where(b => b.id == a.id).FirstOrDefault();
                ob.is_viewinmainpage = false;
                Dbcontext.SaveChanges();
            }
            string[] ele = elem.Split(',');
            foreach (string m in ele)
            {
                int id = Convert.ToInt32(m);
                Tbl_School_Awards ob = Dbcontext.Tbl_School_Awards.Where(a => a.id == id).FirstOrDefault();
                ob.is_viewinmainpage = true;
                Dbcontext.SaveChanges();
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region School Partners
        public ActionResult schoolpartners()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.schoolpartnerslist = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult convertallpartnersimagetothumb()
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("/Images/partners/"));
            if (directory != null)
            {
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    Image img = Image.FromFile(file.FullName);
                    Bitmap bitmapImage = ResizeImageToThumb(img, 105, 130);

                    string filename = Path.GetFileName(file.FullName);
                    var path1 = Server.MapPath("~/Images/partnersthumb/") + filename;
                    if (!System.IO.File.Exists(path1))
                    {
                        bitmapImage.Save(path1);
                        bitmapImage.Dispose();
                    }
                }
            }
            TempData["SuccessMessage"] = "Converted Successfully";
            return RedirectToAction("schoolpartners", "Admin");
        }
        public ActionResult removeschoolpartners(int id)
        {
            var ob = Dbcontext.Tbl_School_Partners.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_School_Partners obj = Dbcontext.Tbl_School_Partners.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("schoolpartners", "Admin");
        }
        public ActionResult createoreditschoolpartners(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["pid"] = null;
                ViewBag.name = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["pid"] = id;
                Tbl_School_Partners ob = Dbcontext.Tbl_School_Partners.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
                ViewBag.image = "../../Images/partners/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateschoolpartners(string name, HttpPostedFileBase RegImage1)
        {
            if (Session["pid"] == null)
            {
                Tbl_School_Partners ob = new Tbl_School_Partners();
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.name = name;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/partners/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 105, 130);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/partnersthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.Tbl_School_Partners.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["pid"]));
                Tbl_School_Partners ob = Dbcontext.Tbl_School_Partners.Where(a => a.id == id).FirstOrDefault();
                ob.is_active = true;
                ob.name = name;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/partners/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 105, 130);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/partnersthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("schoolpartners", "Admin");
        }
        #endregion

        #region Job Details
        public ActionResult career()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.joblist = Dbcontext.Tbl_Job_Details.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removecareer(int id)
        {
            var ob = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Job_Details obj = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("career", "Admin");
        }
        public ActionResult createoreditcareer(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["aid"] = null;
                var res = Dbcontext.Tbl_Job_Details.ToList().LastOrDefault();
                string code = "";
                if (res != null)
                {
                    code = DateTime.Now.Year.ToString().Substring(2, 2) + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "" + (res.id + 1);
                }
                else
                {
                    code = DateTime.Now.Year.ToString().Substring(2, 2) + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "1";
                }
                ViewBag.code = code;
                Session["jcode"] = code;
                ViewBag.experience = "";
                ViewBag.no_of_vacancy = 0;
                ViewBag.position = "";
                ViewBag.responsibilities = "";
                ViewBag.salary = "";
                ViewBag.skills = "";
                ViewBag.qualification = "";
                ViewBag.status = "OPEN";
            }
            else
            {
                Session["aid"] = id;
                Tbl_Job_Details ob = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).FirstOrDefault();
                ViewBag.code = ob.code;
                ViewBag.experience = ob.experience;
                ViewBag.no_of_vacancy = ob.no_of_vacancy;
                ViewBag.position = ob.position;
                ViewBag.responsibilities = ob.responsibilities;
                ViewBag.salary = ob.salary;
                ViewBag.skills = ob.skills;
                ViewBag.qualification = ob.qualification;
                ViewBag.status = ob.status;
            }
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveorUpdatecareer(string code, string experience, int noofvacancy, string position, string responsibilities, string salary, string skills, string status, string qualification)
        {
            if (Session["aid"] == null)
            {
                Tbl_Job_Details ob = new Tbl_Job_Details();
                ob.code = Session["jcode"].ToString();
                ob.experience = experience;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.no_of_vacancy = noofvacancy;
                ob.position = position;
                ob.responsibilities = responsibilities;
                ob.salary = salary;
                ob.skills = skills;
                ob.status = status;
                ob.qualification = qualification;
                Dbcontext.Tbl_Job_Details.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["aid"]));
                Tbl_Job_Details ob = Dbcontext.Tbl_Job_Details.Where(a => a.id == id).FirstOrDefault();
                ob.experience = experience;
                ob.is_active = true;
                ob.no_of_vacancy = noofvacancy;
                ob.position = position;
                ob.responsibilities = responsibilities;
                ob.salary = salary;
                ob.skills = skills;
                ob.status = status;
                ob.qualification = qualification;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("career", "Admin");
        }
        #endregion

        #region Result Category
        public class resultcategoryclass
        {
            public int id { get; set; }
            public string name { get; set; }
            public string typename { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
        }
        public ActionResult resultcategory()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }

            var catg = Dbcontext.Tbl_result_category.Where(a => a.is_active == true).ToList();
            var type = Dbcontext.Tbl_result_type.ToList();
            var res = (from a in catg
                       join b in type on a.typeid equals b.id
                       select new resultcategoryclass
                       {
                           id = a.id,
                           inserted_on = a.inserted_on,
                           name = a.name,
                           typename = b.name
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.resultcatglist = res;
            return View();
        }
        public ActionResult removeresultcategory(int id)
        {
            var ob = Dbcontext.Tbl_result_category.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_result_category obj = Dbcontext.Tbl_result_category.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("resultcategory", "Admin");
        }
        public ActionResult createoreditresultcategory(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.typelist = Dbcontext.Tbl_result_type.Where(a => a.is_active == true).ToList();
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.name = "";
                ViewBag.typeid = 0;
            }
            else
            {
                Session["rid"] = id;
                Tbl_result_category ob = Dbcontext.Tbl_result_category.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
                ViewBag.typeid = ob.typeid;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateresultcategory(string name, int typeid)
        {
            if (Session["rid"] == null)
            {
                Tbl_result_category ob = new Tbl_result_category();
                ob.name = name;
                ob.typeid = typeid;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_result_category.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                Tbl_result_category ob = Dbcontext.Tbl_result_category.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.typeid = typeid;
                ob.is_active = true;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("resultcategory", "Admin");
        }
        #endregion

        #region Result Type
        public ActionResult resulttype()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.resulttypelist = Dbcontext.Tbl_result_type.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeresulttype(int id)
        {
            var ob = Dbcontext.Tbl_result_type.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_result_type obj = Dbcontext.Tbl_result_type.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("resulttype", "Admin");
        }
        public ActionResult createoreditresulttype(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["rid"] = id;
                Tbl_result_type ob = Dbcontext.Tbl_result_type.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateresulttype(string name)
        {
            if (Session["rid"] == null)
            {
                Tbl_result_type ob = new Tbl_result_type();
                ob.name = name;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_result_type.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                Tbl_result_type ob = Dbcontext.Tbl_result_type.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.is_active = true;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("resulttype", "Admin");
        }
        #endregion

        #region Results
        public class resultdetailsclass
        {
            public int id { get; set; }
            public string category { get; set; }
            public string student_name { get; set; }
            public string image { get; set; }
            public string score { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
        }
        public ActionResult resultdetails()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            var results = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            var catg = Dbcontext.Tbl_result_category.ToList();
            var res = (from a in results
                       join b in catg on a.categoryid equals b.id
                       select new resultdetailsclass
                       {
                           id = a.id,
                           image = a.image,
                           inserted_on = a.inserted_on,
                           score = a.score,
                           student_name = a.student_name,
                           category = b.name + "-" + a.year
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.resultdetailslist = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            return View();
        }
        public static Bitmap ResizeImageToThumb(Image image, int hght, int wdth)
        {
            int height = hght;
            int width = wdth;
            Bitmap scaledImage = new Bitmap(width, height);
            try
            {
                System.Drawing.Image originalImage = image;
                using (Graphics g = Graphics.FromImage(scaledImage))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(originalImage, 0, 0, width, height);
                    return scaledImage;
                }
            }
            catch (Exception Exc)
            {
                Exc.ToString();
                return scaledImage;
            }
        }
        public ActionResult convertallresultsimagetothumb()
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("/Images/results/"));
            if (directory != null)
            {
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    Image img = Image.FromFile(file.FullName);
                    Bitmap bitmapImage = ResizeImageToThumb(img, 145, 145);

                    string filename = Path.GetFileName(file.FullName);
                    var path1 = Server.MapPath("~/Images/resultthumb/") + filename;
                    if (!System.IO.File.Exists(path1))
                    {
                        bitmapImage.Save(path1);
                        bitmapImage.Dispose();
                    }
                }
            }
            TempData["SuccessMessage"] = "Converted Successfully";
            return RedirectToAction("studentresult", "Admin");
        }
        public ActionResult removeresultdetails(int id)
        {
            var ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_School_Result obj = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("resultdetails", "Admin");
        }
        public ActionResult createoreditresultdetails(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["did"] = null;
                ViewBag.categoryid = 0;
                ViewBag.score = "";
                ViewBag.student_name = "";
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";

            }
            else
            {
                Session["did"] = id;
                Tbl_School_Result ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                ViewBag.categoryid = ob.categoryid;
                ViewBag.score = ob.score;
                ViewBag.student_name = ob.student_name;
                ViewBag.image = "../../Images/results/" + ob.image;
            }
            ViewBag.resultcategory = Dbcontext.Tbl_result_category.Where(a => a.is_active == true).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateresultdetails(string name, string score, int categoryid, HttpPostedFileBase RegImage1)
        {
            if (Session["did"] == null)
            {
                Tbl_School_Result ob = new Tbl_School_Result();
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.categoryid = categoryid;
                ob.score = score;
                ob.student_name = name;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/results/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 145, 145);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/resultthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.Tbl_School_Result.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["did"]));
                Tbl_School_Result ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                ob.is_active = true;
                ob.categoryid = categoryid;
                ob.score = score;
                ob.student_name = name;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/results/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 145, 145);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/resultthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("resultdetails", "Admin");
        }
        #endregion

        #region Candidate
        public ActionResult candidatelist(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.candidatelist = Dbcontext.Tbl_Candidate_For_Job.Where(a => a.is_active == true && a.jobid == id).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removecandidate(int id)
        {
            var ob = Dbcontext.Tbl_Candidate_For_Job.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Candidate_For_Job obj = Dbcontext.Tbl_Candidate_For_Job.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("candidatelist", "Admin", new { id = ob.FirstOrDefault().jobid });
        }
        #endregion

        #region Student Result
        public class Studentresultclass
        {
            public int id { get; set; }
            public string Studentname { get; set; }
            public string score { get; set; }
            public string image { get; set; }
            public string typename { get; set; }
            public string categoryname { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
        }
        public ActionResult studentresult()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            //DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Images/results"));//Assuming Test is your Folder
            //FileInfo[] Files = d.GetFiles(); //Getting Text files
            //string str = "";           
            //ArrayList arrayList = new ArrayList();

            //foreach (FileInfo fi in Files)
            //{
            //    Image img = Image.FromFile(fi.FullName);
            //    var newImage = ScaleImage(img, 235, 270);
            //    img.Dispose();
            //    newImage.Save(fi.FullName);
            //}
            var students = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            var catg = Dbcontext.Tbl_result_category.ToList();
            var type = Dbcontext.Tbl_result_type.ToList();
            var res = (from a in students
                       select new Studentresultclass
                       {
                           id = a.id,
                           inserted_on = a.inserted_on,
                           categoryname = a.categoryid == 0 ? "" : catg.Where(b => b.id == a.categoryid).FirstOrDefault().name,
                           image = a.image,
                           typename = a.typeid == 0 ? "" : type.Where(c => c.id == a.typeid).FirstOrDefault().name,
                           score = a.score,
                           Studentname = a.student_name
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.studentresultlist = res;
            return View();
        }
        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }
        public JsonResult getresultcategory(int id)
        {
            var ob = Dbcontext.Tbl_result_category.Where(a => a.typeid == id).ToList();
            return Json(ob, JsonRequestBehavior.AllowGet);
        }
        public ActionResult removestudentresult(int id)
        {
            var ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_School_Result obj = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("studentresult", "Admin");
        }
        public ActionResult createoreditstudentresult(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.typelist = Dbcontext.Tbl_result_type.Where(a => a.is_active == true).ToList();
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.categoryid = 0;
                ViewBag.typeid = 0;
                ViewBag.score = "";
                ViewBag.student_name = "";
                ViewBag.year = DateTime.Now.Year;
                ViewBag.image = "../wp-content/themes/healthville/img/noimages.png";
            }
            else
            {
                Session["rid"] = id;
                Tbl_School_Result ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                ViewBag.categoryid = ob.categoryid;
                ViewBag.typeid = ob.typeid;
                ViewBag.score = ob.score;
                ViewBag.student_name = ob.student_name;
                ViewBag.year = ob.year;
                ViewBag.image = "../../Images/results/" + ob.image;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatestudentresult(string student_name, int typeid, int? categoryid, string score, int year, HttpPostedFileBase RegImage1)
        {
            if (Session["rid"] == null)
            {
                Tbl_School_Result ob = new Tbl_School_Result();
                ob.categoryid = categoryid==null?0:categoryid;
                ob.score = score;
                ob.student_name = student_name;
                ob.year = year;
                ob.typeid = typeid;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/results/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 145, 145);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/resultthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                Dbcontext.Tbl_School_Result.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                Tbl_School_Result ob = Dbcontext.Tbl_School_Result.Where(a => a.id == id).FirstOrDefault();
                ob.categoryid = categoryid == null ? 0 : categoryid;
                ob.score = score;
                ob.student_name = student_name;
                ob.year = year;
                ob.typeid = typeid;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/results/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.image = fileName.ToString();
                    Bitmap bitmapImage = ResizeImage(RegImage1.InputStream, 145, 145);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    var path1 = Path.Combine(Server.MapPath("~/Images/resultthumb/"), fileName);
                    bitmapImage.Save(path1);
                    bitmapImage.Dispose();
                }
                ob.is_active = true;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("studentresult", "Admin");
        }
        #endregion

        #region Admission Enquiry
        public class Admission_Enquiry_class
        {
            public int id { get; set; }
            public string firstname { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string lastname { get; set; }
            public string presentschool { get; set; }
            public string className { get; set; }
            public string description { get; set; }
            public string city { get; set; }
            public string feedback { get; set; }
            public Nullable<System.DateTime> dob { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
        }
        public ActionResult admissionenquiry()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            var admission = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.is_active == true).ToList();
            var classes = Dbcontext.Tbl_School_Class.ToList();
            var res = (from a in admission
                       join c in classes.ToList() on a.classid equals c.Class_Id
                       select new Admission_Enquiry_class
                       {
                           firstname = a.firstname,
                           lastname = a.lastname,
                           city = a.city,
                           id = a.id,
                           inserted_on = a.inserted_on,
                           description = a.description,
                           email = a.email,
                           phone = a.phone,
                           presentschool = a.presentschool,
                           dob = a.dob,
                           className = c.Class_Name,
                           feedback = a.feedback
                       }).ToList().OrderByDescending(a => a.inserted_on);

            ViewBag.admissionenquirylist = res;
            return View();
        }
        public ActionResult removeadmissionenquiry(int id)
        {
            var ob = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Admission_Enquiry obj = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("admissionenquiry", "Admin");
        }
        #endregion

        #region Event Videos
        public ActionResult eventvideo()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.eventvideolist = Dbcontext.Tbl_Event_Video.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeeventvideo(int id)
        {
            var ob = Dbcontext.Tbl_Event_Video.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Event_Video obj = Dbcontext.Tbl_Event_Video.Where(a => a.id == id).FirstOrDefault();
                obj.isactive = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("eventvideo", "Admin");
        }
        public ActionResult createorediteventvideo(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["evid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["evid"] = id;
                Tbl_Event_Video ob = Dbcontext.Tbl_Event_Video.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.title;
                ViewBag.link = ob.youtubelink;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateeventvideo(string name, string link)
        {
            if (Session["evid"] == null)
            {
                Tbl_Event_Video ob = new Tbl_Event_Video();
                ob.title = name;
                ob.youtubelink = link;
                ob.inserted_on = DateTime.Now;
                ob.isactive = true;
                Dbcontext.Tbl_Event_Video.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["evid"]));
                Tbl_Event_Video ob = Dbcontext.Tbl_Event_Video.Where(a => a.id == id).FirstOrDefault();
                ob.title = name;
                ob.youtubelink = link;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("eventvideo", "Admin");
        }

        #endregion

        #region Notice
        public ActionResult notice()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.noticelist = Dbcontext.Tbl_school_notice.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removenotice(int id)
        {
            var ob = Dbcontext.Tbl_school_notice.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_school_notice obj = Dbcontext.Tbl_school_notice.Where(a => a.id == id).FirstOrDefault();
                obj.isactive = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("notice", "Admin");
        }
        public ActionResult createoreditnotice(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["scid"] = null;
                ViewBag.notice = "";
            }
            else
            {
                Session["scid"] = id;
                Tbl_school_notice ob = Dbcontext.Tbl_school_notice.Where(a => a.id == id).FirstOrDefault();
                ViewBag.notice = ob.notice;
            }
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveorUpdatenotice(string notice)
        {
            if (Session["scid"] == null)
            {
                Tbl_school_notice ob = new Tbl_school_notice();
                ob.notice = notice;
                ob.inserted_on = DateTime.Now;
                ob.isactive = true;
                Dbcontext.Tbl_school_notice.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["scid"]));
                Tbl_school_notice ob = Dbcontext.Tbl_school_notice.Where(a => a.id == id).FirstOrDefault();
                ob.notice = notice;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("notice", "Admin");
        }

        #endregion

        #region Information Booklet
        public ActionResult informationbooklet()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.informationbookletlist = Dbcontext.Tbl_Information_Booklet.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeinformationbooklet(int id)
        {
            var ob = Dbcontext.Tbl_Information_Booklet.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Information_Booklet obj = Dbcontext.Tbl_Information_Booklet.Where(a => a.id == id).FirstOrDefault();
                obj.isactive = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("informationbooklet", "Admin");
        }
        public ActionResult createoreditinformationbooklet(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["scid"] = null;
                ViewBag.title1 = "";
                ViewBag.pdf = null;
            }
            else
            {
                Session["scid"] = id;
                Tbl_Information_Booklet ob = Dbcontext.Tbl_Information_Booklet.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                ViewBag.pdf = "../../Images/informationbooklet/" + ob.pdf;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateinformationbooklet(string title, HttpPostedFileBase RegImage1)
        {
            if (Session["scid"] == null)
            {
                Tbl_Information_Booklet ob = new Tbl_Information_Booklet();
                ob.title = title;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/informationbooklet/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.pdf = fileName.ToString();
                }
                ob.inserted_on = DateTime.Now;
                ob.isactive = true;
                Dbcontext.Tbl_Information_Booklet.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["scid"]));
                Tbl_Information_Booklet ob = Dbcontext.Tbl_Information_Booklet.Where(a => a.id == id).FirstOrDefault();
                ob.title = title;
                if (RegImage1 != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(RegImage1.FileName.Replace(RegImage1.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/informationbooklet/"), fileName);
                    RegImage1.SaveAs(path);
                    ob.pdf = fileName.ToString();
                }
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("informationbooklet", "Admin");
        }

        #endregion

        #region Latest Updates
        public ActionResult latestupdates()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }

            ViewBag.latestupdateslist = Dbcontext.Tbl_latest_update.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removelatestupdates(int id)
        {
            var ob = Dbcontext.Tbl_latest_update.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_latest_update obj = Dbcontext.Tbl_latest_update.Where(a => a.id == id).FirstOrDefault();
                obj.isactive = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("latestupdates", "Admin");
        }
        public class latestupdateclass
        {

            public string name { get; set; }
            public string path { get; set; }
        }
        public ActionResult createoreditlatestupdate(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["luid"] = null;
                ViewBag.title1 = "";
                ViewBag.description = "";
                Session["latestupdateimages"] = null;
                ViewBag.images = null;
            }
            else
            {
                Session["luid"] = id;
                Tbl_latest_update ob = Dbcontext.Tbl_latest_update.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                ViewBag.description = ob.description;
                List<latestupdateclass> mn = new List<latestupdateclass>();
                var b = Dbcontext.Tbl_latest_update_images.Where(a => a.latestupdateid == id).ToList();
                mn = (from a in b
                      select new latestupdateclass
                      {
                          name = a.images,
                          path = "../../Images/latestupdates/" + a.images
                      }).ToList();
                Session["latestupdateimages"] = mn;
                ViewBag.images = mn;
            }
            return View();
        }
        public JsonResult Savelatestupdateimages(HttpPostedFileBase[] files)
        {
            List<latestupdateclass> mn = new List<latestupdateclass>();
            if (Session["latestupdateimages"] != null)
            {
                mn = Session["latestupdateimages"] as List<latestupdateclass>;
            }
            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(file.FileName.Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/latestupdates/"), fileName);
                    file.SaveAs(path);
                    latestupdateclass bb = new latestupdateclass();
                    bb.name = fileName.ToString();
                    bb.path = "../../Images/latestupdates/" + fileName.ToString();
                    mn.Add(bb);
                }

            }
            Session["latestupdateimages"] = mn;
            return Json(mn, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Removelatestupdateimages(string name)
        {
            List<latestupdateclass> mn = new List<latestupdateclass>();
            if (Session["latestupdateimages"] != null)
            {
                mn = Session["latestupdateimages"] as List<latestupdateclass>;
            }
            if (name != "")
            {
                latestupdateclass bb = mn.Where(a => a.name == name).FirstOrDefault();
                mn.Remove(bb);
                Session["latestupdateimages"] = mn;
            }
            Session["latestupdateimages"] = mn;
            return Json(mn, JsonRequestBehavior.AllowGet);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveorUpdatelatestupdates(string title, string description)
        {
            if (Session["luid"] == null)
            {
                Tbl_latest_update ob = new Tbl_latest_update();
                ob.title = title;
                ob.description = description;
                ob.inserted_on = DateTime.Now;
                ob.isactive = true;
                Dbcontext.Tbl_latest_update.Add(ob);
                Dbcontext.SaveChanges();
                int id = ob.id;
                List<latestupdateclass> mn = new List<latestupdateclass>();
                mn = Session["latestupdateimages"] as List<latestupdateclass>;
                foreach (latestupdateclass ss in mn)
                {
                    Tbl_latest_update_images im = new Tbl_latest_update_images();
                    im.latestupdateid = id;
                    im.images = ss.name;
                    im.inserted_on = DateTime.Now;
                    im.isactive = true;
                    Dbcontext.Tbl_latest_update_images.Add(im);
                    Dbcontext.SaveChanges();
                }
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["luid"]));
                Tbl_latest_update ob = Dbcontext.Tbl_latest_update.Where(a => a.id == id).FirstOrDefault();
                ob.title = title;
                ob.description = description;
                Dbcontext.SaveChanges();
                List<Tbl_latest_update_images> km = Dbcontext.Tbl_latest_update_images.Where(a => a.latestupdateid == id).ToList();
                foreach (Tbl_latest_update_images fd in km)
                {
                    //string filePath = "../../Images/latestupdates/" + fd.images;
                    //if (System.IO.File.Exists(filePath))
                    //{
                    //    System.IO.File.Delete(filePath);
                    //}
                    Dbcontext.Tbl_latest_update_images.Remove(fd);
                    Dbcontext.SaveChanges();
                }
                List<latestupdateclass> mn = new List<latestupdateclass>();
                mn = Session["latestupdateimages"] as List<latestupdateclass>;
                foreach (latestupdateclass ss in mn)
                {
                    Tbl_latest_update_images im = new Tbl_latest_update_images();
                    im.latestupdateid = id;
                    im.images = ss.name;
                    im.inserted_on = DateTime.Now;
                    im.isactive = true;
                    Dbcontext.Tbl_latest_update_images.Add(im);
                    Dbcontext.SaveChanges();
                }
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("latestupdates", "Admin");
        }

        #endregion
        #region Album
        public ActionResult album()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.albumlist = Dbcontext.Tbl_Album.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removealbum(int id)
        {
            var ob = Dbcontext.Tbl_Album.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Album obj = Dbcontext.Tbl_Album.Where(a => a.id == id).FirstOrDefault();
                obj.isactive = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("album", "Admin");
        }
        public class albumclass
        {

            public string name { get; set; }
            public string path { get; set; }
        }
        public ActionResult createoreditalbum(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                Session["alid"] = null;
                ViewBag.title1 = "";
                Session["albumimages"] = null;
                ViewBag.images = null;
            }
            else
            {
                Session["alid"] = id;
                Tbl_Album ob = Dbcontext.Tbl_Album.Where(a => a.id == id).FirstOrDefault();
                ViewBag.title1 = ob.title;
                List<albumclass> mn = new List<albumclass>();
                var b = Dbcontext.Tbl_Album_Images.Where(a => a.albumid == id).ToList();
                mn = (from a in b
                      select new albumclass
                      {
                          name = a.images,
                          path = "../../Images/gallery/" + a.images
                      }).ToList();
                Session["albumimages"] = mn;
                ViewBag.images = mn;
            }
            return View();
        }
        public JsonResult Savealbumimages(HttpPostedFileBase[] files)
        {
            List<albumclass> mn = new List<albumclass>();
            if (Session["albumimages"] != null)
            {
                mn = Session["albumimages"] as List<albumclass>;
            }
            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    string guid = Guid.NewGuid().ToString();
                    var fileName = Path.GetFileName(file.FileName.Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    var path = Path.Combine(Server.MapPath("~/Images/gallery/"), fileName);
                    file.SaveAs(path);
                    albumclass bb = new albumclass();
                    bb.name = fileName.ToString();
                    bb.path = "../../Images/gallery/" + fileName.ToString();
                    mn.Add(bb);
                }
            }
            Session["albumimages"] = mn;
            return Json(mn, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Removealbumimages(string name)
        {
            List<albumclass> mn = new List<albumclass>();
            if (Session["albumimages"] != null)
            {
                mn = Session["albumimages"] as List<albumclass>;
            }
            if (name != "")
            {
                albumclass bb = mn.Where(a => a.name == name).FirstOrDefault();
                mn.Remove(bb);
            }
            Session["albumimages"] = mn;
            return Json(mn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveorUpdatealbum(string title, string description)
        {
            if (Session["alid"] == null)
            {
                Tbl_Album ob = new Tbl_Album();
                ob.title = title;
                ob.inserted_on = DateTime.Now;
                ob.isactive = true;
                Dbcontext.Tbl_Album.Add(ob);
                Dbcontext.SaveChanges();
                int id = ob.id;
                List<albumclass> mn = new List<albumclass>();
                mn = Session["albumimages"] as List<albumclass>;
                foreach (albumclass ss in mn)
                {
                    Tbl_Album_Images im = new Tbl_Album_Images();
                    im.albumid = id;
                    im.images = ss.name;
                    im.inserted_on = DateTime.Now;
                    im.isactive = true;
                    Dbcontext.Tbl_Album_Images.Add(im);
                    Dbcontext.SaveChanges();
                }
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["alid"]));
                Tbl_Album ob = Dbcontext.Tbl_Album.Where(a => a.id == id).FirstOrDefault();
                ob.title = title;
                Dbcontext.SaveChanges();
                List<Tbl_Album_Images> km = Dbcontext.Tbl_Album_Images.Where(a => a.albumid == id).ToList();
                foreach (Tbl_Album_Images fd in km)
                {
                    Dbcontext.Tbl_Album_Images.Remove(fd);
                    Dbcontext.SaveChanges();
                }
                List<albumclass> mn = new List<albumclass>();
                if (Session["albumimages"] != null)
                {
                    mn = Session["albumimages"] as List<albumclass>;
                    foreach (albumclass ss in mn)
                    {
                        Tbl_Album_Images im = new Tbl_Album_Images();
                        im.albumid = id;
                        im.images = ss.name;
                        im.inserted_on = DateTime.Now;
                        im.isactive = true;
                        Dbcontext.Tbl_Album_Images.Add(im);
                        Dbcontext.SaveChanges();
                    }
                }
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("album", "Admin");
        }

        #endregion
    }
}