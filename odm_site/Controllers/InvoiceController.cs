﻿using ClosedXML.Excel;
using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class InvoiceController : Controller
    {
        DB_HREntities dbContext = new DB_HREntities();
        //
        // GET: /Invoice/
        public ActionResult Index()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            return View();
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public ActionResult DownloadSampleInvoiceExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee_Name", typeof(string));
            validationTable.Columns.Add("Department", typeof(string));
            validationTable.Columns.Add("Phone_Number", typeof(string));
            validationTable.Columns.Add("Email_ID", typeof(string));
            validationTable.Columns.Add("Purpose", typeof(string));
            validationTable.Columns.Add("Amount_1", typeof(decimal));
            validationTable.Columns.Add("Total_Amount", typeof(decimal));
            validationTable.TableName = "Invoice_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", "SampleInvoice.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadInvoice(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("Index");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Invoice_Details$]", connection);
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                Tbl_Invoice inv = new Tbl_Invoice();
                                var s = dbContext.Tbl_Invoice.Where(a => a.IsActive == true).ToList().LastOrDefault();
                                if(s==null)
                                {
                                    inv.Invoice_Code = 100001;
                                }
                                else
                                {
                                    inv.Invoice_Code = 100000+s.ID+1;
                                }
                                
                                inv.EmployeeName = dr["Employee_Name"].ToString();
                                inv.Department = dr["Department"].ToString();
                                inv.Mobile = dr["Phone_Number"].ToString();
                                inv.EmailId = dr["Email_ID"].ToString();
                                inv.Purpose = dr["Purpose"].ToString();
                                inv.Amount_1 = Convert.ToDecimal(dr["Amount_1"].ToString());
                                inv.Total_Amount = Convert.ToDecimal(dr["Total_Amount"].ToString());
                                inv.IsActive = true;
                                dbContext.Tbl_Invoice.Add(inv);
                                dbContext.SaveChanges();
                                string encode = Encodings.Base64EncodingMethod(inv.ID.ToString());
                                string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=invoice&sid=" + encode;
                                string shortlink = ToTinyURLS(url);
                                if (inv.Mobile != "")
                                {
                                    Sendsmstoemployee(inv.Mobile, "Dear " + inv.EmployeeName + ", \n We thank you for contributing whole heartedly to the nation, when the nation needed you the most. Please click on the link: " + shortlink + " to find your money receipt. We wish you and your family to stay safe. \n\n Regards\n ODM Educational Group");
                                }

                                if (inv.EmailId != "")
                                {
                                    string body = PopulateBody(inv.EmployeeName, shortlink);
                                    SendHtmlFormattedEmail(inv.EmailId, "Invoice | ODM Educational Group | ", body);
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                    TempData["SuccessMessage"] = "Invoice Details Entered Successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("Index");
            }
        }
        #region Mail and Messages

        public bool Sendsmstoemployee(string mobile, string message)
        {
            try
            {
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + message;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private string PopulateBody(string applicant, string url)
        {
            string body = string.Empty;
          //  string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/invoice.html")))
            {

                body = reader.ReadToEnd().Replace("{{applicant}}", applicant).Replace("{{href}}", url);

            }

            return body;
        }

        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }

        #endregion
        #region Bitly

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        #endregion
    }
}