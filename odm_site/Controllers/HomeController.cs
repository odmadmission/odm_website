﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odm_site.Models;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Threading;

namespace odm_site.Controllers
{
    public class HomeController : Controller
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();

        public class latestupdatesclass
        {
            public int id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string image { get; set; }
            public int comments { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
        }
        public class categoryclass
        {
            public int id { get; set; }
            public string category { get; set; }
            public string categoryid { get; set; }
            public List<studentresultclass> students { get; set; }
        }
        public class studentresultclass
        {
            public int id { get; set; }
            public string student_name { get; set; }
            public string image { get; set; }
            public string score { get; set; }
        }
        
        public ActionResult Index()
        {
           
            ViewBag.mainslider = Dbcontext.tbl_main_slider.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.campus = Dbcontext.Tbl_campus.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.parents = Dbcontext.Tbl_parents_testimonial.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on).Take(9);

            ViewBag.awards = Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true && a.is_viewinmainpage == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.affiliation = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "ACCREDITATIONS & RECOGNITIONS").ToList();
            ViewBag.learning = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "LEARNING PARTNERS").ToList();
            ViewBag.global = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "SERVICE PARTNERS").ToList();
            var blogs = Dbcontext.tbl_school_blog.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on).Take(9);

            var year = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).Select(a => a.year).Max().Value;
            ViewBag.resultyear = year;
            var rescatg = Dbcontext.Tbl_result_category.ToList();
            var restype = Dbcontext.Tbl_result_type.ToList();
            var resstudents = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            var distcatg = (from a in resstudents
                            where a.year == year
                            select new
                            {
                                a.categoryid,
                                a.typeid
                            }).ToList().Distinct();
            List<categoryclass> listcatg = new List<categoryclass>();
            foreach (var item in distcatg)
            {
                categoryclass catgg = new categoryclass();
                catgg.category = item.typeid != 0 ? (restype.Where(a => a.id == item.typeid).FirstOrDefault().name + (item.categoryid != 0 ? (" " + rescatg.Where(a => a.id == item.categoryid).FirstOrDefault().name) : "")) : "";
                catgg.categoryid = item.typeid != 0 ? (item.typeid + (item.categoryid != 0 ? ("_" + item.categoryid) : "")) : "";
                catgg.students = (from a in resstudents
                                  where (a.typeid == item.typeid) && (item.categoryid != 0 ? a.categoryid == item.categoryid : true)
                                  select new studentresultclass
                                  {
                                      student_name = a.student_name,
                                      image = a.image,
                                      score = a.score,
                                      id = a.id
                                  }).ToList();
                listcatg.Add(catgg);
            }
            ViewBag.categorydata = listcatg.FirstOrDefault().category;
            ViewBag.resultdetails = listcatg;
            var res = (from a in blogs
                       select new latestupdatesclass
                       {
                           id = a.id,
                           comments = Dbcontext.Tbl_Comment_On_Blog.Where(b => b.blog_id == a.id).ToList().Count(),
                           description = a.description,
                           image = a.image,
                           title = a.title,
                           inserted_on = a.inserted_on
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.latestupdates = res;
            ViewBag.latestupdatedetails = Dbcontext.Tbl_latest_update.Where(a=>a.isactive==true).ToList().OrderByDescending(a=>a.id);
            return View();
        }
        public ActionResult JoinODM()
        {

            ViewBag.mainslider = Dbcontext.tbl_main_slider.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.campus = Dbcontext.Tbl_campus.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.parents = Dbcontext.Tbl_parents_testimonial.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on).Take(9);

            ViewBag.awards = Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true && a.is_viewinmainpage == true).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.affiliation = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "ACCREDITATIONS & RECOGNITIONS").ToList();
            ViewBag.learning = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "LEARNING PARTNERS").ToList();
            ViewBag.global = Dbcontext.Tbl_School_Partners.Where(a => a.is_active == true && a.name == "SERVICE PARTNERS").ToList();
            var blogs = Dbcontext.tbl_school_blog.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on).Take(9);

            var year = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).Select(a => a.year).Max().Value;
            ViewBag.resultyear = year;
            var rescatg = Dbcontext.Tbl_result_category.ToList();
            var restype = Dbcontext.Tbl_result_type.ToList();
            var resstudents = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            var distcatg = (from a in resstudents
                            where a.year == year
                            select new
                            {
                                a.categoryid,
                                a.typeid
                            }).ToList().Distinct();
            List<categoryclass> listcatg = new List<categoryclass>();
            foreach (var item in distcatg)
            {
                categoryclass catgg = new categoryclass();
                catgg.category = item.typeid != 0 ? (restype.Where(a => a.id == item.typeid).FirstOrDefault().name + (item.categoryid != 0 ? (" " + rescatg.Where(a => a.id == item.categoryid).FirstOrDefault().name) : "")) : "";
                catgg.categoryid = item.typeid != 0 ? (item.typeid + (item.categoryid != 0 ? ("_" + item.categoryid) : "")) : "";
                catgg.students = (from a in resstudents
                                  where (a.typeid == item.typeid) && (item.categoryid != 0 ? a.categoryid == item.categoryid : true)
                                  select new studentresultclass
                                  {
                                      student_name = a.student_name,
                                      image = a.image,
                                      score = a.score,
                                      id = a.id
                                  }).ToList();
                listcatg.Add(catgg);
            }
            ViewBag.categorydata = listcatg.FirstOrDefault().category;
            ViewBag.resultdetails = listcatg;
            var res = (from a in blogs
                       select new latestupdatesclass
                       {
                           id = a.id,
                           comments = Dbcontext.Tbl_Comment_On_Blog.Where(b => b.blog_id == a.id).ToList().Count(),
                           description = a.description,
                           image = a.image,
                           title = a.title,
                           inserted_on = a.inserted_on
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.latestupdates = res;
            ViewBag.latestupdatedetails = Dbcontext.Tbl_latest_update.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.id);
            return View();
        }
        public string StripHTML(string input)
        {
            //if (!string.IsNullOrEmpty(input))
            //{
                input = System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", String.Empty);
                return input;
            //}
        }
        public JsonResult Getlatestblogs()
        {
            var blogs = Dbcontext.tbl_school_blog.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on).Take(9);
            var res = (from a in blogs
                       select new
                       {
                           id = a.id,
                           comments = Dbcontext.Tbl_Comment_On_Blog.Where(b => b.blog_id == a.id).ToList().Count(),
                           description = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(StripHTML(a.description).Length<150?StripHTML(a.description):StripHTML(a.description).Substring(0,150))),
                           image = a.image_thumb,
                           title = a.title,
                           inserted_on = a.inserted_on,
                           insertedOn = Convert.ToDateTime(a.inserted_on).ToString("dd MMM, yyyy")
                       }).ToList().OrderByDescending(a => a.inserted_on).Take(3);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult Recentblogs()
        {
            return PartialView();
        }
        public JsonResult Getresults(string id)
        {
            string[] sp = id.Split('_');
            var year = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).Select(a => a.year).Max().Value;
            var resstudents = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            var res = (from a in resstudents
                       where (a.typeid == Convert.ToInt32(sp[0])) && (a.year == year) && (sp.Length == 1 ? true : a.categoryid == Convert.ToInt32(sp[1]))
                       select new
                       {
                           student_name = a.student_name,
                           image = a.image,
                           score = a.score,
                           id = a.id
                       }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public class yearwisetypeclass
        {
            public int year { get; set; }
            public List<typewisecategoriesclass> types { get; set; }
        }
        public class typewisecategoriesclass
        {
            public int id { get; set; }
            public string type { get; set; }
            public List<Tbl_result_category> result { get; set; }
        }
        public ActionResult Allresults()
        {
            var year = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).Select(a => a.year).ToList().Distinct();
            var rescatg = Dbcontext.Tbl_result_category.ToList();
            var restype = Dbcontext.Tbl_result_type.ToList();
            var resstudents = Dbcontext.Tbl_School_Result.Where(a => a.is_active == true).ToList();
            List<yearwisetypeclass> ob = new List<yearwisetypeclass>();
            foreach (var item in year)
            {
                yearwisetypeclass oy = new yearwisetypeclass();
                oy.year = item.Value;
                oy.types = (from a in restype
                            select new typewisecategoriesclass
                            {
                                id = a.id,
                                type = a.name,
                                result = Dbcontext.Tbl_result_category.Where(b => b.is_active == true && b.typeid == a.id).ToList()
                            }).ToList();
                ob.Add(oy);
            }
            ViewBag.yeardetails = ob;
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult SaveContact(string fullname, string email, string phone, string Purpose, string message)
        {
            try
            {
                tbl_school_contact_us ob = new tbl_school_contact_us();
                ob.purpose = Purpose;
                ob.email = email;
                ob.inserted_on = DateTime.Now;
                ob.is_active = true;
                ob.message = message;
                ob.name = fullname;
                ob.phone = phone;
                Dbcontext.tbl_school_contact_us.Add(ob);
                Dbcontext.SaveChanges();
                contactbody(message, fullname, email, phone, "Contact Us", Purpose);
                return Json(ob.id, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        private void SendHtmlAdmissionEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("admission@odmegroup.org");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "admission@odmegroup.org";
                NetworkCred.Password = "admission@123";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
        private string contactbody(string message, string name, string email, string phone, string subject, string purpose)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Templates/contactustemplate.html")))
            {
                body = reader.ReadToEnd().Replace("{{message}}", message).Replace("{{name}}", name).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{purpose}}", purpose);
               SendHtmlFormattedEmail("info@odmps.org", subject, body);
             //   SendHtmlFormattedEmail("parismita@thedigichamps.com", subject, body);
            }

            return body;
        }
        public ActionResult Thankyou()
        {
           
            return View();
        }

        public JsonResult SaveFeedback(string knowaboutus)
        {
            try
            {
                int id = Convert.ToInt32(Session["cid"].ToString());
                tbl_school_contact_us ob = Dbcontext.tbl_school_contact_us.Where(a => a.id == id).FirstOrDefault();
                ob.feedback = knowaboutus;
                Dbcontext.SaveChanges();
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveAdmissionEnquiryFeedback(string knowaboutus)
        {
            try
            {
                int id = Convert.ToInt32(Session["admissionenquiryid"].ToString());
                Tbl_Admission_Enquiry ob = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.id == id).FirstOrDefault();
                ob.feedback = knowaboutus;
                Dbcontext.SaveChanges();
                Session["admissionenquiryid"] = null;
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public class School_Details
        {
            public int school_Id { get; set; }
            public string school_Name { get; set; }
            public List<Class_Details> classes { get; set; }

        }
        public class Class_Details
        {
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
        }

        public JsonResult GetSchoolsDetails()
        {
            var sch = Dbcontext.Tbl_School.Where(a => a.Is_Active == true).ToList();
            var cls = Dbcontext.Tbl_Class.ToList();
            var schools = (from a in sch.ToList()
                           select new School_Details
                           {
                               school_Id = a.school_Id,
                               school_Name = a.school_Name,
                               classes = (from b in cls.ToList()
                                          where b.school_Id == a.school_Id
                                          select new Class_Details
                                          {
                                              Class_Id = b.Class_Id,
                                              Class_Name = b.Class_Name
                                          }).ToList()
                           }).ToList();
            return Json(new { schools = schools }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult testimonial()
        {
            ViewBag.testimonials = Dbcontext.Tbl_parents_testimonial.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult Events()
        {
            ViewBag.events = Dbcontext.tbl_school_event.Where(a => a.is_active == true).ToList();
            return View();
        }
        public ActionResult Eventdetails(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Events", "Home");
            }
            ViewBag.eventdet = Dbcontext.tbl_school_event.Where(a => a.id == id).FirstOrDefault();
            return View();
        }
        public PartialViewResult _Admissionenquiry()
        {
            return PartialView();
        }
        public class albumclass
        {
            public int id { get; set; }
            public string name { get; set; }
            public string imgpath { get; set; }
            public List<Tbl_Album_Images> images { get; set; }
        }
        public async Task<ActionResult> photogallery()
        {
            var albumimages = Dbcontext.Tbl_Album.Where(a => a.isactive == true).ToList();
            var album = Dbcontext.Tbl_Album_Images.Where(b => b.isactive == true).ToList();
            List<albumclass> res = (from a in albumimages
                                    select new albumclass
                                    {
                                        id = a.id,
                                        imgpath = album.Where(b => b.albumid == a.id).FirstOrDefault() == null ? null : album.Where(b => b.albumid == a.id).FirstOrDefault().images,
                                        name = a.title
                                    }).ToList().OrderByDescending(a => a.id).ToList();
            return View(res);
        }
        public Task<List<albumclass>> GetalbumsAsync()  // Asynchronous Method
        {
            var albumimages = Dbcontext.Tbl_Album.Where(a => a.isactive == true).ToList();
            var album = Dbcontext.Tbl_Album_Images.Where(b => b.isactive == true).ToList();
            List<albumclass> res = (from a in albumimages
                                         select new albumclass
                                         {
                                             id = a.id,
                                             imgpath = album.Where(b => b.albumid == a.id).FirstOrDefault() == null ? null : album.Where(b => b.albumid == a.id).FirstOrDefault().images,
                                             name = a.title
                                         }).ToList().OrderByDescending(a => a.id).ToList();
            Thread.Sleep(100000);
            return Task.FromResult(res);

        }
        public JsonResult GetImages(int id)
        {
            var images = Dbcontext.Tbl_Album_Images.Where(b => b.isactive == true && b.albumid == id).ToList();
            return Json(images, JsonRequestBehavior.AllowGet);
        }
        public ActionResult mediagallery()
        {
            ViewBag.mediagallery = Dbcontext.tbl_media_gallery.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult blog()
        {
            ViewBag.blogs = Dbcontext.tbl_school_blog.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult admission_enquiry()
        {
            ViewBag.classlist = Dbcontext.Tbl_School_Class.Where(a => a.Is_Active == true).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult saveenquirydetails(string city, int classid, string description, DateTime dob, string emailid, string fathername, string firstname, string lastname, string phone, string presentschool)
        {
            Tbl_Admission_Enquiry ob = new Tbl_Admission_Enquiry();
            ob.city = city;
            ob.classid = classid;
            ob.description = description;
            ob.dob = dob;
            ob.email = emailid;
            ob.fathername = fathername;
            ob.firstname = firstname;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.lastname = lastname;
            ob.phone = phone;
            ob.presentschool = presentschool;
            ob.status = "Enquiry";
            Dbcontext.Tbl_Admission_Enquiry.Add(ob);
            Dbcontext.SaveChanges();
            Tbl_School_Class cls=Dbcontext.Tbl_School_Class.Where(a => a.Class_Id == classid).FirstOrDefault();
            enquirybody(description, firstname + " " + lastname, emailid, phone, "Admission Enquiry For Class-" + cls.Class_Name, fathername, cls.Class_Name, presentschool, dob.ToString("dd MMM yyyy"), city);
            return RedirectToAction("thankyoupage", "Home", new { id = ob.id });
        }
        private string enquirybody(string message, string name, string email, string phone, string subject, string fathername, string classname, string presentschool, string dob, string city)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Templates/admissionenquirytemplate.html")))
            {
                body = reader.ReadToEnd().Replace("{{message}}", message).Replace("{{name}}", name).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{fathername}}", fathername).Replace("{{classname}}", classname).Replace("{{presentschool}}", presentschool).Replace("{{dob}}", dob).Replace("{{city}}",city);
                SendHtmlAdmissionEmail("admission@odmegroup.org", subject, body);
                //SendHtmlFormattedEmail("parismita@thedigichamps.com", subject, body);
            }

            return body;
        }
        public ActionResult thankyoupage(int id)
        {
            Session["admissionenquiryid"] = id;
            return View();
        }
        public ActionResult onlinepayment()
        {
            return View();
        }
        public ActionResult blogdetails(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("blog", "Home");
            }
            else
            {
                var res = Dbcontext.tbl_school_blog.Where(a => a.id == id).FirstOrDefault();
                ViewBag.blogdet = res;
                return View();
            }
        }
        public class blogcomments
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string message { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
            public Nullable<bool> is_active { get; set; }
            public Nullable<int> blog_id { get; set; }
            public int totalreply { get; set; }
            public List<Tbl_Sub_Comments_On_Blog> subcomments { get; set; }
        }
        public JsonResult Getcomment(int id)
        {
            var ob = Dbcontext.Tbl_Comment_On_Blog.Where(a => a.is_active == true && a.id == id).FirstOrDefault();
            return Json(ob, JsonRequestBehavior.AllowGet);
        }
        public class commentdetails
        {
            public int totalcomment { get; set; }
            public List<blogcomments> comments { get; set; }
        }
        public JsonResult PostComments(int blogid, string email, string message, string name)
        {
            Tbl_Comment_On_Blog ob = new Tbl_Comment_On_Blog();
            ob.blog_id = blogid;
            ob.email = email;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.message = message;
            ob.name = name;
            Dbcontext.Tbl_Comment_On_Blog.Add(ob);
            Dbcontext.SaveChanges();
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PostSubComments(int commentid, string email, string message, string name)
        {
            Tbl_Sub_Comments_On_Blog ob = new Tbl_Sub_Comments_On_Blog();
            ob.comment_id = commentid;
            ob.email = email;
            ob.inserted_on = DateTime.Now;
            ob.is_active = true;
            ob.message = message;
            ob.name = name;
            Dbcontext.Tbl_Sub_Comments_On_Blog.Add(ob);
            Dbcontext.SaveChanges();
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCommentsDetails(string id)
        {
            tbl_school_blog blog = Dbcontext.tbl_school_blog.Where(a => a.title == id).FirstOrDefault();
            commentdetails ob = new commentdetails();
            ob.totalcomment = Dbcontext.Tbl_Comment_On_Blog.Where(a => a.is_active == true && a.blog_id == blog.id).ToList().Count();
            ob.comments = (from a in Dbcontext.Tbl_Comment_On_Blog.Where(a => a.is_active == true && a.blog_id == blog.id)
                           select new blogcomments
                           {
                               blog_id = a.blog_id,
                               email = a.email,
                               id = a.id,
                               inserted_on = a.inserted_on,
                               is_active = a.is_active,
                               message = a.message,
                               name = a.name,
                               totalreply = Dbcontext.Tbl_Sub_Comments_On_Blog.Where(b => b.is_active == true && b.comment_id == a.id).ToList().Count(),
                               subcomments = Dbcontext.Tbl_Sub_Comments_On_Blog.Where(b => b.is_active == true && b.comment_id == a.id).ToList()
                           }).ToList();
            return Json(ob, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Eventvideos()
        {
            ViewBag.eventvideolist = Dbcontext.Tbl_Event_Video.Where(a => a.isactive == true).ToList().OrderByDescending(a=>a.inserted_on);
            return View();
        }
        public ActionResult Notices()
        {
            ViewBag.noticelist = Dbcontext.Tbl_school_notice.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult InformationBooklet()
        {
            ViewBag.InformationBookletlist = Dbcontext.Tbl_Information_Booklet.Where(a => a.isactive == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public class latestupdatesdetailsclass
        {
            public int id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
            public string latestupdateimage { get; set; }
            public List<Tbl_latest_update_images> images { get; set; }
        }
        public ActionResult latestupdates()
        {
            var latestupdate = Dbcontext.Tbl_latest_update.Where(a => a.isactive == true).ToList();
            var res = (from a in latestupdate
                       select new latestupdatesdetailsclass {
                       inserted_on=a.inserted_on,
                       description=a.description,
                       id=a.id,
                       title=a.title,
                       latestupdateimage =Dbcontext.Tbl_latest_update_images.Where(b => b.latestupdateid == a.id && b.isactive == true).FirstOrDefault()!=null? Dbcontext.Tbl_latest_update_images.Where(b => b.latestupdateid == a.id && b.isactive == true).FirstOrDefault().images:null
                       }).ToList().OrderByDescending(a => a.inserted_on);
            ViewBag.latestupdatelist = res;
            ViewBag.Firstlatestupdate = res.FirstOrDefault();
            return View();
        }
        public ActionResult latestupdatedetails(int? id)
        {
            if(id==null)
            {
                return RedirectToAction("latestupdates", "Home");
            }
            var latestupdate = Dbcontext.Tbl_latest_update.Where(a => a.isactive == true).ToList();
            var res = (from a in latestupdate
                       where a.id==id
                       select new latestupdatesdetailsclass
                       {
                           inserted_on = a.inserted_on,
                           description = a.description,
                           id = a.id,
                           title = a.title,
                           images = Dbcontext.Tbl_latest_update_images.Where(b => b.latestupdateid == a.id && b.isactive == true).ToList()
                       }).FirstOrDefault();
            ViewBag.latestupdatedetails = res;
            return View();
        }
        public ActionResult privacypolicy()
        {
            return View();
        }
        public ActionResult sitemap()
        {          
            return View();
        }
    }
}