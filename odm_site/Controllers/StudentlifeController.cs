﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class StudentlifeController : Controller
    {
        // GET: Studentlife
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UN_Nodal_School()
        {
            return View();
        }
        public ActionResult Residential_Facility()
        {
            return View();
        }
        public ActionResult literary_activities()
        {
            return View();
        }
        public ActionResult art_and_cultural()
        {
            return View();
        }
        public ActionResult sports_and_wellness()
        {
            return View();
        }
        public ActionResult trips_and_expeditions()
        {
            return View();
        }
        public ActionResult Achievers_club()
        {
            return View();
        }
        public ActionResult community_service()
        {
            return View();
        }
        public ActionResult Green_India_Club()
        {
            return View();
        }
        public ActionResult Leap_series()
        {
            return View();
        }
        public ActionResult odm_media_club()
        {
            return View();
        }
    }
}