﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class ReUnionController : Controller
    {
        // GET: ReUnion
        DB_ODMSchoolEntities db = new DB_ODMSchoolEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult mobileverification()
        {
            if (Session["otp"] == null && Session["UserId"] == null && Session["Mobile"] == null)
            {
                return RedirectToAction("Register");
            }
            else
            {
                RegistrationMessagecls msg = new RegistrationMessagecls { id = Convert.ToInt32(Session["UserId"]), mobile = Session["Mobile"].ToString(), otp = Session["otp"].ToString() };
                return View(msg);
            }
        }
        public bool Sendsmstovisitor(string mobile, string message)
        {
            try
            {
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + message;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public JsonResult Checkmobile(string mobile)
        {
            try
            {
                Tbl_SchoolReunion union = db.Tbl_SchoolReunion.Where(a => a.mobile == mobile).FirstOrDefault();
                if (union == null)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveReunionDetails(OnlineRegistrationcls online)
        {
            try
            {
                Tbl_SchoolReunion union = new Tbl_SchoolReunion();
                union.city = online.city;
                union.Class_Name = online.classID;
                union.email = online.email;
                union.F_Name = online.fname;
                union.L_Name = online.lname;
                union.mobile = online.mobile;
                union.inserted_on = DateTime.Now;
                union.is_Active = true;
                union.modified_on = DateTime.Now;
                union.Parent_Name = online.pname;
                union.Passout_Year = online.passoutyear;
                union.status = "REGISTER";
                db.Tbl_SchoolReunion.Add(union);
                db.SaveChanges();
                string otp = random_password(6);
                bool sms = Sendotp(otp, online.mobile.ToString());
                Session["otp"] = otp;
                Session["UserId"] = union.id;
                Session["Mobile"] = online.mobile;
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e1)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult OTP_Verify(string otp, int id, string mobile)
        {
            try
            {
                if (otp == Session["otp"].ToString())
                {
                    bool msg = Sendsmstovisitor(mobile.ToString(), "Thank you for registering in School Reunion 2019 at ODM! Please note, only after the verification of your credentials, you will be sent an e-invite by the school team. Your verification will depend on whether you are a genuine student passed out of ODM from any session before 2015 (Either from Class X or Class XII)");
                    Tbl_SchoolReunion union = db.Tbl_SchoolReunion.Where(a => a.id == id).FirstOrDefault();
                    sendmailtoparents(union.F_Name + " " + union.L_Name, "School Re-Union", union.email);
                    return Json(1, JsonRequestBehavior.AllowGet);  //Successfully Verified
                }
                else
                {
                    return Json(2, JsonRequestBehavior.AllowGet);  //Incorrect otp
                }
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);  //Server Error
            }
        }
        public string sendmailtoparents(string parent, string subject, string sender)
        {
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/mailtemplate.html")))
            {
                body = reader.ReadToEnd().Replace("{{parent}}", parent);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("info@odmps.org");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "info@odmps.org";
                NetworkCred.Password = "odm2018@ps";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
        protected string random_password(int length)
        {
            try
            {
                const string valid = "1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public JsonResult ResendOTP(string phone)
        {
            try
            {
                string otp = random_password(6);
                bool sms = Sendsms(otp, phone.ToString());
                Session["otp"] = otp;
                //if (sms == true)
                //{
                return Json(1, JsonRequestBehavior.AllowGet);  //Successfully send
                //}
                //else
                //{
                //    return Json(3, JsonRequestBehavior.AllowGet);  // Not send sms
                //}
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);  //Server Error
            }
        }
        public bool Sendotp(string opt, string mobile)
        {
            try
            {
                string message = "OTP for ODM is {{otpno}}. Please use this OTP to register  for Online Application Form. Do note that the OTP expired in 1 hour. Team ODM.";
                var regex = new Regex(Regex.Escape("{{otpno}}"));
                var newText = regex.Replace(message, opt, 9);
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + newText;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Sendsms(string opt, string mobile)
        {
            try
            {
                string message = "OTP for ODM is {{otpno}}. Please use this OTP to Reset your password. OTP expires in 1 hour. ODM";
                var regex = new Regex(Regex.Escape("{{otpno}}"));
                var newText = regex.Replace(message, opt, 9);
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=DIGCMP&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + newText;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public ActionResult SuccessfullyRegister()
        {
            return View();
        }
        public ActionResult AdminLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AdminLogin(string Username, string Password)
        {
            if (Username == "Admin")
            {
                if (Password == "admin@1234")
                {
                    Session["user_id"] = 121;
                    return RedirectToAction("ReUnionList", "ReUnion");
                }
                else
                {
                    ViewBag.errormessage = "Incorrect Password... Please try again";
                }
            }
            else
            {
                ViewBag.errormessage = "Invalid Username and Password... Please try again";
            }
            return View("AdminLogin");
        }
        public ActionResult ReUnionList(string rdclass, string ddlpassout, string fromdate, string todate)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("AdminLogin", "ReUnion");
            }
            var s = db.Tbl_SchoolReunion.Where(a => a.is_Active == true).ToList();
            ViewBag.msg = "";
            if (rdclass != null && rdclass != "all")
            {
                s = s.Where(a => a.Class_Name == rdclass).ToList();
            }
            if (ddlpassout != null && ddlpassout != "all")
            {
                int yr = Convert.ToInt32(ddlpassout);
                s = s.Where(a => a.Passout_Year == yr).ToList();
            }
            if (fromdate != null && todate != null && fromdate != "" && todate != "")
            {
                DateTime fdt = Convert.ToDateTime(fromdate);
                DateTime tdt = Convert.ToDateTime(todate);
                if (fdt < tdt)
                {
                    s = s.Where(a => a.inserted_on >= fdt && a.inserted_on <= tdt).ToList();
                }
                else
                {
                    ViewBag.msg = "From Date must be less than To Date";
                }
            }
            ViewBag.reunionlist = s;
            return View();
        }
        public ActionResult LogOut()
        {
            return RedirectToAction("AdminLogin", "ReUnion");
        }
    }
}