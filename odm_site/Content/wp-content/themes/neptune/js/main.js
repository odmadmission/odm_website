﻿var scrollToID = function (id) {
    $('html, body, .main-body').animate({
        scrollTop: $('#' + id).offset().top - 15
    }, 500);
    return false;
};

$('.delete-confirm').click(function () {
    var link = $(this).attr('href');
    if (confirm("Are you sure you want to delete this item?") == true) {
        window.location(link);
    } else {
        return false;
    }
});

function init_gallery() {
    $(".gallery a").simpleLightbox()
}

var activity_grid_adjust = function () {
    if ($("#mqCheck-600").is(':visible')) {
        $('.activity-grid .item .wrapper').css('height', 'auto');
        var heights = [];
        $('.activity-grid .item .wrapper').each(function () {
            var elem = $(this);
            var height = elem.outerHeight();
            heights.push(height);
        });
        heights = heights.sort(function (a, b) { return b - a });
        var tallest = heights[0];
        $('.activity-grid .item .wrapper').css('height', tallest + 'px');
    } else {
        $('.activity-grid .item .wrapper').css('height', 'auto');
    }
};

var updates_slides_adjust = function () {
    if ($("#mqCheck-600").is(':visible')) {
        $('.updates-list .item').css('height', 'auto');
        var heights = [];
        $('.updates-list .item').each(function () {
            var elem = $(this);
            var height = elem.outerHeight();
            heights.push(height);
        });
        heights = heights.sort(function (a, b) { return b - a });
        var tallest = heights[0];
        $('.updates-list .item').css('height', tallest + 'px');
    } else {
        $('.updates-list .item').css('height', 'auto');
    }
};

$('.ajaxForm').submit(function (e) {
    e.preventDefault();

    var form = $(this);
    var submitButton = form.find('.formSubmit');
    var messageBox = form.find('.formMessage');

    var origButtonHTML = submitButton.html();
    submitButton.attr('disabled', 'disabled');
    submitButton.html('<span class="fa fa-spin fa-spinner"></span>');

    var postData = form.serializeArray();
    var formURL = form.attr('action');
    var methodType = form.attr('method');

    form.children('.form-group').removeClass('error');

    $.ajax({
        url: formURL,
        type: methodType,
        contentType: false,
        processData: false,
        data: new FormData(this),

        success: function (data, textStatus, jqXHR) {
            var response = jQuery.parseJSON(data);
            if (response.success === true) {
                messageBox.html("<div class='alert alert-success'>" + response.success_message + "</div>");
                submitButton.attr('disabled', 'disabled');
                submitButton.html("<span class='glyphicon glyphicon-thumbs-up'></span>");
                return;
            } else {
                var error_phrase = '<ul>';
                $(response.error_message).each(function (index, value) {
                    var key = Object.keys(value)[0];
                    $('#' + key).parent('.form-group').addClass('error');
                    //$('#'+key).after("<small class='help-block error text-danger'>" + value + "</small>")

                    error_phrase = error_phrase + '<li>' + value[key];
                    error_phrase = error_phrase + '</li>';
                });
                error_phrase += '</ul>';

                messageBox.html("<div class='alert alert-danger'>" + error_phrase + "</div>");
                submitButton.html(origButtonHTML);
                submitButton.removeAttr('disabled');
                return;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) { }
    });
});

$('.wheel-links > li > a').click(function (e) {
    scrollToID('ferris-container');
});

$('.timeline-slider .item').click(function (e) {
    e.preventDefault();
    var item = $(this);

    var date = item.attr('href');
    date = date.replace("#", '');
    date = parseInt(date);

    if (item.hasClass('active-year')) {
        return;
    }

    $('.timeline-slider .item').removeClass('active-year');
    item.addClass('active-year');

    var $slide = $('.milestones-slider #' + date + '-1');
    var slideIndex = $slide.data("slick-index");

    $(".milestones-slider .item").removeClass('active-year');
    $(".milestones-slider ." + date).addClass('active-year');
    $(".milestones-slider").slick("slickGoTo", slideIndex);

});

$(".testimonialModal").on("hidden.bs.modal", function () {
    var modal = $(this);
    var video = $(modal).find('.testimonialVideo');
    var src = video.attr('src');
    src = removeParam('autoplay', src);
    video.attr('src', src);
});

$(".testimonialModal").on("show.bs.modal", function () {
    var modal = $(this);
    var video = $(modal).find('.testimonialVideo');
    var src = video.attr('src');
    src += '&autoplay=1';
    video.attr('src', src);
});

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

$(window).scroll(function () {
    if ($(document).scrollTop() > 150) {
        $('aside#sticky').addClass('visible');
    } else {
        $('aside#sticky').removeClass('visible');
    }
});

$("#aside-toggle").click(function (e) {
    $('html, body, .main-body').animate({
        scrollTop: 0
    }, 500);

    if ($('#main-nav .navbar-collapse').hasClass('in')) {
        return false;
    }
    return;
});

$("#sidebar-toggle").click(function (e) {
    e.preventDefault();
    $(".sidebar-nav").toggleClass('open');
});

function sidebar_adjust() {
    if (!$("#mqCheck-767").is(':visible')) {
        var layout = $('#inner-layout');
        var toggle = $("#sidebar-toggle");

        var layoutOffset = layout.offset().top;
        var layoutHeight = layout.outerHeight();

        var sidebar = $("#sidebar-nav ul");
        var sidebarHeight = sidebar.outerHeight();

        var scrollPos = $(document).scrollTop();

        if (scrollPos > layoutOffset) {
            $(".sidebar-nav").addClass('active');
        } else {
            $(".sidebar-nav").removeClass('active');
        }

        if ((scrollPos >= (layoutOffset - 30)) && ((layoutHeight - (scrollPos - layoutOffset) - 30) >= (sidebarHeight))) {
            $(".sidebar-nav").addClass('fixed');
        } else {
            $(".sidebar-nav").removeClass('fixed');
        }

    } else {
        $(".sidebar-nav").removeClass('fixed');
    }
}

$("#inner-layout").on("swipeleft", function () {
    if (!$("#mqCheck-767").is(':visible')) {
        $(".sidebar-nav").addClass('open');
    }
});

/*
$("#inner-layout").swipe( {
	//Generic swipe handler for all directions
	swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
		if( ( ! $("#mqCheck-767").is(':visible')) && (direction == 'left') ) {
			$(".sidebar-nav").addClass('open');
		}
		if( ( ! $("#mqCheck-767").is(':visible')) && (direction == 'right') ) {
			$(".sidebar-nav").removeClass('open');
		}
		return;
	},
	excludedElements: "button, input, select, textarea, .noSwipe",
});
*/

/*
$("#inner-layout").swipe( { fingers:'all', allowPageScroll:"auto"} );
*/

$(document).on('ready scroll', function () {
    sidebar_adjust();
});

$(window).on('load resize', function () {
    sidebar_adjust();
});
