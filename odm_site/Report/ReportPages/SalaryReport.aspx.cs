﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using odm_site.Models;
using System.Globalization;
using CrystalDecisions.Shared;

namespace odm_site.Report.ReportPages
{
    public partial class SalaryReport : System.Web.UI.Page
    {
        DB_HREntities Dbcontext = new DB_HREntities();
        DB_ODMSchoolEntities context = new DB_ODMSchoolEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.QueryString["type"].ToString();
            switch (type)
            {
                case "Salaryslip":
                    string sid = Convert.ToString(Request.QueryString["sid"].ToString());
                    int id = Convert.ToInt32(Encodings.Base64DecodingMethod(sid));
                    DataTable dt = GetEmployeeDetails(id);
                    DataTable dt1 = GetEmployeeIncomeSalaryDetails(id);
                    DataTable dt2 = GetEmployeeDeductionSalaryDetails(id);
                    DataSet ds = new DataSet();
                    ds.Merge(dt.Copy());
                    //ds.Merge(dt1.Copy());
                    DataTable mdt = new DataTable();
                    int count = 0;
                    count = dt1.Rows.Count <= dt2.Rows.Count ? dt2.Rows.Count : dt1.Rows.Count;
                    mdt.Columns.Add("ICategory");
                    mdt.Columns.Add("IAmount");
                    mdt.Columns.Add("DCategory");
                    mdt.Columns.Add("DAmount");

                    for (int i = 0; i < count; i++)
                    {
                        DataRow dr = mdt.NewRow();
                        dr["ICategory"] = (dt1.Rows.Count > i) ? dt1.Rows[i]["Icategoryname"].ToString() : "";
                        dr["IAmount"] = (dt1.Rows.Count > i) ? dt1.Rows[i]["Iamount"].ToString() : "";
                        dr["DCategory"] = (dt2.Rows.Count > i) ? dt2.Rows[i]["Dcategoryname"].ToString() : "";
                        dr["DAmount"] = (dt2.Rows.Count > i) ? dt2.Rows[i]["Damount"].ToString() : "";
                        mdt.Rows.Add(dr);
                    }
                    mdt.TableName = "EmployeeSalaryDetails";
                    ds.Merge(mdt.Copy());
                    // ds.WriteXml(@"D:\parismita\XML_FILES\salarydetails.xml");

                    ReportDocument rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Report/pdf/salaryslip.rpt"));
                    rd.SetDataSource(ds);



                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "SalarySlip");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }

                    //System.IO.Stream m = null;
                    //m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //byte[] byteArray = null;
                    //byteArray = new byte[m.Length];
                    //m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    //rd.Close();
                    //rd.Dispose();
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";
                    //Response.BinaryWrite(byteArray);

                    break;
                case "invoice":
                    sid = Convert.ToString(Request.QueryString["sid"].ToString());
                    id = Convert.ToInt32(Encodings.Base64DecodingMethod(sid));
                    DataTable invoicedt = new DataTable();
                    var inv = Dbcontext.Tbl_Invoice.Where(a => a.ID == id).FirstOrDefault();
                    ds = new DataSet();
                    invoicedt.Columns.Add("Invoice_Code");
                    invoicedt.Columns.Add("Employee_Name");
                    invoicedt.Columns.Add("Department");
                    invoicedt.Columns.Add("Phone_Number");
                    invoicedt.Columns.Add("Email_ID");
                    invoicedt.Columns.Add("Purpose");
                    invoicedt.Columns.Add("Amount_1");
                    invoicedt.Columns.Add("Total_Amount");

                    DataRow drw = invoicedt.NewRow();
                    drw["Invoice_Code"] = inv.Invoice_Code;
                    drw["Employee_Name"] = inv.EmployeeName;
                    drw["Department"] = inv.Department;
                    drw["Phone_Number"] = inv.Mobile;
                    drw["Email_ID"] = inv.EmailId;
                    drw["Purpose"] = inv.Purpose;
                    drw["Amount_1"] = inv.Amount_1;
                    drw["Total_Amount"] = inv.Total_Amount;
                    invoicedt.Rows.Add(drw);

                    invoicedt.TableName = "InvoiceDetails";
                    ds.Merge(invoicedt.Copy());
                   // ds.WriteXml(@"E:\XML\invoicedetails.xml");

                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Report/pdf/invoice.rpt"));
                    rd.SetDataSource(ds);



                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Invoice");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }

                    //System.IO.Stream m = null;
                    //m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //byte[] byteArray = null;
                    //byteArray = new byte[m.Length];
                    //m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    //rd.Close();
                    //rd.Dispose();
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";
                    //Response.BinaryWrite(byteArray);

                    break;
                case "receipt":
                    sid = Convert.ToString(Request.QueryString["sid"].ToString());
                    id = Convert.ToInt32(Encodings.Base64DecodingMethod(sid));
                    invoicedt = new DataTable();
                    var receipts = context.Tbl_Receipt_Details.Where(a => a.Is_Active == true && a.Receipt_Id == id).ToList();
                    var studentdetails = context.Tbl_Student_Details.ToList();
                    var purposes = context.Tbl_Purpose.ToList();
                    var paymentmodelist = context.Tbl_Paymentmode.Distinct().ToList();
                    var res = (from a in receipts
                               join b in studentdetails on a.Student_Id equals b.Student_Id

                               select new
                               {
                                   Adjustment_Amount = a.Adjustment_Amount,
                                   Amount1 = (a.Amount1 == null && a.Amount1 == 0) ? "" : a.Amount1.ToString(),
                                   Amount2 = (a.Amount2 == null && a.Amount2 == 0) ? "" : a.Amount2.ToString(),
                                   Amount3 = (a.Amount3 == null && a.Amount3 == 0) ? "" : a.Amount3.ToString(),
                                   Amount4 = (a.Amount4 == null && a.Amount4 == 0) ? "" : a.Amount4.ToString(),
                                   Class_Name = b.Class_Name,
                                   Email_ID = b.Email_ID,
                                   Grand_Total = a.Grand_Total,
                                   inserted_on = a.inserted_on,
                                   Invoice_No = a.Invoice_No,
                                   Is_Active = a.Is_Active,
                                   modified_on = a.modified_on,
                                   Note = a.Note,
                                   Phone_No_1 = b.Phone_No_1,
                                   Phone_No_2 = b.Phone_No_2,
                                   Purpose_Name1 = (a.Purpose1 != null && a.Purpose1 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose1).FirstOrDefault().Purpose_Name : "",
                                   Purpose_Name2 = (a.Purpose2 != null && a.Purpose2 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose2).FirstOrDefault().Purpose_Name : "",
                                   Purpose_Name3 = (a.Purpose3 != null && a.Purpose3 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose3).FirstOrDefault().Purpose_Name : "",
                                   Purpose_Name4 = (a.Purpose4 != null && a.Purpose4 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose4).FirstOrDefault().Purpose_Name : "",
                                   Purpose1 = a.Purpose1,
                                   Purpose2 = a.Purpose2,
                                   Purpose3 = a.Purpose3,
                                   Purpose4 = a.Purpose4,
                                   Receipt_Id = a.Receipt_Id,
                                   School_Name = b.School_Name,
                                   School_No = b.School_No,
                                   Section_Name = b.Section_Name,
                                   Student_Id = a.Student_Id,
                                   Student_Name = b.Student_Name,
                                   Total_Amount = a.Total_Amount,
                                   Payment_Date = a.Payment_Date,
                                   Paymentmode_Id = a.Paymentmode_Id,
                                   Reference_No = a.Reference_No,
                                   Paymentmode = paymentmodelist.Where(h => h.Paymentmode_Id == a.Paymentmode_Id).FirstOrDefault().Paymentmode_Name
                               }).FirstOrDefault();
                    ds = new DataSet();
                    invoicedt.Columns.Add("Invoice_Code");
                    invoicedt.Columns.Add("Student_Name");
                    invoicedt.Columns.Add("School_Name");
                    invoicedt.Columns.Add("School_No");
                    invoicedt.Columns.Add("Class");
                    invoicedt.Columns.Add("Section");
                    invoicedt.Columns.Add("Phone_Number");
                    invoicedt.Columns.Add("Email_ID");
                    invoicedt.Columns.Add("Note");
                    invoicedt.Columns.Add("Purpose1");
                    invoicedt.Columns.Add("Purpose2");
                    invoicedt.Columns.Add("Purpose3");
                    invoicedt.Columns.Add("Purpose4");
                    invoicedt.Columns.Add("Amount1");
                    invoicedt.Columns.Add("Amount2");
                    invoicedt.Columns.Add("Amount3");
                    invoicedt.Columns.Add("Amount4");
                    invoicedt.Columns.Add("Grand_Total");
                    invoicedt.Columns.Add("Adjustment");
                    invoicedt.Columns.Add("Total");

                    //  drw = new DataRow();
                    drw = invoicedt.NewRow();
                    drw["Invoice_Code"] = res.Invoice_No;
                    drw["Student_Name"] = res.Student_Name;
                    drw["School_Name"] = res.School_Name;
                    drw["School_No"] = res.School_No;
                    drw["Class"] = res.Class_Name;
                    drw["Section"] = res.Section_Name;
                    drw["Phone_Number"] = res.Phone_No_1;
                    drw["Email_ID"] = res.Email_ID;
                    drw["Note"] = res.Note;
                    drw["Purpose1"] = res.Purpose_Name1;
                    drw["Purpose2"] = res.Purpose_Name2;
                    drw["Purpose3"] = res.Purpose_Name3;
                    drw["Purpose4"] = res.Purpose_Name4;
                    drw["Amount1"] = res.Amount1;
                    drw["Amount2"] = res.Amount2;
                    drw["Amount3"] = res.Amount3;
                    drw["Amount4"] = res.Amount4;
                    drw["Grand_Total"] = res.Grand_Total;
                    drw["Adjustment"] = res.Adjustment_Amount;
                    drw["Total"] = res.Total_Amount;
                    invoicedt.Rows.Add(drw);

                    invoicedt.TableName = "ReceiptDetails";
                    ds.Merge(invoicedt.Copy());
                    //  ds.WriteXml(@"F:\xmlfile\receiptdetails.xml");

                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Report/pdf/receipt.rpt"));
                    rd.SetDataSource(ds);
                   // TextObject txtobj =(TextObject)rd.ReportDefinition.ReportObjects["Text22"];
                    var refd = res.Payment_Date.Value.ToString("dd.MM.yyyy") + " / " + res.Reference_No;
                    rd.SetParameterValue("reference", refd);
                    rd.SetParameterValue("paymentmode", res.Paymentmode);
                    CrystalReportViewer1.ReportSource = rd;
                    
                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, res.Student_Name+"_"+res.Invoice_No);
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }
                    break;
                //default:
                //    string encode = Encodings.Base64EncodingMethod("8");
                //    Response.Redirect("~/Report/ReportPages/SalaryReport.aspx?type=Salaryslip&sid=" + encode + "");
                //    break;
            }
        }

        public DataTable GetEmployeeDetails(int id)
        {

            DataSet ds = new DataSet("EmployeeDetails");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("name");
            dt1.Columns.Add("designation");
            dt1.Columns.Add("department");
            dt1.Columns.Add("monthyear");
            dt1.Columns.Add("totalsalary");
            dt1.Columns.Add("netdeduction");
            dt1.Columns.Add("netsalary");
            dt1.Columns.Add("organisation");
            DataRow dr;
            dr = dt1.NewRow();

            Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
            Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == ob.emp_id).FirstOrDefault();
            dr["name"] = ob1.emp_name;
            dr["designation"] = ob1.designation;
            dr["department"] = ob1.dept_id != null ? Dbcontext.Tbl_Department.Where(a => a.id == ob1.dept_id).FirstOrDefault().name : "";
            dr["monthyear"] = DateTimeFormatInfo.InvariantInfo.MonthNames[ob.month.Value - 1] + " " + ob.year.Value;
            dr["totalsalary"] = ob.total_salary;
            dr["netdeduction"] = ob.net_deduction;
            dr["netsalary"] = ob.net_salary;
            dr["organisation"] = ob1.organisation_id != null ? Dbcontext.Tbl_Organisation.Where(a => a.id == ob1.organisation_id).FirstOrDefault().name : "";
            dt1.Rows.Add(dr);
            dt1.TableName = "EmployeeDetails";
            return dt1;

        }
        public DataTable GetEmployeeIncomeSalaryDetails(int id)
        {

            DataSet ds = new DataSet("EmployeeIncomeSalaryDetails");
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Icategoryname");
            dt1.Columns.Add("Iamount");
            DataRow dr;
            Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
            var obdt = Dbcontext.Tbl_Employee_Salary_Details.Where(a => a.emp_salary_id == id && a.is_active == true).ToList();
            Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == ob.emp_id).FirstOrDefault();
            var categorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
            var catgdetailslist = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true && a.type_id == ob1.type_id).ToList();
            var res = (from a in categorylist
                       join b in catgdetailslist on a.id equals b.salary_category_id
                       where a.id == 1
                       select new
                       {
                           categorydetailsname = b.name,
                           amount = obdt.Where(m => m.emp_category_details_id == b.id).FirstOrDefault().amount
                       }).ToList();
            foreach (var item in res)
            {
                dr = dt1.NewRow();
                dr["Icategoryname"] = item.categorydetailsname;
                dr["Iamount"] = item.amount;
                dt1.Rows.Add(dr);
            }
            dt1.TableName = "EmployeeIncomeSalaryDetails";
            return dt1;

        }
        public DataTable GetEmployeeDeductionSalaryDetails(int id)
        {

            DataSet ds = new DataSet("EmployeeDeductionSalaryDetails");
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Dcategoryname");
            dt1.Columns.Add("Damount");
            DataRow dr;
            Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
            Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == ob.emp_id).FirstOrDefault();
            var obdt = Dbcontext.Tbl_Employee_Salary_Details.Where(a => a.emp_salary_id == id && a.is_active == true).ToList();
            var categorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
            var catgdetailslist = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true && a.type_id == ob1.type_id).ToList();
            var res = (from a in categorylist
                       join b in catgdetailslist on a.id equals b.salary_category_id
                       where a.id == 2
                       select new
                       {
                           categorydetailsname = b.name,
                           amount = obdt.Where(m => m.emp_category_details_id == b.id).FirstOrDefault().amount
                       }).ToList();
            foreach (var item in res)
            {
                dr = dt1.NewRow();
                dr["Dcategoryname"] = item.categorydetailsname;
                dr["Damount"] = item.amount;
                dt1.Rows.Add(dr);
            }
            dr = dt1.NewRow();
            dr["Dcategoryname"] = "";
            dr["Damount"] = "";
            dt1.Rows.Add(dr);
            dr = dt1.NewRow();
            dr["Dcategoryname"] = "Net Deduction";
            dr["Damount"] = ob.net_deduction;
            dt1.Rows.Add(dr);
            dt1.TableName = "EmployeeDeductionSalaryDetails";
            return dt1;

        }

    }
}