//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Applicant_Status
    {
        public int Applicant_Status_Id { get; set; }
        public string Applicant_Status { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
    }
}
