//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Visitor_for_hostel_student
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public Nullable<int> Student_Id { get; set; }
        public Nullable<int> Class_Id { get; set; }
        public string Purpose { get; set; }
        public string Place { get; set; }
        public Nullable<System.DateTime> Visit_Date { get; set; }
        public Nullable<System.TimeSpan> Checkin_Time { get; set; }
        public Nullable<System.TimeSpan> Checkout_Time { get; set; }
        public string Total_Time { get; set; }
        public string Status { get; set; }
        public string Student_Status { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Isapprovedbyparents { get; set; }
        public Nullable<bool> ischeckin { get; set; }
    }
}
