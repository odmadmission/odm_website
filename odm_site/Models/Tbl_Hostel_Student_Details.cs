//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Hostel_Student_Details
    {
        public int Hostel_Student_Id { get; set; }
        public Nullable<int> Student_Id { get; set; }
        public Nullable<System.DateTime> Joining_Date { get; set; }
        public Nullable<System.DateTime> Leaving_Date { get; set; }
        public Nullable<int> Room_Id { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public Nullable<bool> Is_Active { get; set; }
    }
}
