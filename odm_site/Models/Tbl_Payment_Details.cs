//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Payment_Details
    {
        public int Payment_ID { get; set; }
        public string Payment_No { get; set; }
        public string Trans_No { get; set; }
        public Nullable<int> User_Id { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Amt_In_Words { get; set; }
        public string Payment_Mode { get; set; }
        public Nullable<bool> Is_Paid { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> Inserted_Date { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<System.DateTime> Payment_Date { get; set; }
    }
}
