//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Student_Personal_Details
    {
        public int Student_Id { get; set; }
        public string Form_No { get; set; }
        public Nullable<int> User_Id { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public string Category { get; set; }
        public string Mother_Tongue { get; set; }
        public string Languages_Known { get; set; }
        public string Passport_No { get; set; }
        public Nullable<System.DateTime> Passport_Expiry_Date { get; set; }
        public string Aadhar_No { get; set; }
        public string Photo { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public string Email_Id { get; set; }
    }
}
