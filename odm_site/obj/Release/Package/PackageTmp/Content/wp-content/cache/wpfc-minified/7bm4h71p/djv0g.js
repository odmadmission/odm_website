﻿function ffwd_frontend_ajax(form_id, current_view, id, album_id, enter_or_back, type, srch_btn, title, sortByParam, load_more, image_onclick_action) {
    var page_number = jQuery("#page_number_" + current_view).val();
    var ffwd_load_more = jQuery("#ffwd_load_more_" + current_view).val();
    var ffwd_previous_album_ids = jQuery('#ffwd_previous_album_id_' + current_view).val();
    var ffwd_previous_album_page_numbers = jQuery('#album_page_number_' + current_view).val();
    var masonry_already_loaded = jQuery(".ffwd_masonry_thumb_cont_" + current_view + " img").length;
    if (typeof load_more == "undefined") {
        var load_more = false;
    }
    var post_data = {};
    if (album_id == 'back') { // Back from album.
        var album_page_number = ffwd_previous_album_page_numbers.split(",");
        page_number = album_page_number[0];
        jQuery('#album_page_number_' + current_view).val(ffwd_previous_album_page_numbers.replace(album_page_number[0] + ',', ''));
    }
    else if (enter_or_back != '') { // Enter album (not change the page).
        jQuery('#ffwd_previous_album_id_' + current_view).val(enter_or_back + ',' + ffwd_previous_album_ids);
        if (page_number) {
            jQuery('#album_page_number_' + current_view).val(page_number + ',' + ffwd_previous_album_page_numbers);
        }
        page_number = 1;
    }
    if (typeof title == "undefined") {
        var title = "";
    }
    if (typeof sortByParam == "undefined") {
        var sortByParam = jQuery(".bwg_order_" + current_view).val();
    }
    post_data["page_number_" + current_view] = page_number;
    post_data["ffwd_load_more_" + current_view] = ffwd_load_more;
    post_data["album_id_" + current_view] = album_id;
    post_data["ffwd_previous_album_id_" + current_view] = jQuery('#ffwd_previous_album_id_' + current_view).val();
    post_data["album_page_number_" + current_view] = jQuery('#album_page_number_' + current_view).val();
    post_data["type_" + current_view] = type;
    post_data["title_" + current_view] = title;
    post_data["sortImagesByValue_" + current_view] = sortByParam;
    // Loading.
    jQuery("#ffwd_ajax_loading_" + current_view).css('display', '');
    jQuery.post(
		window.location,
		post_data,
		function (data) {
		    if (load_more) {
		        var strr = jQuery(data).find('#' + id).html();
		        jQuery('#' + id).append(strr);
		        var str = jQuery(data).find('.ffwd_nav_cont_' + current_view).html();
		        jQuery('.ffwd_nav_cont_' + current_view).html(str);
		    }
		    else {
		        var str = jQuery(data).find('#' + form_id).html();
		        jQuery('#' + form_id).html(str);
		    }
		}
	).success(function (jqXHR, textStatus, errorThrown) {
	    jQuery("#ffwd_ajax_loading_" + current_view).css('display', 'none');
	    if (jQuery(".pagination-links_" + current_view).length) {
	        jQuery("html, body").animate({ scrollTop: jQuery('#' + form_id).offset().top - 150 }, 500);
	    }
	    /* For all*/
	    window["ffwd_document_ready_" + current_view]();
	    /* For masonry view.*/
	    if (id == "ffwd_masonry_thumbnails_" + current_view || id == "ffwd_album_masonry_" + current_view) {
	        window["ffwd_masonry_ajax_" + current_view](masonry_already_loaded);
	    }
	});
    return false;
}
/* For thumnail amd masonry view */
function ffwd_fill_likes_thumnail(id_object_id, ffwd, graph_url) {
    var ids_array_count = Math.ceil(id_object_id.length / 50);
    var ids_array = [];
    var img_ids = [];
    for (var i = 0; i < ids_array_count; i++) {
        ids_array[i] = [];
        k = 0;
        for (var j = i * 50; j < (i + 1) * 50; j++) {
            if (!id_object_id[j])
                break;

            ids_array[i][k] = [];
            var object_id = id_object_id[i]['from'] + '_' + id_object_id[j]['object_id']


            ids_array[i][k] = object_id;
            img_ids[object_id] = id_object_id[j]['id'];
            k++;
        }

    }

    for (var i = 0; i < ids_array.length; i++) {

        ids = ids_array[i].join(',');

        url_for_cur_id_likes_thumb = graph_url.replace('{FB_ID}', ''),

			graph_url_for_likes_thumb = url_for_cur_id_likes_thumb.replace('{EDGE}', '');
        graph_url_for_likes_thumb = graph_url_for_likes_thumb.replace('{FIELDS}', 'ids=' + ids + '&fields=comments.summary(true).limit(0),likes.summary(true).limit(0)&');
        graph_url_for_likes_thumb = graph_url_for_likes_thumb.replace('{OTHER}', '');

        jQuery.getJSON(graph_url_for_likes_thumb,
			function (result) {

			    for (var j in result) {

			        ffwd_fill_likes_comments_thumb(result[j], ffwd, img_ids[j]);


			    }

			}
		);


    }


}

function createCallback_thumbnail(id, ffwd, type) {
    return function (result) {
        do_something_with_data_thumbnail(result, id, ffwd, type);
    };
}

function ffwd_fill_likes_comments_thumb(result, ffwd, id) {

    var likes_count = (typeof result['likes']['summary'] != 'undefined') ? parseInt(result['likes']['summary']['total_count']) : '0';
    jQuery('#ffwd_likes_' + ffwd + '_' + id + ' span').html(likes_count);

    var comments_count = (typeof result['comments']['summary'] != 'undefined') ? parseInt(result['comments']['summary']['total_count']) : '0';
    jQuery('#ffwd_comments_' + ffwd + '_' + id + ' span').html(comments_count);

}

function do_something_with_data_thumbnail(result, id, ffwd, type) {
    switch (type) {
        case 'likes':
            var likes_count = (typeof result['summary'] != 'undefined') ? parseInt(result['summary']['total_count']) : '0';
            jQuery('#ffwd_likes_' + ffwd + '_' + id + ' span').html(likes_count);
            break;
        case 'comments':
            var comments_count = (typeof result['summary'] != 'undefined') ? parseInt(result['summary']['total_count']) : '0';
            jQuery('#ffwd_comments_' + ffwd + '_' + id + ' span').html(comments_count);
            break;
        default:
            console.log('error');
            break;
    }
}

/* For album view */
function ffwd_fill_thum_srs_likes_compact_album(id_object_id, ffwd, graph_url, ffwd_album_info, image_onclick_action) {
    if (typeof id_object_id == 'object') {
        /*Album*/


        var ids_array_count = Math.ceil(id_object_id.length / 50);
        var ids_array = [];
        var img_ids = [];
        for (var i = 0; i < ids_array_count; i++) {
            ids_array[i] = [];
            k = 0;
            for (var j = i * 50; j < (i + 1) * 50; j++) {
                if (!id_object_id[j])
                    break;

                ids_array[i][k] = [];
                var object_id = id_object_id[j]['object_id']


                ids_array[i][k] = object_id;
                img_ids[object_id] = id_object_id[j]['id'];
                k++;
            }

        }

        for (var i = 0; i < ids_array.length; i++) {

            ids = ids_array[i].join(',');

            url_for_cur_id_likes_thumb = graph_url.replace('{FB_ID}', ''),

				graph_url_for_likes_thumb = url_for_cur_id_likes_thumb.replace('{EDGE}', 'photos');
            graph_url_for_likes_thumb = graph_url_for_likes_thumb.replace('{FIELDS}', 'ids=' + ids + '&fields=source,width,height,count,link&');
            graph_url_for_likes_thumb = graph_url_for_likes_thumb.replace('{OTHER}', '');
            jQuery.getJSON(graph_url_for_likes_thumb,
				function (result) {

				    for (var j in result) {

				        ffwd_fill_likes_comments_c_album(result[j], ffwd, img_ids[j], ffwd_album_info, image_onclick_action);


				    }

				}
			);


        }




    }
    else {
        /*Gallery*/
        var album_id = id_object_id,
			graph_url_for_album_photos = graph_url.replace('{EDGE}', 'photos'),
			url_for_album_photos = graph_url_for_album_photos.replace('{FB_ID}', album_id),
			url_for_album_photos = url_for_album_photos.replace('{FIELDS}', 'fields=images,link,from{picture,name,link}&');


        jQuery.getJSON(url_for_album_photos, createCallback_album('', ffwd, 'photos', graph_url, ffwd_album_info, image_onclick_action));

    }
}



function ffwd_fill_likes_comments_c_album(result, ffwd, id, ffwd_album_info, image_onclick_action) {
    var height = (typeof result['data'][0] != 'undefined') ? result['data'][0]['height'] : 0,
		width = (typeof result['data'][0] != 'undefined') ? result['data'][0]['width'] : 0,
		image_thumb_width,
		image_thumb_height,
		resolution_w,
		resolution_h,
		count = (typeof result['data'] != 'undefined') ? result['data'].length : 0,
		scale;
    if (count) {
        if (width && height) {
            resolution_w = width;
            resolution_h = height;
            if (resolution_w != 0 && resolution_h != 0) {
                scale = Math.max(ffwd_album_info["album_thumb_width"] / resolution_w, ffwd_album_info["album_thumb_height"] / resolution_h);
                image_thumb_width = resolution_w * scale;
                image_thumb_height = resolution_h * scale;
            }
            else {
                image_thumb_width = ffwd_album_info["album_thumb_width"];
                image_thumb_height = ffwd_album_info["album_thumb_height"];
            }
        }
        else {
            image_thumb_width = ffwd_album_info["album_thumb_width"];
            image_thumb_height = ffwd_album_info["album_thumb_height"];
        }
        scale = Math.max(ffwd_album_info["album_thumb_width"] / image_thumb_width, image_thumb_height / image_thumb_height);
        image_thumb_width *= scale;
        image_thumb_height *= scale;
        thumb_left = (ffwd_album_info["album_thumb_width"] - image_thumb_width) / 2;
        thumb_top = (ffwd_album_info["album_thumb_height"] - image_thumb_height) / 2;

        jQuery('#ffwd_album_cover_' + id + '_' + ffwd).attr('src', result['data'][0]['source']).css({
            'width': image_thumb_width + 'px',
            'height': image_thumb_height + 'px',
            'margin-left': thumb_left + 'px',
            'margin-top': thumb_top + 'px',
        });
    }
    else {
        /*jQuery( "[ffwd_object_id='"+object_id+"']" ).remove();*/
    }
}


function createCallback_album(id, ffwd, type, graph_url, ffwd_album_info, image_onclick_action) {
    return function (result) {
        do_something_with_data_album(result, id, ffwd, type, graph_url, ffwd_album_info, image_onclick_action);
    };
}

function ffwd_fill_likes_comments_c_gallery(result, ffwd, id) {
    var likes_count = (typeof result['likes']['summary'] != 'undefined') ? parseInt(result['likes']['summary']['total_count']) : '0';
    jQuery('#ffwd_likes_' + id + '_' + ffwd + ' span').html(likes_count);


    var comments_count = (typeof result['comments']['summary'] != 'undefined') ? parseInt(result['comments']['summary']['total_count']) : '0';
    jQuery('#ffwd_comments_' + id + '_' + ffwd + ' span').html(comments_count);
}


function do_something_with_data_album(result, id, ffwd, type, graph_url, ffwd_album_info, image_onclick_action) {

    switch (type) {
        case 'likes':
        case 'comments':
            var likes_count = (typeof result['likes']['summary'] != 'undefined') ? parseInt(result['likes']['summary']['total_count']) : '0';
            jQuery('#ffwd_likes_' + id + '_' + ffwd + ' span').html(likes_count);


            var comments_count = (typeof result['comments']['summary'] != 'undefined') ? parseInt(result['comments']['summary']['total_count']) : '0';
            jQuery('#ffwd_comments_' + id + '_' + ffwd + ' span').html(comments_count);
            break;
        case 'covers':
            var height = (typeof result['data'][0] != 'undefined') ? result['data'][0]['height'] : 0,
				width = (typeof result['data'][0] != 'undefined') ? result['data'][0]['width'] : 0,
				image_thumb_width,
				image_thumb_height,
				resolution_w,
				resolution_h,
				count = (typeof result['data'] != 'undefined') ? result['data'].length : 0,
				scale;
            if (count) {
                if (width && height) {
                    resolution_w = width;
                    resolution_h = height;
                    if (resolution_w != 0 && resolution_h != 0) {
                        scale = Math.max(ffwd_album_info["album_thumb_width"] / resolution_w, ffwd_album_info["album_thumb_height"] / resolution_h);
                        image_thumb_width = resolution_w * scale;
                        image_thumb_height = resolution_h * scale;
                    }
                    else {
                        image_thumb_width = ffwd_album_info["album_thumb_width"];
                        image_thumb_height = ffwd_album_info["album_thumb_height"];
                    }
                }
                else {
                    image_thumb_width = ffwd_album_info["album_thumb_width"];
                    image_thumb_height = ffwd_album_info["album_thumb_height"];
                }
                scale = Math.max(ffwd_album_info["album_thumb_width"] / image_thumb_width, image_thumb_height / image_thumb_height);
                image_thumb_width *= scale;
                image_thumb_height *= scale;
                thumb_left = (ffwd_album_info["album_thumb_width"] - image_thumb_width) / 2;
                thumb_top = (ffwd_album_info["album_thumb_height"] - image_thumb_height) / 2;

                jQuery('#ffwd_album_cover_' + id + '_' + ffwd).attr('src', result['data'][0]['source']).css({
                    'width': image_thumb_width + 'px',
                    'height': image_thumb_height + 'px',
                    'margin-left': thumb_left + 'px',
                    'margin-top': thumb_top + 'px',
                });
            }
            else {
                /*jQuery( "[ffwd_object_id='"+object_id+"']" ).remove();*/
            }
            break;
        case 'photos':
            var graph_url_for_likes = graph_url.replace('{EDGE}', ''),
				graph_url_for_likes = graph_url_for_likes.replace('{FIELDS}', ''),
				graph_url_for_comments = graph_url.replace('{EDGE}', ''),
				graph_url_for_comments = graph_url_for_comments.replace('{FIELDS}', '&fields=likes.summary(true).limit(0),comments.summary(true).limit(0)&');

            var data = result['data'];

            content = '';
            ffwd_album_info["data"] = [];
            curent_view = ffwd;


            var ids_array_count = Math.ceil(data.length / 50);
            var ids_array = [];
            var img_ids = [];
            for (var j = 0; j < ids_array_count; j++) {
                ids_array[j] = [];
                k = 0;
                for (var i = j * 50; i < (j + 1) * 50; i++) {
                    if (!data[i])
                        break;

                    ids_array[j][k] = [];
                    var object_id = data[i]['from']['id'] + '_' + data[i]['id']


                    ids_array[j][k] = object_id;
                    img_ids[object_id] = i;
                    k++;
                    //////////////////////////////////////////////////////
                    var row = {},
						images = data[i]['images'],
						image_obj_id = data[i]['id'];
                    index_in_images = (images.length > 3) ? 2 : 0,
						image = data[i]['images'][index_in_images];
                    row['id'] = i;
                    row['object_id'] = image_obj_id;
                    row['from'] = data[i]['from']['id'];
                    ffwd_album_info["data"].push(row);

                    var height = image['height'],
						width = image['width'],
						image_thumb_width,
						image_thumb_height,
						resolution_w,
						resolution_h,
						scale;

                    if (typeof width != 'undefined' && typeof height != 'undefined') {
                        resolution_w = width;
                        resolution_h = height;
                        if (resolution_w != 0 && resolution_h != 0) {
                            scale = Math.max(ffwd_album_info["thumb_width"] / resolution_w, ffwd_album_info["thumb_height"] / resolution_h);
                            image_thumb_width = resolution_w * scale;
                            image_thumb_height = resolution_h * scale;
                        }
                        else {
                            image_thumb_width = ffwd_album_info["thumb_width"];
                            image_thumb_height = ffwd_album_info["thumb_height"];
                        }
                    }
                    else {
                        image_thumb_width = ffwd_album_info["thumb_width"];
                        image_thumb_height = ffwd_album_info["thumb_height"];
                    }

                    scale = Math.max(ffwd_album_info["thumb_width"] / image_thumb_width, image_thumb_height / image_thumb_height);
                    image_thumb_width *= scale;
                    image_thumb_height *= scale;
                    thumb_left = (ffwd_album_info["thumb_width"] - image_thumb_width) / 2;
                    thumb_top = (ffwd_album_info["thumb_height"] - image_thumb_height) / 2;

                    main_url = image.source;

                    if (image_onclick_action == 'facebook') {
                        main_url = data[i]['link'];
                    }

                    if (image_onclick_action == 'none') {
                        main_url = '#';
                    }


                    content += '<a  class="ffwd_lightbox_' + curent_view + '" href="' + main_url + '" data-image-id="' + i + '" data-image-obj-id="' + image_obj_id + '" >' +
						'<div class="ffwd_standart_thumb_' + curent_view + '">' +
						'<div class="ffwd_standart_thumb_spun1_' + curent_view + '">' +
						'<div class="ffwd_standart_thumb_spun2_' + curent_view + '">' +
						'<img class="ffwd_standart_thumb_img_' + curent_view + '" style="width:' + image_thumb_width + 'px; height:' + image_thumb_height + 'px; margin-left: ' + thumb_left + 'px; margin-top: ' + thumb_top + 'px;" id="" src="' + image.source + '" alt="" />' +
						'<div class="ffwd_likes_comments_container_' + curent_view + '" >' +
						'<div class="ffwd_likes_comments_container_tab_' + curent_view + '" >' +
						'<div class="ffwd_likes_comments_' + curent_view + '" >' +
						'<div id="ffwd_likes_' + i + '_' + curent_view + '" class="ffwd_likes_' + curent_view + '">' +
						'<span></span>' +
						'</div>' +
						'<div id="ffwd_comments_' + i + '_' + curent_view + '" class="ffwd_comments_' + curent_view + '">' +
						'<span></span>' +
						'</div>' +
						'<div style="clear:both"></div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</a>';

                    var url_for_likes = graph_url_for_likes.replace('{FB_ID}', data[i]['from']['id'] + '_' + data[i]['id']),
						url_for_comments = graph_url_for_comments.replace('{FB_ID}', data[i]['from']['id'] + '_' + data[i]['id']);

                    //jQuery.getJSON(url_for_likes, createCallback_album(i, ffwd, 'likes'));


                    // jQuery.getJSON(url_for_comments, createCallback_album(i, ffwd, 'comments'));

                }

            }

            //console.log(img_ids);

            for (var i = 0; i < ids_array.length; i++) {

                ids = ids_array[i].join(',');



                var graph_url_for_comments = graph_url.replace('{EDGE}', '');
                graph_url_for_comments = graph_url_for_comments.replace('{FB_ID}', '')
                graph_url_for_comments = graph_url_for_comments.replace('{FIELDS}', '&ids=' + ids + '&fields=likes.summary(true).limit(0),comments.summary(true).limit(0)&');



                jQuery.getJSON(graph_url_for_comments,
					function (result) {

					    for (var j in result) {

					        ffwd_fill_likes_comments_c_gallery(result[j], ffwd, img_ids[j]);
					        //  console.log(result[j]);

					    }

					}
				);


            }


            jQuery('#ffwd_gallery_' + curent_view).html(content);
            break;
        default:
            console.log('error');
            break;
    }
}
/* For Blog-style view */
function ffwd_get_passed_time(time) {
    var today = new Date(),
		arr = time.split(/[^0-9]/);
    today = Date.parse(today) / 1000 - client_server_date_difference;
    time = Date.UTC(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
    time /= 1000;
    time = today - time;

    var tokens = {
        'year': '31536000',
        'month': '2592000',
        'week': '604800',
        'day': '86400',
        'hour': '3600',
        'minute': '60',
        'second': '1'
    };
    var tokens_s = {
        'year': 'years',
        'month': 'months',
        'week': 'weeks',
        'day': 'days',
        'hour': 'hours',
        'minute': 'minutes',
        'second': 'seconds'
    };
    for (unit in tokens) {
        if (time < parseInt(tokens[unit])) continue;
        var numberOfUnits = Math.floor(time / parseInt(tokens[unit]));

        if (numberOfUnits > 1)
            return numberOfUnits + ' ' + ffwd_frontend_text[tokens_s[unit]] + ' ago';

        return numberOfUnits + ' ' + ffwd_frontend_text[unit] + ' ago';
    }
}

function ffwd_fill_likes_blog_style(id_object_id, ffwd, owner_info, ffwd_params, graph_url) {



    var ids_array_count = Math.ceil(id_object_id.length / 50);
    var ids_array = [];
    var img_ids = [];
    for (var i = 0; i < ids_array_count; i++) {
        ids_array[i] = [];
        k = 0;
        for (var j = i * 50; j < (i + 1) * 50; j++) {
            if (!id_object_id[j])
                break;

            ids_array[i][k] = [];
            var object_id = id_object_id[j]['object_id'].replace(id_object_id[j]['from'], owner_info['id'])


            ids_array[i][k] = object_id;
            img_ids[object_id] = id_object_id[j]['id'];
            k++;
        }

    }

    var reactions_array = {};

    /*	for(var i = 0; i < ids_array.length; i++)
        {
    
            ids=ids_array[i].join(',');
    
            url_for_cur_id_likes = graph_url.replace('{FB_ID}',''),
                graph_url_for_likes = url_for_cur_id_likes.replace('{EDGE}', '');
            graph_url_for_likes = graph_url_for_likes.replace('{FIELDS}', 'ids='+ids+'&fields=reactions.type(LIKE).limit(0).summary(true).as(like),reactions.type(LOVE).limit(0).summary(true).as(love),reactions.type(WOW).limit(0).summary(true).as(wow),reactions.type(HAHA).limit(0).summary(true).as(haha),reactions.type(SAD).limit(0).summary(true).as(sad),reactions.type(ANGRY).limit(0).summary(true).as(angry),reactions.limit(5).summary(true).as(total_reactions)&');
            graph_url_for_likes = graph_url_for_likes.replace('{OTHER}', 'summary=true');
            jQuery.getJSON(graph_url_for_likes,
                function(result)
                {
    
                    for(var j in result)
                    {
    
                        ffwd_fill_likes(result[j],ffwd,img_ids[j]);
    
                    }
    
                }
            );
        }*/


    for (var i = 0; i < id_object_id.length; i++) {


        /*For likes*/

        var object_id = id_object_id[i]['object_id'].replace(id_object_id[i]['from'], owner_info['id']);

        url_for_cur_id = graph_url.replace('{FB_ID}', object_id);
        graph_url_for_likes = url_for_cur_id.replace('{EDGE}', '');
        graph_url_for_likes = graph_url_for_likes.replace('{FIELDS}', 'fields=reactions.type(LIKE).limit(0).summary(true).as(like),reactions.type(LOVE).limit(0).summary(true).as(love),reactions.type(WOW).limit(0).summary(true).as(wow),reactions.type(HAHA).limit(0).summary(true).as(haha),reactions.type(SAD).limit(0).summary(true).as(sad),reactions.type(ANGRY).limit(0).summary(true).as(angry),reactions.limit(5).summary(true).as(total_reactions)&');
        graph_url_for_likes = graph_url_for_likes.replace('{OTHER}', 'summary=true');

        /*For comments*/




        var graph_url_for_comments = url_for_cur_id.replace('{EDGE}', 'comments');
        graph_url_for_comments = graph_url_for_comments.replace('{FIELDS}', 'fields=created_time,from,like_count,message,comment_count&');
        graph_url_for_comments = graph_url_for_comments.replace('{OTHER}', 'summary=true&filter=' + ffwd_params["comments_filter"] + '&order=' + ffwd_params["comments_order"] + '&limit=25');

        /*For future (attachment message_tags fields)*/
        /*console.log(graph_url_for_comments);*/
        /*For shares*/
        var graph_url_for_shares = url_for_cur_id.replace('{EDGE}', '');
        graph_url_for_shares = graph_url_for_shares.replace('{FIELDS}', 'fields=shares&');
        graph_url_for_shares = graph_url_for_shares.replace('{OTHER}', '');

        /*For attachments*/
        var graph_url_for_attachments = url_for_cur_id.replace('{EDGE}', 'attachments'),
			graph_url_for_attachments = graph_url_for_attachments.replace('{FIELDS}', '');
        graph_url_for_attachments = graph_url_for_attachments.replace('{OTHER}', '');

        /*For who post*/

        var url_for_who_post = graph_url.replace('{FB_ID}', id_object_id[i]['from']),

			graph_url_for_who_post = url_for_who_post.replace('{EDGE}', ''),
			graph_url_for_who_post = graph_url_for_who_post.replace('{FIELDS}', 'fields=picture,name,link&');
        graph_url_for_who_post = graph_url_for_who_post.replace('{OTHER}', '');

        if (id_object_id[i]['type'] != 'events') {
            //jQuery.getJSON(graph_url_for_likes, createCallback_blog_style(id_object_id[i]['id'], ffwd, 'likes'));
            //jQuery.getJSON(graph_url_for_shares, createCallback_blog_style(id_object_id[i]['id'], ffwd, 'shares'));


            //do_something_with_data_blog_style(reactions_array[object_id], id_object_id[i]['id'], ffwd, 'likes', "", ffwd_params, graph_url);

            if (!id_object_id[i]['shares']) {
                id_object_id[i]['shares'] = '{}';
            }

            do_something_with_data_blog_style(id_object_id[i]['shares'], id_object_id[i]['id'], ffwd, 'shares', "", ffwd_params, graph_url);

            // jQuery.getJSON(graph_url_for_attachments, createCallback_blog_style(id_object_id[i]['id'], ffwd, 'attachments', "", ffwd_params));


            if (id_object_id[i]['attachments'])
                do_something_with_data_blog_style(id_object_id[i]['attachments'], id_object_id[i]['id'], ffwd, 'attachments', "", ffwd_params, graph_url);

        }

        if (id_object_id[i]['comments']);
        do_something_with_data_blog_style(id_object_id[i]['comments'], id_object_id[i]['id'], ffwd, 'comments', "", ffwd_params, graph_url);


        // jQuery.getJSON(id_object_id[i]['comments'], createCallback_blog_style(id_object_id[i]['id'], ffwd, 'comments', "", ffwd_params, graph_url));
        //jQuery.getJSON(graph_url_for_who_post, createCallback_blog_style(id_object_id[i], ffwd, 'who_post', owner_info, ffwd_params));
        do_something_with_data_blog_style(id_object_id[i]['who_post'], id_object_id[i], ffwd, 'who_post', owner_info, ffwd_params, graph_url);



    }
}

function createCallback_blog_style(id, ffwd, type, owner_info, ffwd_params, graph_url) {
    return function (result) {
        do_something_with_data_blog_style(result, id, ffwd, type, owner_info, ffwd_params, graph_url);
    };
}

function do_something_with_data_blog_style(result, id, ffwd, type, owner_info, ffwd_params, graph_url) {

    switch (type) {

        case 'comments':

            var total_count = (result['data'].length < 25) ? result['data'].length : result['summary']['total_count'];
            jQuery('#ffwd_comments_count_' + ffwd + '_' + id).html(total_count);
            /*console.log(result);
			 console.log(result['data'].length);*/
            var more_comments = false,
				comments_exist = false;
            for (var i = 0, j = 1, z = 0; i < result['data'].length; i++, j++) {
                comments_exist = true;
                comment_id = result['data'][i]['id'];
                var display = 'display:block';
                if (j > 4) {
                    display = 'display:none';
                    more_comments = true;
                    z++;
                }

                /*
                                var url_for_cur_id_comm_user_pic = graph_url.replace('{FB_ID}', result['data'][i]['from']['id']);
                                url_for_cur_id_comm_user_pic = url_for_cur_id_comm_user_pic.replace('{EDGE}', '');
                                url_for_cur_id_comm_user_pic = url_for_cur_id_comm_user_pic.replace('{FIELDS}', 'fields=picture&');
                                url_for_cur_id_comm_user_pic = url_for_cur_id_comm_user_pic.replace('{OTHER}', '');
                */



                //var comment_author_pic = '<div style="float:left" id="ffwd_comment_author_pic_' + ffwd + '_' + id + '" class="ffwd_comment_author_pic_' + ffwd + '" > <img class="user_' + result['data'][i]['from']['id'] + '" src="https://graph.facebook.com/'+result['data'][i]['from']['id']+'/picture" > </div>',
                var comment_author_pic = '<span></span>',
					comment_author_name = '<span></span>',
					//comment_author_name = '<a id="ffwd_comment_author_name_' + ffwd + '_' + id + '" href="https://www.facebook.com/' + result['data'][i]['from']['id'] + '" class="ffwd_comment_author_name_' + ffwd + '" > ' + result['data'][i]['from']['name'] + ' </a>',
					comment_message = '<span id="ffwd_comment_message_' + ffwd + '_' + id + '" class="ffwd_comment_message_' + ffwd + '" > ' + result['data'][i]['message'] + ' </span>',
					comment_date = '<span id="ffwd_comment_date_' + ffwd + '_' + id + '" class="ffwd_comment_date_' + ffwd + '" > ' + ffwd_get_passed_time(result['data'][i]['created_time']) + '</span>',
					comment_likes_count = '<span id="ffwd_comment_likes_count_' + ffwd + '_' + id + '" class="ffwd_comment_likes_count_' + ffwd + '" > ' + result['data'][i]['like_count'] + ' </span>',
					comments_date_likes = '<div>' + comment_date + comment_likes_count + '</div>',
					comment_replies_cont = (ffwd_params["comments_filter"] == "toplevel" && ffwd_params["comments_replies"] == "1" && result['data'][i]['comment_count'] > 0) ? '<div class="ffwd_comment_replies_' + ffwd + '"><div id="ffwd_comment_replies_label_' + comment_id + '" class="ffwd_comment_replies_label_' + ffwd + '">' + result['data'][i]['comment_count'] + ' ' + ffwd_frontend_text.comment_reply + ' </div><div class="ffwd_comment_replies_content_' + ffwd + '"></div></div>' : '',
					comment_div_cont = '<div class="ffwd_comment_content_' + ffwd + '" id="ffwd_comment_content_' + ffwd + '_' + id + '" >' + comment_author_name + comment_message + comments_date_likes + comment_replies_cont + '<div style="clear:both"></div></div>',
					comment = '<div id="ffwd_comment_' + ffwd + '_' + comment_id + '" class="ffwd_comment_' + ffwd + '" style="' + display + '">' + comment_author_pic + comment_div_cont + '<div style="clear:both" > </div></div>';

                jQuery('#ffwd_comments_content_' + id + '_' + ffwd + '').append(comment);
                (function (i) {
                    jQuery('#ffwd_comment_replies_label_' + result['data'][i]['id']).bind("click", function () {

                        ffwd_get_comments_replies(result['data'][i]['id'], graph_url, ffwd_params, ffwd);
                    })
                }(i))


            }
            if (more_comments) {
                jQuery('#ffwd_comments_content_' + id + '_' + ffwd).append('<div class="ffwd_view_more_comments_cont_' + ffwd + '"> <a href="#" class="ffwd_view_more_comments" more_count="' + z + '"> <span>' + ffwd_frontend_text.view + ' ' + z + ' ' + ffwd_frontend_text.more_comments + '</span> <a> </div>');
            }
            if (!comments_exist) {
                jQuery('#ffwd_comments_content_' + id + '_' + ffwd).remove();
            }
            ffwd_blog_style_resize(ffwd_params, ffwd);
            break;
        case 'shares':

            var shares_count = (result != null && result['count'] != null) ? parseInt(result['count']) : '0';
            jQuery('#ffwd_shares_' + ffwd + '_' + id).html(shares_count);
            break;
        case 'attachments':
            var src = '', length = 0, album_id = '';
            /*
			 erb story mej nshaca vor addes photos aranc albumi anun talu hetevabar @ngela timline -i mej
			 u avtomat et posti subattachmentsneri arkayutayn depqum kberi dranq, ISK ete nshvaca added photos to album esinch
			 hetevabar petqa albumi id-in vercnel araji media-i targeti urlic u pageID + albumid posti subattamentner@ cuyc tal!!!!!
			 */

            if (result) {
                /*If exists subattachments*/
                if (result['subattachments']) {
                    length = result['subattachments']['data'].length;
                    if (typeof result['subattachments']['data'][0]['media'] != "undefined") {
                        src = result['subattachments']['data'][0]['media']['image']['src'];
                    }
                    /*First time add profile picture*/
                    if (result['type'] == 'gallery') {
                        src = result['subattachments']['data'][length - 1]['media']['image']['src'];
                    }
                }
                else if (result['media']) {
                    /* Check album containing this photo (compare title)
					 * If not Timeline photos or Profile Pictures so get photos from that album
					 */
                    /*if (result['data'][0]['title'] != 'Timeline Photos' && result['data'][0]['title'] != 'Profile Pictures') {
					 album_id = result['data'][0]['url'].split("photos/");
					 if (typeof album_id[1] != 'undefined') {
					 album_id = album_id[1].split(".");
					 album_id = album_id[1];

					 }
					 }*/
                    src = result['media']['image']['src'];
                }


                jQuery('#ffwd_blog_style_img_' + id + '_' + ffwd).attr('src', src);
                if (src == '')
                    jQuery('#ffwd_blog_style_img_' + id + '_' + ffwd).remove();

            }
            ffwd_blog_style_resize(ffwd_params, ffwd);
            break;
        case 'who_post':

            var who_post = result;

            var who_post_name_link = (ffwd_params['blog_style_author'] == "1") ? '<a class="ffwd_blog_style_object_from_name_' + ffwd + '" href="https://www.facebook.com/' + who_post['id'] + '" target="_blank">' + who_post['name'] + '</a>' : '',
				owner_name_link = '<a class="ffwd_blog_style_object_from_name_' + ffwd + '" href="https://www.facebook.com/' + owner_info['id'] + '" target="_blank">' + owner_info['name'] + '</a>',
				who_post_pic = '<img id="ffwd_user_pic_' + ffwd + '_' + id['id'] + '" class="ffwd_user_pic" src="' + who_post['picture']['data']['url'] + '" style="width:50px;height:50px;box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2);margin-top: 9px;">',
				place,
				full_place = '',
				story = id['story'].replace(/'/g, "&#039;"),
				who_post_index = story.indexOf(who_post['name']),
				owner_index = story.indexOf(owner_info['name']),
				place_name = '',
				place_id = '',
				story_tags = id['story_tags'],
				place_index = -1;

            if (id['place'] != null) {
                /* place_id = id['place']['id'];
               if(id['place']['location']){
               var street = (ffwd_params['event_street'] == "1") ? ((typeof id['place']['location']['street'] != 'undefined') ? id['place']['location']['street'] : '') : '',
                   city = (ffwd_params['event_city'] == "1") ? ((typeof id['place']['location']['city'] != 'undefined') ? id['place']['location']['city'] : '') : '',
                   country = (ffwd_params['event_country'] == "1") ? ((typeof id['place']['location']['country'] != 'undefined') ? id['place']['location']['country'] : '') : '',
                   state = (ffwd_params['event_zip'] == "1") ? ((typeof id['place']['location']['state'] != 'undefined') ? id['place']['location']['state'] : '') : '',
                   zip = (ffwd_params['event_zip'] == "1") ? ((typeof id['place']['location']['zip'] != 'undefined') ? id['place']['location']['zip'] : '') : '',
                   latitude = (ffwd_params['event_map'] == "1") ? ((typeof id['place']['location']['latitude'] != 'undefined') ? id['place']['location']['latitude'] : '') : '',
                   longitude = (ffwd_params['event_map'] == "1") ? ((typeof id['place']['location']['longitude'] != 'undefined') ? id['place']['location']['longitude'] : '') : '';

               full_place = ((ffwd_params['event_street'] == "1") ? '<div class="ffwd_place_street_' + ffwd + '" >' + street + '</div> ' : '') +
                   ((ffwd_params['event_city'] == "1" || ffwd_params['event_zip'] == "1" || ffwd_params['event_country'] == "1" ) ? '<div class="ffwd_place_city_state_country_' + ffwd + '" >' + city + ' ' + state + ' ' + zip + ' ' + country + '</div>' : '') +
                   ((ffwd_params['event_map'] == "1") ? '<a class="ffwd_place_map_' + ffwd + '" style="text-decoration:underline" href="https://maps.google.com/maps?q=' + latitude + ',' + longitude + '" target="_blank">Map</a>' : '');
               }*/
                if (id['place']['name'] && ffwd_params['event_street'] == "1") {
                    full_place = '<div class="ffwd_place_city_state_country_' + ffwd + '" >' + id['place']['name'] + '</div>';
                }
            }
            // Who post
            if (who_post_index != -1) {
                story = story.replace(who_post['name'], who_post_name_link);
            }
            if (owner_index != -1) {
                story = story.replace(owner_info['name'], owner_name_link);
            }
            if (who_post_index == -1 && owner_index == -1) {
                story = who_post_name_link;
            }

            // With whom after was
            if (story_tags != null) {
                var type = story_tags.constructor.name;
                if (type == "Object") {
                    for (var x in story_tags) {
                        var story_tag_name = story_tags[x]["0"]["name"],
							story_tag_id = story_tags[x]["0"]["id"];
                        with_name_index = story.indexOf(story_tag_name);
                        if ((with_name_index != -1) && (story_tag_name != who_post['name']) && (story_tag_name != owner_info['name']) && (story_tag_id != place_id)) {
                            story_tag_link = (/*ffwd_params['blog_style_with_whom'] == "1"*/true) ? '<a class="ffwd_blog_style_object_from_name_' + ffwd + '" href="https://www.facebook.com/' + story_tag_id + '" target="_blank">' + story_tag_name + '</a>' : '',
								story = story.replace(story_tag_name, story_tag_link);
                        }
                        else if (story_tag_id == place_id) {
                            // Where after was
                            place_index = 1;
                            place = (ffwd_params['blog_style_place_name'] == "1") ? '<a class="ffwd_place_name_' + ffwd + '" href="https://www.facebook.com/' + story_tag_id + '" target="_blank">' + story_tag_name + '</a>' : '';
                            story = story.replace("\u2014", "");
                            story = story.replace(story_tag_name, place);
                        }
                    }
                }
                else if (type == "Array") {
                    for (var j = 0; j < story_tags.length; j++) {
                        if (typeof story_tags[j]["0"] != "undefined") {
                            var story_tag_name = story_tags[j]["0"]["name"],
								story_tag_id = story_tags[j]["0"]["id"];
                        } else {
                            var story_tag_name = story_tags[j].name,
								story_tag_id = story_tags[j].id;
                        }
                        with_name_index = story.indexOf(story_tag_name);
                        if ((with_name_index != -1) && (story_tag_name != who_post['name']) && (story_tag_name != owner_info['name']) && (story_tag_id != place_id)) {
                            story_tag_link = (/*ffwd_params['blog_style_with_whom'] == "1"*/true) ? '<a class="ffwd_blog_style_object_from_name_' + ffwd + '" href="https://www.facebook.com/' + story_tag_id + '" target="_blank">' + story_tag_name + '</a>' : '',
								story = story.replace(story_tag_name, story_tag_link);
                        }

                        else if (story_tag_id == place_id) {
                            // Where after was
                            place_index = 1;
                            place = (ffwd_params['blog_style_place_name'] == "1") ? '<a class="ffwd_place_name_' + ffwd + '" href="https://www.facebook.com/' + story_tag_id + '" target="_blank">' + story_tag_name + '</a>' : '';
                            story = story.replace("\u2014", "");
                            story = story.replace(story_tag_name, place);
                        }
                    }
                }
            }
            // Where after was
            if (ffwd_params['blog_style_place_name'] == "1"/* && typeof place != 'undefined' && place != ''*/) {
                if (id['type'] == 'events') {
                    story += full_place;
                }
            }
            else {
                story = story.replace(/ at| in|/gi, "");
            }
            jQuery('#ffwd_blog_style_object_from_pic_' + ffwd + '_' + id['id']).html(who_post_pic).attr("href", who_post['link']);
            jQuery('#ffwd_blog_style_object_story_' + ffwd + '_' + id['id']).html(story);
            break;
        default:
            break;
    }
}


function ffwd_get_comments_replies(comment_id, graph_url, ffwd_params, ffwd) {

    var url_for_cur_id_comm_replies = graph_url.replace('{FB_ID}', comment_id);
    url_for_cur_id_comm_replies = url_for_cur_id_comm_replies.replace('{EDGE}', 'comments');
    url_for_cur_id_comm_replies = url_for_cur_id_comm_replies.replace('{FIELDS}', 'fields=parent,created_time,from,like_count,message&');
    url_for_cur_id_comm_replies = url_for_cur_id_comm_replies.replace('{OTHER}', '');
    jQuery.getJSON(url_for_cur_id_comm_replies, function (result) {
        for (var k = 0; k < result['data'].length; k++) {
            var parent_comm_id = result['data'][k]["parent"]["id"],
				comment_reply_id = result['data'][k]["id"];

            var url_for_cur_id_comm_rep_user_pic = graph_url.replace('{FB_ID}', result['data'][k]['from']['id']);
            url_for_cur_id_comm_rep_user_pic = url_for_cur_id_comm_rep_user_pic.replace('{EDGE}', '');
            url_for_cur_id_comm_rep_user_pic = url_for_cur_id_comm_rep_user_pic.replace('{FIELDS}', 'fields=picture&');
            url_for_cur_id_comm_rep_user_pic = url_for_cur_id_comm_rep_user_pic.replace('{OTHER}', '');

            var comment_reply_author_pic = '<div style="float:left" class="ffwd_comment_reply_author_pic_' + ffwd + '" > <img class="reply_user_' + result['data'][k]['from']['id'] + '" src="https://graph.facebook.com/' + result['data'][k]['from']['id'] + '/picture" > </div>',
				comment_reply_author_name = '<a href="https://www.facebook.com/' + result['data'][k]['from']['id'] + '" class="ffwd_comment_reply_author_name_' + ffwd + '" > ' + result['data'][k]['from']['name'] + ' </a>',
				comment_reply_message = '<span class="ffwd_comment_reply_message_' + ffwd + '" > ' + result['data'][k]['message'] + ' </span>',
				comment_reply_date = '<span class="ffwd_comment_reply_date_' + ffwd + '" > ' + ffwd_get_passed_time(result['data'][k]['created_time']) + '</span>',
				comment_reply_likes_count = '<span class="ffwd_comment_reply_likes_count_' + ffwd + '" > ' + result['data'][k]['like_count'] + ' </span>',
				comments_reply_date_likes = '<div>' + comment_reply_date + comment_reply_likes_count + '</div>',
				comment_reply_div_cont = '<div class="ffwd_comment_reply_content_' + ffwd + '" >' + comment_reply_author_name + comment_reply_message + comments_reply_date_likes + '<div style="clear:both"></div></div>',
				comment_reply = '<div id="ffwd_comment_reply_' + ffwd + '_' + comment_reply_id + '" class="ffwd_comment_reply_' + ffwd + '">' + comment_reply_author_pic + comment_reply_div_cont + '<div style="clear:both" > </div></div>';
            jQuery('#ffwd_comment_' + ffwd + '_' + parent_comm_id + ' .ffwd_comment_replies_content_' + ffwd).append(comment_reply);

        }
        ffwd_blog_style_resize(ffwd_params, ffwd);
    });


}
function ffwd_fill_likes(result, ffwd, id) {
    var total_reactions_count = {}
    total_reactions_count[ffwd] = parseInt(result['total_reactions']['summary']['total_count']);
    var likes_count = {};
    likes_count[ffwd] = parseInt(result['like']['summary']['total_count']);
    var love_count = {};
    love_count[ffwd] = parseInt(result['love']['summary']['total_count']);
    var wow_count = {};
    wow_count[ffwd] = parseInt(result['wow']['summary']['total_count']);
    var haha_count = {};
    haha_count[ffwd] = parseInt(result['haha']['summary']['total_count']);
    var sad_count = {};
    sad_count[ffwd] = parseInt(result['sad']['summary']['total_count']);
    var angry_count = {};
    angry_count[ffwd] = parseInt(result['angry']['summary']['total_count']);

    jQuery('#ffwd_likes_' + ffwd + '_' + id).html(total_reactions_count[ffwd]);
    if (total_reactions_count[ffwd] >= 3 && result['total_reactions']['data'][0]) {
        var likes_some_names = '<div class="ffwd_like_name_cont_' + ffwd + '"> <a class="ffwd_like_name_' + ffwd + '" href="https://www.facebook.com/' + result['total_reactions']['data'][0]['id'] + '" target="_blank">' + result['total_reactions']['data'][0]['name'] + ' , </a><a class="ffwd_like_name_' + ffwd + '" href="https://www.facebook.com/' + result['total_reactions']['data'][1]['id'] + '" target="_blank">' + result['total_reactions']['data'][1]['name'] + ' </a></div>';
        var likes_count_last_part = '<div class="ffwd_almost_' + ffwd + '">  ' + ffwd_frontend_text.and + ' ' + (total_reactions_count[ffwd] - 2) + ' ' + ffwd_frontend_text.others + ' </div>';
    }
    else if (total_reactions_count[ffwd] == 2 && result['total_reactions']['data'][0]) {
        var likes_some_names = '<div class="ffwd_like_name_cont_' + ffwd + '"> <a class="ffwd_like_name_' + ffwd + '" href="https://www.facebook.com/' + result['total_reactions']['data'][0]['id'] + '" target="_blank">' + result['total_reactions']['data'][0]['name'] + ' , </a><a class="ffwd_like_name_' + ffwd + '" href="https://www.facebook.com/' + result['total_reactions']['data'][1]['id'] + '" target="_blank">' + result['total_reactions']['data'][1]['name'] + ' </a></div>';
        var likes_count_last_part = '';
    }
    else if (total_reactions_count[ffwd] == 1 && result['total_reactions']['data'][0]) {
        var likes_some_names = '<div class="ffwd_like_name_cont_' + ffwd + '"> <a class="ffwd_like_name_' + ffwd + '" href="https://www.facebook.com/' + result['total_reactions']['data'][0]['id'] + '" target="_blank">' + result['total_reactions']['data'][0]['name'] + '</a></div>';
        var likes_count_last_part = '';
    }
    else {
        var likes_some_names = '';
        var likes_count_last_part = '';
    }

    var reactions_icons = ''
    reactions_width = 0;
    if (likes_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_like_' + ffwd + '" title="Like"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_like_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_like_' + id + '_' + ffwd).html('Like: ' + likes_count[ffwd]);
    }
    if (love_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_love_' + ffwd + '" title="Love"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_love_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_love_' + id + '_' + ffwd).html('Love: ' + love_count[ffwd]);


    }
    if (haha_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_haha_' + ffwd + '" title="Haha"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_haha_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_haha_' + id + '_' + ffwd).html('Haha: ' + haha_count[ffwd]);


    }
    if (wow_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_wow_' + ffwd + '" title="WOW"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_wow_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_wow_' + id + '_' + ffwd).html('Wow: ' + wow_count[ffwd]);


    }
    if (sad_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_sad_' + ffwd + '" title="Sad"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_sad_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_sad_' + id + '_' + ffwd).html('Sad: ' + sad_count[ffwd]);


    }
    if (angry_count[ffwd] != 0) {
        reactions_icons += '<span class="ffwd_react_icons_angry_' + ffwd + '" title="Angry"></span>';
        reactions_width += 40;
        jQuery('#ffwd_reactions_angry_' + id + '_' + ffwd).show();
        jQuery('#ffwd_tootlip_text_angry_' + id + '_' + ffwd).html('Angry: ' + angry_count[ffwd]);


    }

    jQuery('#ffwd_reacts_' + id + '_' + ffwd).css('width', reactions_width + 52 + 'px');

    if (total_reactions_count[ffwd] == 0)
        jQuery('#ffwd_reacts_' + id + '_' + ffwd).css('display', 'none');

    var likes_names_count = '<div class="ffwd_likes_names_' + ffwd + '">' + reactions_icons + likes_some_names + likes_count_last_part + ' </div><div style="clear:both" ></div>';


    if (total_reactions_count[ffwd])
        jQuery('#ffwd_likes_names_count_' + id + '_' + ffwd).html(likes_names_count);
    else
        jQuery('#ffwd_likes_names_count_' + id + '_' + ffwd).remove();

}


function ffwd_blog_style_resize(ffwd_params, ffwd) {
    var window_width = window.innerWidth,
		blogstyle_object = jQuery(".blog_style_object_container_" + ffwd),
		blogstyle_object_width = blogstyle_object.width(),
		blogstyle_comment_content = jQuery(".ffwd_comment_content_" + ffwd),
		blogstyle_comment_reply_content = jQuery(".ffwd_comment_reply_content_" + ffwd),
		blogstyle_comment_content_left_margin = (parseInt(blogstyle_comment_content.css("margin-left"))) ?
			parseInt(blogstyle_comment_content.css("margin-left")) : 0,
		comment_author_pic_width = (parseInt(jQuery(".ffwd_comment_author_pic_" + ffwd + " img").css("width"))) ?
			parseInt(jQuery(".ffwd_comment_author_pic_" + ffwd + " img").css("width")) : 0,
		comment_reply_author_pic_width = (parseInt(jQuery(".ffwd_comment_reply_author_pic_" + ffwd + " img").css("width"))) ?
			parseInt(jQuery(".ffwd_comment_reply_author_pic_" + ffwd + " img").css("width")) : 0,
		comment_content_padding = (parseInt(jQuery(".ffwd_comment_" + ffwd).css("padding-left"))) ?
			parseInt(jQuery(".ffwd_comment_" + ffwd).css("padding-left")) : 0,
		ffwd_blog_style_img = jQuery(".ffwd_blog_style_img_" + ffwd);
    /* Minus one for twenty fifteen theme :))*/
    max_width_for_comment_content = blogstyle_object_width -
		comment_author_pic_width -
		blogstyle_comment_content_left_margin -
		(2 * comment_content_padding) - 1,
		max_width_for_comment_reply_content = max_width_for_comment_content -
			comment_reply_author_pic_width -
			blogstyle_comment_content_left_margin -
			(2 * comment_content_padding) - 1;


    max_width_for_attachment = (ffwd_params["blog_style_view_type"] != "1" && window_width > 520) ?
	blogstyle_object_width * 0.55 : blogstyle_object_width;

    blogstyle_comment_content.css("max-width", max_width_for_comment_content);
    blogstyle_comment_reply_content.css("max-width", max_width_for_comment_reply_content);
    ffwd_blog_style_img.css("max-width", max_width_for_attachment);
}


/*
jQuery(window).ready(function () {
  jQuery.ajax({
    url: ffwd_frontend_text.ajax_url,
    data: {
      'action': 'ffwd_autoupdate',
		},
    type: 'get',
    success: function (response) {
      console.log(response);
    }

  })

})*/;

(function (e, t, n) { typeof define == "function" && define.amd ? define(["jquery"], function (r) { return n(r, e, t), r.mobile }) : n(e.jQuery, e, t) })(this, document, function (e, t, n, r) { (function (e, t, n, r) { function x(e) { while (e && typeof e.originalEvent != "undefined") e = e.originalEvent; return e } function T(t, n) { var i = t.type, s, o, a, l, c, h, p, d, v; t = e.Event(t), t.type = n, s = t.originalEvent, o = e.event.props, i.search(/^(mouse|click)/) > -1 && (o = f); if (s) for (p = o.length, l; p;) l = o[--p], t[l] = s[l]; i.search(/mouse(down|up)|click/) > -1 && !t.which && (t.which = 1); if (i.search(/^touch/) !== -1) { a = x(s), i = a.touches, c = a.changedTouches, h = i && i.length ? i[0] : c && c.length ? c[0] : r; if (h) for (d = 0, v = u.length; d < v; d++) l = u[d], t[l] = h[l] } return t } function N(t) { var n = {}, r, s; while (t) { r = e.data(t, i); for (s in r) r[s] && (n[s] = n.hasVirtualBinding = !0); t = t.parentNode } return n } function C(t, n) { var r; while (t) { r = e.data(t, i); if (r && (!n || r[n])) return t; t = t.parentNode } return null } function k() { g = !1 } function L() { g = !0 } function A() { E = 0, v.length = 0, m = !1, L() } function O() { k() } function M() { _(), c = setTimeout(function () { c = 0, A() }, e.vmouse.resetTimerDuration) } function _() { c && (clearTimeout(c), c = 0) } function D(t, n, r) { var i; if (r && r[t] || !r && C(n.target, t)) i = T(n, t), e(n.target).trigger(i); return i } function P(t) { var n = e.data(t.target, s); if (!m && (!E || E !== n)) { var r = D("v" + t.type, t); r && (r.isDefaultPrevented() && t.preventDefault(), r.isPropagationStopped() && t.stopPropagation(), r.isImmediatePropagationStopped() && t.stopImmediatePropagation()) } } function H(t) { var n = x(t).touches, r, i; if (n && n.length === 1) { r = t.target, i = N(r); if (i.hasVirtualBinding) { E = w++, e.data(r, s, E), _(), O(), d = !1; var o = x(t).touches[0]; h = o.pageX, p = o.pageY, D("vmouseover", t, i), D("vmousedown", t, i) } } } function B(e) { if (g) return; d || D("vmousecancel", e, N(e.target)), d = !0, M() } function j(t) { if (g) return; var n = x(t).touches[0], r = d, i = e.vmouse.moveDistanceThreshold, s = N(t.target); d = d || Math.abs(n.pageX - h) > i || Math.abs(n.pageY - p) > i, d && !r && D("vmousecancel", t, s), D("vmousemove", t, s), M() } function F(e) { if (g) return; L(); var t = N(e.target), n; D("vmouseup", e, t); if (!d) { var r = D("vclick", e, t); r && r.isDefaultPrevented() && (n = x(e).changedTouches[0], v.push({ touchID: E, x: n.clientX, y: n.clientY }), m = !0) } D("vmouseout", e, t), d = !1, M() } function I(t) { var n = e.data(t, i), r; if (n) for (r in n) if (n[r]) return !0; return !1 } function q() { } function R(t) { var n = t.substr(1); return { setup: function (r, s) { I(this) || e.data(this, i, {}); var o = e.data(this, i); o[t] = !0, l[t] = (l[t] || 0) + 1, l[t] === 1 && b.bind(n, P), e(this).bind(n, q), y && (l.touchstart = (l.touchstart || 0) + 1, l.touchstart === 1 && b.bind("touchstart", H).bind("touchend", F).bind("touchmove", j).bind("scroll", B)) }, teardown: function (r, s) { --l[t], l[t] || b.unbind(n, P), y && (--l.touchstart, l.touchstart || b.unbind("touchstart", H).unbind("touchmove", j).unbind("touchend", F).unbind("scroll", B)); var o = e(this), u = e.data(this, i); u && (u[t] = !1), o.unbind(n, q), I(this) || o.removeData(i) } } } var i = "virtualMouseBindings", s = "virtualTouchID", o = "vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "), u = "clientX clientY pageX pageY screenX screenY".split(" "), a = e.event.mouseHooks ? e.event.mouseHooks.props : [], f = e.event.props.concat(a), l = {}, c = 0, h = 0, p = 0, d = !1, v = [], m = !1, g = !1, y = "addEventListener" in n, b = e(n), w = 1, E = 0, S; e.vmouse = { moveDistanceThreshold: 10, clickDistanceThreshold: 10, resetTimerDuration: 1500 }; for (var U = 0; U < o.length; U++) e.event.special[o[U]] = R(o[U]); y && n.addEventListener("click", function (t) { var n = v.length, r = t.target, i, o, u, a, f, l; if (n) { i = t.clientX, o = t.clientY, S = e.vmouse.clickDistanceThreshold, u = r; while (u) { for (a = 0; a < n; a++) { f = v[a], l = 0; if (u === r && Math.abs(f.x - i) < S && Math.abs(f.y - o) < S || e.data(u, s) === f.touchID) { t.preventDefault(), t.stopPropagation(); return } } u = u.parentNode } } }, !0) })(e, t, n), function (e) { e.mobile = {} }(e), function (e, t) { var r = { touch: "ontouchend" in n }; e.mobile.support = e.mobile.support || {}, e.extend(e.support, r), e.extend(e.mobile.support, r) }(e), function (e, t, r) { function l(t, n, r) { var i = r.type; r.type = n, e.event.dispatch.call(t, r), r.type = i } var i = e(n); e.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "), function (t, n) { e.fn[n] = function (e) { return e ? this.bind(n, e) : this.trigger(n) }, e.attrFn && (e.attrFn[n] = !0) }); var s = e.mobile.support.touch, o = "touchmove scroll", u = s ? "touchstart" : "mousedown", a = s ? "touchend" : "mouseup", f = s ? "touchmove" : "mousemove"; e.event.special.scrollstart = { enabled: !0, setup: function () { function s(e, n) { r = n, l(t, r ? "scrollstart" : "scrollstop", e) } var t = this, n = e(t), r, i; n.bind(o, function (t) { if (!e.event.special.scrollstart.enabled) return; r || s(t, !0), clearTimeout(i), i = setTimeout(function () { s(t, !1) }, 50) }) } }, e.event.special.tap = { tapholdThreshold: 750, setup: function () { var t = this, n = e(t); n.bind("vmousedown", function (r) { function a() { clearTimeout(u) } function f() { a(), n.unbind("vclick", c).unbind("vmouseup", a), i.unbind("vmousecancel", f) } function c(e) { f(), s === e.target && l(t, "tap", e) } if (r.which && r.which !== 1) return !1; var s = r.target, o = r.originalEvent, u; n.bind("vmouseup", a).bind("vclick", c), i.bind("vmousecancel", f), u = setTimeout(function () { l(t, "taphold", e.Event("taphold", { target: s })) }, e.event.special.tap.tapholdThreshold) }) } }, e.event.special.swipe = { scrollSupressionThreshold: 10, durationThreshold: 1000, horizontalDistanceThreshold: 10, verticalDistanceThreshold: 475, start: function (t) { var n = t.originalEvent.touches ? t.originalEvent.touches[0] : t; return { time: (new Date).getTime(), coords: [n.pageX, n.pageY], origin: e(t.target) } }, stop: function (e) { var t = e.originalEvent.touches ? e.originalEvent.touches[0] : e; return { time: (new Date).getTime(), coords: [t.pageX, t.pageY] } }, handleSwipe: function (t, n) { n.time - t.time < e.event.special.swipe.durationThreshold && Math.abs(t.coords[0] - n.coords[0]) > e.event.special.swipe.horizontalDistanceThreshold && Math.abs(t.coords[1] - n.coords[1]) < e.event.special.swipe.verticalDistanceThreshold && t.origin.trigger("swipe").trigger(t.coords[0] > n.coords[0] ? "swipeleft" : "swiperight") }, setup: function () { var t = this, n = e(t); n.bind(u, function (t) { function o(t) { if (!i) return; s = e.event.special.swipe.stop(t), Math.abs(i.coords[0] - s.coords[0]) > e.event.special.swipe.scrollSupressionThreshold && t.preventDefault() } var i = e.event.special.swipe.start(t), s; n.bind(f, o).one(a, function () { n.unbind(f, o), i && s && e.event.special.swipe.handleSwipe(i, s), i = s = r }) }) } }, e.each({ scrollstop: "scrollstart", taphold: "tap", swipeleft: "swipe", swiperight: "swipe" }, function (t, n) { e.event.special[t] = { setup: function () { e(this).bind(n, e.noop) } } }) }(e, this) });

(function (a) { function d(b) { var c = b || window.event, d = [].slice.call(arguments, 1), e = 0, f = !0, g = 0, h = 0; return b = a.event.fix(c), b.type = "mousewheel", c.wheelDelta && (e = c.wheelDelta / 120), c.detail && (e = -c.detail / 3), h = e, c.axis !== undefined && c.axis === c.HORIZONTAL_AXIS && (h = 0, g = -1 * e), c.wheelDeltaY !== undefined && (h = c.wheelDeltaY / 120), c.wheelDeltaX !== undefined && (g = -1 * c.wheelDeltaX / 120), d.unshift(b, e, g, h), (a.event.dispatch || a.event.handle).apply(this, d) } var b = ["DOMMouseScroll", "mousewheel"]; if (a.event.fixHooks) for (var c = b.length; c;) a.event.fixHooks[b[--c]] = a.event.mouseHooks; a.event.special.mousewheel = { setup: function () { if (this.addEventListener) for (var a = b.length; a;) this.addEventListener(b[--a], d, !1); else this.onmousewheel = d }, teardown: function () { if (this.removeEventListener) for (var a = b.length; a;) this.removeEventListener(b[--a], d, !1); else this.onmousewheel = null } }, a.fn.extend({ mousewheel: function (a) { return a ? this.bind("mousewheel", a) : this.trigger("mousewheel") }, unmousewheel: function (a) { return this.unbind("mousewheel", a) } }) })(jQuery);
/*custom scrollbar*/
(function (c) { var b = { init: function (e) { var f = { set_width: false, set_height: false, horizontalScroll: false, scrollInertia: 950, mouseWheel: true, mouseWheelPixels: "auto", autoDraggerLength: true, autoHideScrollbar: false, snapAmount: null, snapOffset: 0, scrollButtons: { enable: false, scrollType: "continuous", scrollSpeed: "auto", scrollAmount: 40 }, advanced: { updateOnBrowserResize: true, updateOnContentResize: false, autoExpandHorizontalScroll: false, autoScrollOnFocus: true, normalizeMouseWheelDelta: false }, contentTouchScroll: true, callbacks: { onScrollStart: function () { }, onScroll: function () { }, onTotalScroll: function () { }, onTotalScrollBack: function () { }, onTotalScrollOffset: 0, onTotalScrollBackOffset: 0, whileScrolling: function () { } }, theme: "light" }, e = c.extend(true, f, e); return this.each(function () { var m = c(this); if (e.set_width) { m.css("width", e.set_width) } if (e.set_height) { m.css("height", e.set_height) } if (!c(document).data("mCustomScrollbar-index")) { c(document).data("mCustomScrollbar-index", "1") } else { var t = parseInt(c(document).data("mCustomScrollbar-index")); c(document).data("mCustomScrollbar-index", t + 1) } m.wrapInner("<div class='mCustomScrollBox mCS-" + e.theme + "' id='mCSB_" + c(document).data("mCustomScrollbar-index") + "' style='position:relative; height:100%; overflow:hidden; max-width:100%;' />").addClass("mCustomScrollbar _mCS_" + c(document).data("mCustomScrollbar-index")); var g = m.children(".mCustomScrollBox"); if (e.horizontalScroll) { g.addClass("mCSB_horizontal").wrapInner("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />"); var k = g.children(".mCSB_h_wrapper"); k.wrapInner("<div class='mCSB_container' style='position:absolute; left:0;' />").children(".mCSB_container").css({ width: k.children().outerWidth(), position: "relative" }).unwrap() } else { g.wrapInner("<div class='mCSB_container' style='position:relative; top:0;' />") } var o = g.children(".mCSB_container"); if (c.support.touch) { o.addClass("mCS_touch") } o.after("<div class='mCSB_scrollTools' style='position:absolute;'><div class='mCSB_draggerContainer'><div class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' style='position:relative;'></div></div><div class='mCSB_draggerRail'></div></div></div>"); var l = g.children(".mCSB_scrollTools"), h = l.children(".mCSB_draggerContainer"), q = h.children(".mCSB_dragger"); if (e.horizontalScroll) { q.data("minDraggerWidth", q.width()) } else { q.data("minDraggerHeight", q.height()) } if (e.scrollButtons.enable) { if (e.horizontalScroll) { l.prepend("<a class='mCSB_buttonLeft' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonRight' oncontextmenu='return false;'></a>") } else { l.prepend("<a class='mCSB_buttonUp' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonDown' oncontextmenu='return false;'></a>") } } g.bind("scroll", function () { if (!m.is(".mCS_disabled")) { g.scrollTop(0).scrollLeft(0) } }); m.data({ mCS_Init: true, mCustomScrollbarIndex: c(document).data("mCustomScrollbar-index"), horizontalScroll: e.horizontalScroll, scrollInertia: e.scrollInertia, scrollEasing: "mcsEaseOut", mouseWheel: e.mouseWheel, mouseWheelPixels: e.mouseWheelPixels, autoDraggerLength: e.autoDraggerLength, autoHideScrollbar: e.autoHideScrollbar, snapAmount: e.snapAmount, snapOffset: e.snapOffset, scrollButtons_enable: e.scrollButtons.enable, scrollButtons_scrollType: e.scrollButtons.scrollType, scrollButtons_scrollSpeed: e.scrollButtons.scrollSpeed, scrollButtons_scrollAmount: e.scrollButtons.scrollAmount, autoExpandHorizontalScroll: e.advanced.autoExpandHorizontalScroll, autoScrollOnFocus: e.advanced.autoScrollOnFocus, normalizeMouseWheelDelta: e.advanced.normalizeMouseWheelDelta, contentTouchScroll: e.contentTouchScroll, onScrollStart_Callback: e.callbacks.onScrollStart, onScroll_Callback: e.callbacks.onScroll, onTotalScroll_Callback: e.callbacks.onTotalScroll, onTotalScrollBack_Callback: e.callbacks.onTotalScrollBack, onTotalScroll_Offset: e.callbacks.onTotalScrollOffset, onTotalScrollBack_Offset: e.callbacks.onTotalScrollBackOffset, whileScrolling_Callback: e.callbacks.whileScrolling, bindEvent_scrollbar_drag: false, bindEvent_content_touch: false, bindEvent_scrollbar_click: false, bindEvent_mousewheel: false, bindEvent_buttonsContinuous_y: false, bindEvent_buttonsContinuous_x: false, bindEvent_buttonsPixels_y: false, bindEvent_buttonsPixels_x: false, bindEvent_focusin: false, bindEvent_autoHideScrollbar: false, mCSB_buttonScrollRight: false, mCSB_buttonScrollLeft: false, mCSB_buttonScrollDown: false, mCSB_buttonScrollUp: false }); if (e.horizontalScroll) { if (m.css("max-width") !== "none") { if (!e.advanced.updateOnContentResize) { e.advanced.updateOnContentResize = true } } } else { if (m.css("max-height") !== "none") { var s = false, r = parseInt(m.css("max-height")); if (m.css("max-height").indexOf("%") >= 0) { s = r, r = m.parent().height() * s / 100 } m.css("overflow", "hidden"); g.css("max-height", r) } } m.mCustomScrollbar("update"); if (e.advanced.updateOnBrowserResize) { var i, j = c(window).width(), u = c(window).height(); c(window).bind("resize." + m.data("mCustomScrollbarIndex"), function () { if (i) { clearTimeout(i) } i = setTimeout(function () { if (!m.is(".mCS_disabled") && !m.is(".mCS_destroyed")) { var w = c(window).width(), v = c(window).height(); if (j !== w || u !== v) { if (m.css("max-height") !== "none" && s) { g.css("max-height", m.parent().height() * s / 100) } m.mCustomScrollbar("update"); j = w; u = v } } }, 150) }) } if (e.advanced.updateOnContentResize) { var p; if (e.horizontalScroll) { var n = o.outerWidth() } else { var n = o.outerHeight() } p = setInterval(function () { if (e.horizontalScroll) { if (e.advanced.autoExpandHorizontalScroll) { o.css({ position: "absolute", width: "auto" }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({ width: o.outerWidth(), position: "relative" }).unwrap() } var v = o.outerWidth() } else { var v = o.outerHeight() } if (v != n) { m.mCustomScrollbar("update"); n = v } }, 300) } }) }, update: function () { var n = c(this), k = n.children(".mCustomScrollBox"), q = k.children(".mCSB_container"); q.removeClass("mCS_no_scrollbar"); n.removeClass("mCS_disabled mCS_destroyed"); k.scrollTop(0).scrollLeft(0); var y = k.children(".mCSB_scrollTools"), o = y.children(".mCSB_draggerContainer"), m = o.children(".mCSB_dragger"); if (n.data("horizontalScroll")) { var A = y.children(".mCSB_buttonLeft"), t = y.children(".mCSB_buttonRight"), f = k.width(); if (n.data("autoExpandHorizontalScroll")) { q.css({ position: "absolute", width: "auto" }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({ width: q.outerWidth(), position: "relative" }).unwrap() } var z = q.outerWidth() } else { var w = y.children(".mCSB_buttonUp"), g = y.children(".mCSB_buttonDown"), r = k.height(), i = q.outerHeight() } if (i > r && !n.data("horizontalScroll")) { y.css("display", "block"); var s = o.height(); if (n.data("autoDraggerLength")) { var u = Math.round(r / i * s), l = m.data("minDraggerHeight"); if (u <= l) { m.css({ height: l }) } else { if (u >= s - 10) { var p = s - 10; m.css({ height: p }) } else { m.css({ height: u }) } } m.children(".mCSB_dragger_bar").css({ "line-height": m.height() + "px" }) } var B = m.height(), x = (i - r) / (s - B); n.data("scrollAmount", x).mCustomScrollbar("scrolling", k, q, o, m, w, g, A, t); var D = Math.abs(q.position().top); n.mCustomScrollbar("scrollTo", D, { scrollInertia: 0, trigger: "internal" }) } else { if (z > f && n.data("horizontalScroll")) { y.css("display", "block"); var h = o.width(); if (n.data("autoDraggerLength")) { var j = Math.round(f / z * h), C = m.data("minDraggerWidth"); if (j <= C) { m.css({ width: C }) } else { if (j >= h - 10) { var e = h - 10; m.css({ width: e }) } else { m.css({ width: j }) } } } var v = m.width(), x = (z - f) / (h - v); n.data("scrollAmount", x).mCustomScrollbar("scrolling", k, q, o, m, w, g, A, t); var D = Math.abs(q.position().left); n.mCustomScrollbar("scrollTo", D, { scrollInertia: 0, trigger: "internal" }) } else { k.unbind("mousewheel focusin"); if (n.data("horizontalScroll")) { m.add(q).css("left", 0) } else { m.add(q).css("top", 0) } y.css("display", "none"); q.addClass("mCS_no_scrollbar"); n.data({ bindEvent_mousewheel: false, bindEvent_focusin: false }) } } }, scrolling: function (h, p, m, j, w, e, A, v) { var k = c(this); if (!k.data("bindEvent_scrollbar_drag")) { var n, o; if (c.support.msPointer) { j.bind("MSPointerDown", function (H) { H.preventDefault(); k.data({ on_drag: true }); j.addClass("mCSB_dragger_onDrag"); var G = c(this), J = G.offset(), F = H.originalEvent.pageX - J.left, I = H.originalEvent.pageY - J.top; if (F < G.width() && F > 0 && I < G.height() && I > 0) { n = I; o = F } }); c(document).bind("MSPointerMove." + k.data("mCustomScrollbarIndex"), function (H) { H.preventDefault(); if (k.data("on_drag")) { var G = j, J = G.offset(), F = H.originalEvent.pageX - J.left, I = H.originalEvent.pageY - J.top; D(n, o, I, F) } }).bind("MSPointerUp." + k.data("mCustomScrollbarIndex"), function (x) { k.data({ on_drag: false }); j.removeClass("mCSB_dragger_onDrag") }) } else { j.bind("mousedown touchstart", function (H) { H.preventDefault(); H.stopImmediatePropagation(); var G = c(this), K = G.offset(), F, J; if (H.type === "touchstart") { var I = H.originalEvent.touches[0] || H.originalEvent.changedTouches[0]; F = I.pageX - K.left; J = I.pageY - K.top } else { k.data({ on_drag: true }); j.addClass("mCSB_dragger_onDrag"); F = H.pageX - K.left; J = H.pageY - K.top } if (F < G.width() && F > 0 && J < G.height() && J > 0) { n = J; o = F } }).bind("touchmove", function (H) { H.preventDefault(); H.stopImmediatePropagation(); var K = H.originalEvent.touches[0] || H.originalEvent.changedTouches[0], G = c(this), J = G.offset(), F = K.pageX - J.left, I = K.pageY - J.top; D(n, o, I, F) }); c(document).bind("mousemove." + k.data("mCustomScrollbarIndex"), function (H) { if (k.data("on_drag")) { var G = j, J = G.offset(), F = H.pageX - J.left, I = H.pageY - J.top; D(n, o, I, F) } }).bind("mouseup." + k.data("mCustomScrollbarIndex"), function (x) { k.data({ on_drag: false }); j.removeClass("mCSB_dragger_onDrag") }) } k.data({ bindEvent_scrollbar_drag: true }) } function D(G, H, I, F) { if (k.data("horizontalScroll")) { k.mCustomScrollbar("scrollTo", (j.position().left - (H)) + F, { moveDragger: true, trigger: "internal" }) } else { k.mCustomScrollbar("scrollTo", (j.position().top - (G)) + I, { moveDragger: true, trigger: "internal" }) } } if (c.support.touch && k.data("contentTouchScroll")) { if (!k.data("bindEvent_content_touch")) { var l, B, r, s, u, C, E; p.bind("touchstart", function (x) { x.stopImmediatePropagation(); l = x.originalEvent.touches[0] || x.originalEvent.changedTouches[0]; B = c(this); r = B.offset(); u = l.pageX - r.left; s = l.pageY - r.top; C = s; E = u }); p.bind("touchmove", function (x) { x.preventDefault(); x.stopImmediatePropagation(); l = x.originalEvent.touches[0] || x.originalEvent.changedTouches[0]; B = c(this).parent(); r = B.offset(); u = l.pageX - r.left; s = l.pageY - r.top; if (k.data("horizontalScroll")) { k.mCustomScrollbar("scrollTo", E - u, { trigger: "internal" }) } else { k.mCustomScrollbar("scrollTo", C - s, { trigger: "internal" }) } }) } } if (!k.data("bindEvent_scrollbar_click")) { m.bind("click", function (F) { var x = (F.pageY - m.offset().top) * k.data("scrollAmount"), y = c(F.target); if (k.data("horizontalScroll")) { x = (F.pageX - m.offset().left) * k.data("scrollAmount") } if (y.hasClass("mCSB_draggerContainer") || y.hasClass("mCSB_draggerRail")) { k.mCustomScrollbar("scrollTo", x, { trigger: "internal", scrollEasing: "draggerRailEase" }) } }); k.data({ bindEvent_scrollbar_click: true }) } if (k.data("mouseWheel")) { if (!k.data("bindEvent_mousewheel")) { h.bind("mousewheel", function (H, J) { var G, F = k.data("mouseWheelPixels"), x = Math.abs(p.position().top), I = j.position().top, y = m.height() - j.height(); if (k.data("normalizeMouseWheelDelta")) { if (J < 0) { J = -1 } else { J = 1 } } if (F === "auto") { F = 100 + Math.round(k.data("scrollAmount") / 2) } if (k.data("horizontalScroll")) { I = j.position().left; y = m.width() - j.width(); x = Math.abs(p.position().left) } if ((J > 0 && I !== 0) || (J < 0 && I !== y)) { H.preventDefault(); H.stopImmediatePropagation() } G = x - (J * F); k.mCustomScrollbar("scrollTo", G, { trigger: "internal" }) }); k.data({ bindEvent_mousewheel: true }) } } if (k.data("scrollButtons_enable")) { if (k.data("scrollButtons_scrollType") === "pixels") { if (k.data("horizontalScroll")) { v.add(A).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend", i, g); k.data({ bindEvent_buttonsContinuous_x: false }); if (!k.data("bindEvent_buttonsPixels_x")) { v.bind("click", function (x) { x.preventDefault(); q(Math.abs(p.position().left) + k.data("scrollButtons_scrollAmount")) }); A.bind("click", function (x) { x.preventDefault(); q(Math.abs(p.position().left) - k.data("scrollButtons_scrollAmount")) }); k.data({ bindEvent_buttonsPixels_x: true }) } } else { e.add(w).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend", i, g); k.data({ bindEvent_buttonsContinuous_y: false }); if (!k.data("bindEvent_buttonsPixels_y")) { e.bind("click", function (x) { x.preventDefault(); q(Math.abs(p.position().top) + k.data("scrollButtons_scrollAmount")) }); w.bind("click", function (x) { x.preventDefault(); q(Math.abs(p.position().top) - k.data("scrollButtons_scrollAmount")) }); k.data({ bindEvent_buttonsPixels_y: true }) } } function q(x) { if (!j.data("preventAction")) { j.data("preventAction", true); k.mCustomScrollbar("scrollTo", x, { trigger: "internal" }) } } } else { if (k.data("horizontalScroll")) { v.add(A).unbind("click"); k.data({ bindEvent_buttonsPixels_x: false }); if (!k.data("bindEvent_buttonsContinuous_x")) { v.bind("mousedown touchstart MSPointerDown", function (y) { y.preventDefault(); var x = z(); k.data({ mCSB_buttonScrollRight: setInterval(function () { k.mCustomScrollbar("scrollTo", Math.abs(p.position().left) + x, { trigger: "internal", scrollEasing: "easeOutCirc" }) }, 17) }) }); var i = function (x) { x.preventDefault(); clearInterval(k.data("mCSB_buttonScrollRight")) }; v.bind("mouseup touchend MSPointerUp mouseout MSPointerOut", i); A.bind("mousedown touchstart MSPointerDown", function (y) { y.preventDefault(); var x = z(); k.data({ mCSB_buttonScrollLeft: setInterval(function () { k.mCustomScrollbar("scrollTo", Math.abs(p.position().left) - x, { trigger: "internal", scrollEasing: "easeOutCirc" }) }, 17) }) }); var g = function (x) { x.preventDefault(); clearInterval(k.data("mCSB_buttonScrollLeft")) }; A.bind("mouseup touchend MSPointerUp mouseout MSPointerOut", g); k.data({ bindEvent_buttonsContinuous_x: true }) } } else { e.add(w).unbind("click"); k.data({ bindEvent_buttonsPixels_y: false }); if (!k.data("bindEvent_buttonsContinuous_y")) { e.bind("mousedown touchstart MSPointerDown", function (y) { y.preventDefault(); var x = z(); k.data({ mCSB_buttonScrollDown: setInterval(function () { k.mCustomScrollbar("scrollTo", Math.abs(p.position().top) + x, { trigger: "internal", scrollEasing: "easeOutCirc" }) }, 17) }) }); var t = function (x) { x.preventDefault(); clearInterval(k.data("mCSB_buttonScrollDown")) }; e.bind("mouseup touchend MSPointerUp mouseout MSPointerOut", t); w.bind("mousedown touchstart MSPointerDown", function (y) { y.preventDefault(); var x = z(); k.data({ mCSB_buttonScrollUp: setInterval(function () { k.mCustomScrollbar("scrollTo", Math.abs(p.position().top) - x, { trigger: "internal", scrollEasing: "easeOutCirc" }) }, 17) }) }); var f = function (x) { x.preventDefault(); clearInterval(k.data("mCSB_buttonScrollUp")) }; w.bind("mouseup touchend MSPointerUp mouseout MSPointerOut", f); k.data({ bindEvent_buttonsContinuous_y: true }) } } function z() { var x = k.data("scrollButtons_scrollSpeed"); if (k.data("scrollButtons_scrollSpeed") === "auto") { x = Math.round((k.data("scrollInertia") + 100) / 40) } return x } } } if (k.data("autoScrollOnFocus")) { if (!k.data("bindEvent_focusin")) { h.bind("focusin", function () { h.scrollTop(0).scrollLeft(0); var x = c(document.activeElement); if (x.is("input,textarea,select,button,a[tabindex],area,object")) { var G = p.position().top, y = x.position().top, F = h.height() - x.outerHeight(); if (k.data("horizontalScroll")) { G = p.position().left; y = x.position().left; F = h.width() - x.outerWidth() } if (G + y < 0 || G + y > F) { k.mCustomScrollbar("scrollTo", y, { trigger: "internal" }) } } }); k.data({ bindEvent_focusin: true }) } } if (k.data("autoHideScrollbar")) { if (!k.data("bindEvent_autoHideScrollbar")) { h.bind("mouseenter", function (x) { h.addClass("mCS-mouse-over"); d.showScrollbar.call(h.children(".mCSB_scrollTools")) }).bind("mouseleave touchend", function (x) { h.removeClass("mCS-mouse-over"); if (x.type === "mouseleave") { d.hideScrollbar.call(h.children(".mCSB_scrollTools")) } }); k.data({ bindEvent_autoHideScrollbar: true }) } } }, scrollTo: function (e, f) { var i = c(this), o = { moveDragger: false, trigger: "external", callbacks: true, scrollInertia: i.data("scrollInertia"), scrollEasing: i.data("scrollEasing") }, f = c.extend(o, f), p, g = i.children(".mCustomScrollBox"), k = g.children(".mCSB_container"), r = g.children(".mCSB_scrollTools"), j = r.children(".mCSB_draggerContainer"), h = j.children(".mCSB_dragger"), t = draggerSpeed = f.scrollInertia, q, s, m, l; if (!k.hasClass("mCS_no_scrollbar")) { i.data({ mCS_trigger: f.trigger }); if (i.data("mCS_Init")) { f.callbacks = false } if (e || e === 0) { if (typeof (e) === "number") { if (f.moveDragger) { p = e; if (i.data("horizontalScroll")) { e = h.position().left * i.data("scrollAmount") } else { e = h.position().top * i.data("scrollAmount") } draggerSpeed = 0 } else { p = e / i.data("scrollAmount") } } else { if (typeof (e) === "string") { var v; if (e === "top") { v = 0 } else { if (e === "bottom" && !i.data("horizontalScroll")) { v = k.outerHeight() - g.height() } else { if (e === "left") { v = 0 } else { if (e === "right" && i.data("horizontalScroll")) { v = k.outerWidth() - g.width() } else { if (e === "first") { v = i.find(".mCSB_container").find(":first") } else { if (e === "last") { v = i.find(".mCSB_container").find(":last") } else { v = i.find(e) } } } } } } if (v.length === 1) { if (i.data("horizontalScroll")) { e = v.position().left } else { e = v.position().top } p = e / i.data("scrollAmount") } else { p = e = v } } } if (i.data("horizontalScroll")) { if (i.data("onTotalScrollBack_Offset")) { s = -i.data("onTotalScrollBack_Offset") } if (i.data("onTotalScroll_Offset")) { l = g.width() - k.outerWidth() + i.data("onTotalScroll_Offset") } if (p < 0) { p = e = 0; clearInterval(i.data("mCSB_buttonScrollLeft")); if (!s) { q = true } } else { if (p >= j.width() - h.width()) { p = j.width() - h.width(); e = g.width() - k.outerWidth(); clearInterval(i.data("mCSB_buttonScrollRight")); if (!l) { m = true } } else { e = -e } } var n = i.data("snapAmount"); if (n) { e = Math.round(e / n) * n - i.data("snapOffset") } d.mTweenAxis.call(this, h[0], "left", Math.round(p), draggerSpeed, f.scrollEasing); d.mTweenAxis.call(this, k[0], "left", Math.round(e), t, f.scrollEasing, { onStart: function () { if (f.callbacks && !i.data("mCS_tweenRunning")) { u("onScrollStart") } if (i.data("autoHideScrollbar")) { d.showScrollbar.call(r) } }, onUpdate: function () { if (f.callbacks) { u("whileScrolling") } }, onComplete: function () { if (f.callbacks) { u("onScroll"); if (q || (s && k.position().left >= s)) { u("onTotalScrollBack") } if (m || (l && k.position().left <= l)) { u("onTotalScroll") } } h.data("preventAction", false); i.data("mCS_tweenRunning", false); if (i.data("autoHideScrollbar")) { if (!g.hasClass("mCS-mouse-over")) { d.hideScrollbar.call(r) } } } }) } else { if (i.data("onTotalScrollBack_Offset")) { s = -i.data("onTotalScrollBack_Offset") } if (i.data("onTotalScroll_Offset")) { l = g.height() - k.outerHeight() + i.data("onTotalScroll_Offset") } if (p < 0) { p = e = 0; clearInterval(i.data("mCSB_buttonScrollUp")); if (!s) { q = true } } else { if (p >= j.height() - h.height()) { p = j.height() - h.height(); e = g.height() - k.outerHeight(); clearInterval(i.data("mCSB_buttonScrollDown")); if (!l) { m = true } } else { e = -e } } var n = i.data("snapAmount"); if (n) { e = Math.round(e / n) * n - i.data("snapOffset") } d.mTweenAxis.call(this, h[0], "top", Math.round(p), draggerSpeed, f.scrollEasing); d.mTweenAxis.call(this, k[0], "top", Math.round(e), t, f.scrollEasing, { onStart: function () { if (f.callbacks && !i.data("mCS_tweenRunning")) { u("onScrollStart") } if (i.data("autoHideScrollbar")) { d.showScrollbar.call(r) } }, onUpdate: function () { if (f.callbacks) { u("whileScrolling") } }, onComplete: function () { if (f.callbacks) { u("onScroll"); if (q || (s && k.position().top >= s)) { u("onTotalScrollBack") } if (m || (l && k.position().top <= l)) { u("onTotalScroll") } } h.data("preventAction", false); i.data("mCS_tweenRunning", false); if (i.data("autoHideScrollbar")) { if (!g.hasClass("mCS-mouse-over")) { d.hideScrollbar.call(r) } } } }) } if (i.data("mCS_Init")) { i.data({ mCS_Init: false }) } } } function u(w) { this.mcs = { top: k.position().top, left: k.position().left, draggerTop: h.position().top, draggerLeft: h.position().left, topPct: Math.round((100 * Math.abs(k.position().top)) / Math.abs(k.outerHeight() - g.height())), leftPct: Math.round((100 * Math.abs(k.position().left)) / Math.abs(k.outerWidth() - g.width())) }; switch (w) { case "onScrollStart": i.data("mCS_tweenRunning", true).data("onScrollStart_Callback").call(i, this.mcs); break; case "whileScrolling": i.data("whileScrolling_Callback").call(i, this.mcs); break; case "onScroll": i.data("onScroll_Callback").call(i, this.mcs); break; case "onTotalScrollBack": i.data("onTotalScrollBack_Callback").call(i, this.mcs); break; case "onTotalScroll": i.data("onTotalScroll_Callback").call(i, this.mcs); break } } }, stop: function () { var g = c(this), e = g.children().children(".mCSB_container"), f = g.children().children().children().children(".mCSB_dragger"); d.mTweenAxisStop.call(this, e[0]); d.mTweenAxisStop.call(this, f[0]) }, disable: function (e) { var j = c(this), f = j.children(".mCustomScrollBox"), h = f.children(".mCSB_container"), g = f.children(".mCSB_scrollTools"), i = g.children().children(".mCSB_dragger"); f.unbind("mousewheel focusin mouseenter mouseleave touchend"); h.unbind("touchstart touchmove"); if (e) { if (j.data("horizontalScroll")) { i.add(h).css("left", 0) } else { i.add(h).css("top", 0) } } g.css("display", "none"); h.addClass("mCS_no_scrollbar"); j.data({ bindEvent_mousewheel: false, bindEvent_focusin: false, bindEvent_content_touch: false, bindEvent_autoHideScrollbar: false }).addClass("mCS_disabled") }, destroy: function () { var e = c(this); e.removeClass("mCustomScrollbar _mCS_" + e.data("mCustomScrollbarIndex")).addClass("mCS_destroyed").children().children(".mCSB_container").unwrap().children().unwrap().siblings(".mCSB_scrollTools").remove(); c(document).unbind("mousemove." + e.data("mCustomScrollbarIndex") + " mouseup." + e.data("mCustomScrollbarIndex") + " MSPointerMove." + e.data("mCustomScrollbarIndex") + " MSPointerUp." + e.data("mCustomScrollbarIndex")); c(window).unbind("resize." + e.data("mCustomScrollbarIndex")) } }, d = { showScrollbar: function () { this.stop().animate({ opacity: 1 }, "fast") }, hideScrollbar: function () { this.stop().animate({ opacity: 0 }, "fast") }, mTweenAxis: function (g, i, h, f, o, y) { var y = y || {}, v = y.onStart || function () { }, p = y.onUpdate || function () { }, w = y.onComplete || function () { }; var n = t(), l, j = 0, r = g.offsetTop, s = g.style; if (i === "left") { r = g.offsetLeft } var m = h - r; q(); e(); function t() { if (window.performance && window.performance.now) { return window.performance.now() } else { if (window.performance && window.performance.webkitNow) { return window.performance.webkitNow() } else { if (Date.now) { return Date.now() } else { return new Date().getTime() } } } } function x() { if (!j) { v.call() } j = t() - n; u(); if (j >= g._time) { g._time = (j > g._time) ? j + l - (j - g._time) : j + l - 1; if (g._time < j + 1) { g._time = j + 1 } } if (g._time < f) { g._id = _request(x) } else { w.call() } } function u() { if (f > 0) { g.currVal = k(g._time, r, m, f, o); s[i] = Math.round(g.currVal) + "px" } else { s[i] = h + "px" } p.call() } function e() { l = 1000 / 60; g._time = j + l; _request = (!window.requestAnimationFrame) ? function (z) { u(); return setTimeout(z, 0.01) } : window.requestAnimationFrame; g._id = _request(x) } function q() { if (g._id == null) { return } if (!window.requestAnimationFrame) { clearTimeout(g._id) } else { window.cancelAnimationFrame(g._id) } g._id = null } function k(B, A, F, E, C) { switch (C) { case "linear": return F * B / E + A; break; case "easeOutQuad": B /= E; return -F * B * (B - 2) + A; break; case "easeInOutQuad": B /= E / 2; if (B < 1) { return F / 2 * B * B + A } B--; return -F / 2 * (B * (B - 2) - 1) + A; break; case "easeOutCubic": B /= E; B--; return F * (B * B * B + 1) + A; break; case "easeOutQuart": B /= E; B--; return -F * (B * B * B * B - 1) + A; break; case "easeOutQuint": B /= E; B--; return F * (B * B * B * B * B + 1) + A; break; case "easeOutCirc": B /= E; B--; return F * Math.sqrt(1 - B * B) + A; break; case "easeOutSine": return F * Math.sin(B / E * (Math.PI / 2)) + A; break; case "easeOutExpo": return F * (-Math.pow(2, -10 * B / E) + 1) + A; break; case "mcsEaseOut": var D = (B /= E) * B, z = D * B; return A + F * (0.499999999999997 * z * D + -2.5 * D * D + 5.5 * z + -6.5 * D + 4 * B); break; case "draggerRailEase": B /= E / 2; if (B < 1) { return F / 2 * B * B * B + A } B -= 2; return F / 2 * (B * B * B + 2) + A; break } } }, mTweenAxisStop: function (e) { if (e._id == null) { return } if (!window.requestAnimationFrame) { clearTimeout(e._id) } else { window.cancelAnimationFrame(e._id) } e._id = null }, rafPolyfill: function () { var f = ["ms", "moz", "webkit", "o"], e = f.length; while (--e > -1 && !window.requestAnimationFrame) { window.requestAnimationFrame = window[f[e] + "RequestAnimationFrame"]; window.cancelAnimationFrame = window[f[e] + "CancelAnimationFrame"] || window[f[e] + "CancelRequestAnimationFrame"] } } }; d.rafPolyfill.call(); c.support.touch = !!("ontouchstart" in window); c.support.msPointer = window.navigator.msPointerEnabled; var a = ("https:" == document.location.protocol) ? "https:" : "http:"; c.event.special.mousewheel || document.write('<script src="' + a + '//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.0.6/jquery.mousewheel.min.js"><\/script>'); c.fn.mCustomScrollbar = function (e) { if (b[e]) { return b[e].apply(this, Array.prototype.slice.call(arguments, 1)) } else { if (typeof e === "object" || !e) { return b.init.apply(this, arguments) } else { c.error("Method " + e + " does not exist") } } } })(jQuery);

; (function ($) {

    function defined(a) {
        return typeof a !== 'undefined';
    }

    function extend(child, parent, prototype) {
        var F = function () { };
        F.prototype = parent.prototype;
        child.prototype = new F();
        child.prototype.constructor = child;
        parent.prototype.constructor = parent;
        child._super = parent.prototype;
        if (prototype) {
            $.extend(child.prototype, prototype);
        }
    }

    var SUBST = [
        ['', ''],               // spec
        ['exit', 'cancel'],     // firefox & old webkits expect cancelFullScreen instead of exitFullscreen
        ['screen', 'Screen']    // firefox expects FullScreen instead of Fullscreen
    ];

    var VENDOR_PREFIXES = ['', 'o', 'ms', 'moz', 'webkit', 'webkitCurrent'];

    function native(obj, name) {
        var prefixed;

        if (typeof obj === 'string') {
            name = obj;
            obj = document;
        }

        for (var i = 0; i < SUBST.length; ++i) {
            name = name.replace(SUBST[i][0], SUBST[i][1]);
            for (var j = 0; j < VENDOR_PREFIXES.length; ++j) {
                prefixed = VENDOR_PREFIXES[j];
                prefixed += j === 0 ? name : name.charAt(0).toUpperCase() + name.substr(1);
                if (defined(obj[prefixed])) {
                    return obj[prefixed];
                }
            }
        }

        return void 0;
    } var ua = navigator.userAgent;
    var fsEnabled = native('fullscreenEnabled');
    var IS_ANDROID_CHROME = ua.indexOf('Android') !== -1 && ua.indexOf('Chrome') !== -1;
    var IS_NATIVELY_SUPPORTED =
            !IS_ANDROID_CHROME &&
             defined(native('fullscreenElement')) &&
            (!defined(fsEnabled) || fsEnabled === true);

    var version = $.fn.jquery.split('.');
    var JQ_LT_17 = (parseInt(version[0]) < 2 && parseInt(version[1]) < 7);

    var FullScreenAbstract = function () {
        this.__options = null;
        this._fullScreenElement = null;
        this.__savedStyles = {};
    };

    FullScreenAbstract.prototype = {
        _DEFAULT_OPTIONS: {
            styles: {
                'boxSizing': 'border-box',
                'MozBoxSizing': 'border-box',
                'WebkitBoxSizing': 'border-box'
            },
            toggleClass: null
        },
        __documentOverflow: '',
        __htmlOverflow: '',
        _preventDocumentScroll: function () {
            this.__documentOverflow = $('body')[0].style.overflow;
            this.__htmlOverflow = $('html')[0].style.overflow;
            // $('body, html').css('overflow', 'hidden');
        },
        _allowDocumentScroll: function () {
            // $('body')[0].style.overflow = this.__documentOverflow;
            // $('html')[0].style.overflow = this.__htmlOverflow;
        },
        _fullScreenChange: function () {
            if (!this.isFullScreen()) {
                this._allowDocumentScroll();
                this._revertStyles();
                this._triggerEvents();
                this._fullScreenElement = null;
            } else {
                this._preventDocumentScroll();
                this._triggerEvents();
            }
        },
        _fullScreenError: function (e) {
            this._revertStyles();
            this._fullScreenElement = null;
            if (e) {
                $(document).trigger('fscreenerror', [e]);
            }
        },
        _triggerEvents: function () {
            $(this._fullScreenElement).trigger(this.isFullScreen() ? 'fscreenopen' : 'fscreenclose');
            $(document).trigger('fscreenchange', [this.isFullScreen(), this._fullScreenElement]);
        },
        _saveAndApplyStyles: function () {
            var $elem = $(this._fullScreenElement);
            this.__savedStyles = {};
            for (var property in this.__options.styles) {
                // save
                this.__savedStyles[property] = this._fullScreenElement.style[property];
                // apply
                this._fullScreenElement.style[property] = this.__options.styles[property];
            }
            if (this.__options.toggleClass) {
                $elem.addClass(this.__options.toggleClass);
            }
        },
        _revertStyles: function () {
            var $elem = $(this._fullScreenElement);
            for (var property in this.__options.styles) {
                this._fullScreenElement.style[property] = this.__savedStyles[property];
            }
            if (this.__options.toggleClass) {
                $elem.removeClass(this.__options.toggleClass);
            }
        },
        open: function (elem, options) {
            // do nothing if request is for already fullscreened element
            if (elem === this._fullScreenElement) {
                return;
            }
            // exit active fullscreen before opening another one
            if (this.isFullScreen()) {
                this.exit();
            }
            // save fullscreened element
            this._fullScreenElement = elem;
            // apply options, if any
            this.__options = $.extend(true, {}, this._DEFAULT_OPTIONS, options);
            // save current element styles and apply new
            this._saveAndApplyStyles();
        },
        exit: null,
        isFullScreen: null,
        isNativelySupported: function () {
            return IS_NATIVELY_SUPPORTED;
        }
    };
    var FullScreenNative = function () {
        FullScreenNative._super.constructor.apply(this, arguments);
        this.exit = $.proxy(native('exitFullscreen'), document);
        this._DEFAULT_OPTIONS = $.extend(true, {}, this._DEFAULT_OPTIONS, {
            'styles': {
                'width': '100%',
                'height': '100%'
            }
        });
        $(document)
            .bind(this._prefixedString('fullscreenchange') + ' MSFullscreenChange', $.proxy(this._fullScreenChange, this))
            .bind(this._prefixedString('fullscreenerror') + ' MSFullscreenError', $.proxy(this._fullScreenError, this));
    };

    extend(FullScreenNative, FullScreenAbstract, {
        VENDOR_PREFIXES: ['', 'o', 'moz', 'webkit'],
        _prefixedString: function (str) {
            return $.map(this.VENDOR_PREFIXES, function (s) {
                return s + str;
            }).join(' ');
        },
        open: function (elem, options) {
            FullScreenNative._super.open.apply(this, arguments);
            var requestFS = native(elem, 'requestFullscreen');
            requestFS.call(elem);
        },
        exit: $.noop,
        isFullScreen: function () {
            return native('fullscreenElement') !== null;
        },
        element: function () {
            return native('fullscreenElement');
        }
    });
    var FullScreenFallback = function () {
        FullScreenFallback._super.constructor.apply(this, arguments);
        this._DEFAULT_OPTIONS = $.extend({}, this._DEFAULT_OPTIONS, {
            'styles': {
                'position': 'fixed',
                'zIndex': '2147483647',
                'left': 0,
                'top': 0,
                'bottom': 0,
                'right': 0
            }
        });
        this.__delegateKeydownHandler();
    };

    extend(FullScreenFallback, FullScreenAbstract, {
        __isFullScreen: false,
        __delegateKeydownHandler: function () {
            var $doc = $(document);
            $doc.delegate('*', 'keydown.fullscreen', $.proxy(this.__keydownHandler, this));
            var data = JQ_LT_17 ? $doc.data('events') : $._data(document).events;
            var events = data['keydown'];
            if (!JQ_LT_17) {
                events.splice(0, 0, events.splice(events.delegateCount - 1, 1)[0]);
            } else {
                data.live.unshift(data.live.pop());
            }
        },
        __keydownHandler: function (e) {
            if (this.isFullScreen() && e.which === 27) {
                this.exit();
                return false;
            }
            return true;
        },
        _revertStyles: function () {
            FullScreenFallback._super._revertStyles.apply(this, arguments);
            // force redraw (fixes bug in IE7 with content dissapearing)
            this._fullScreenElement.offsetHeight;
        },
        open: function (elem) {
            FullScreenFallback._super.open.apply(this, arguments);
            this.__isFullScreen = true;
            this._fullScreenChange();
        },
        exit: function () {
            this.__isFullScreen = false;
            this._fullScreenChange();
        },
        isFullScreen: function () {
            return this.__isFullScreen;
        },
        element: function () {
            return this.__isFullScreen ? this._fullScreenElement : null;
        }
    }); $.fullscreen = IS_NATIVELY_SUPPORTED
                    ? new FullScreenNative()
                    : new FullScreenFallback();

    $.fn.fullscreen = function (options) {
        var elem = this[0];

        options = $.extend({
            toggleClass: null,
            // overflow: 'hidden'
        }, options);
        options.styles = {
            // overflow: options.overflow
        };
        // delete options.overflow;

        if (elem) {
            $.fullscreen.open(elem, options);
        }

        return this;
    };
})(jQuery);